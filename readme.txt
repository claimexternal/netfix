

  ___    ___ ________  ___  ___  ________   ________  ___       ________  ________  ________                                     
 |\  \  /  /|\   __  \|\  \|\  \|\   ___  \|\   ____\|\  \     |\   __  \|\   __  \|\   ____\                                    
 \ \  \/  / | \  \|\  \ \  \\\  \ \  \\ \  \ \  \___|\ \  \    \ \  \|\  \ \  \|\ /\ \  \___|_                                   
  \ \    / / \ \  \\\  \ \  \\\  \ \  \\ \  \ \  \  __\ \  \    \ \   __  \ \   __  \ \_____  \                                  
   \/  /  /   \ \  \\\  \ \  \\\  \ \  \\ \  \ \  \|\  \ \  \____\ \  \ \  \ \  \|\  \|____|\  \                                 
 __/  / /      \ \_______\ \_______\ \__\\ \__\ \_______\ \_______\ \__\ \__\ \_______\____\_\  \                                
|\___/ /        \|_______|\|_______|\|__| \|__|\|_______|\|_______|\|__|\|__|\|_______|\_________\                               
\|___|/                                                                               \|_________|   Presents                            
                                                                                                                                 
                                                                                                                                 
 ___       ________  ________  ________  ________  _____ ______   ___  ________           ________  _____ ______   ________      
|\  \     |\   __  \|\   __  \|\   __  \|\   ___ \|\   _ \  _   \|\  \|\   ___  \        |\   ____\|\   _ \  _   \|\   ____\     
\ \  \    \ \  \|\  \ \  \|\ /\ \  \|\  \ \  \_|\ \ \  \\\__\ \  \ \  \ \  \\ \  \       \ \  \___|\ \  \\\__\ \  \ \  \___|_    
 \ \  \    \ \   __  \ \   __  \ \   __  \ \  \ \\ \ \  \\|__| \  \ \  \ \  \\ \  \       \ \  \    \ \  \\|__| \  \ \_____  \   
  \ \  \____\ \  \ \  \ \  \|\  \ \  \ \  \ \  \_\\ \ \  \    \ \  \ \  \ \  \\ \  \       \ \  \____\ \  \    \ \  \|____|\  \  
   \ \_______\ \__\ \__\ \_______\ \__\ \__\ \_______\ \__\    \ \__\ \__\ \__\\ \__\       \ \_______\ \__\    \ \__\____\_\  \ 
    \|_______|\|__|\|__|\|_______|\|__|\|__|\|_______|\|__|     \|__|\|__|\|__| \|__|        \|_______|\|__|     \|__|\_________\
                                                                                                                     \|_________|
                                                                                                                                 
                                                                                                                                 

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

 * Work on Laravel 4.2

Repository: https://pett@bitbucket.org/pett/cms.git

External library:

- Paypal / API
- Intervention / API

Functionalities:

IMAGE
- Smart image crop & resize. Image fitting
- Smart image placheholder creator

DEVELOP
- Easy database loop
- Easy frontend module implement
- Easy backend module implement
- Easy Ajax implementation

CUSTOMIZATION
- Easy access to custom functionalities

EMAIL
- Easy email send with custom templates

STATISTICS
- Every user movement is tracked in the db for statistical purposes
- Each backend module has his stats service


Enjoy it ;)

Made with Love by Enrico Pettinao
enricopettinao@gmail.com