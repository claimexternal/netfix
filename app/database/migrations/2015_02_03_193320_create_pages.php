<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePages extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pages', function( $table )
			{

				$table->increments('id');
				$table->softDeletes();
				$table->timestamps();
				$table->integer('parent_id');
				$table->integer('ordination');
				$table->unsignedInteger('title'); //S
				$table->unsignedInteger('description'); //S
				$table->unsignedInteger('url');
				$table->text('media'); //Serialized array media galley
				//$table->primary('id');

				//Foreign key
				$table->foreign('title')->references('id')->on('strings');
				$table->foreign('description')->references('id')->on('strings');
				$table->foreign('url')->references('id')->on('strings');

			});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pages');
	}

}
