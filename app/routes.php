<?php

require __DIR__.'/helpers.php';

Route::post('/bitbucketpullrequest', function() {

	LabConfig::deployment();

});

//Chiamata post Form Generico
Route::post('formrequest/{code}', 'RouteController@formRequest');

//Leggo file upload form
Route::get('formupload/{filename}', 'RouteController@formFile');

//Chiamate post per moduli custom front end
Route::post('{custom}/{action}', 'RouteController@postCustom');

//Chiamate standard
Route::get('{all}', array('as' => 'dinamic', 'uses' => 'RouteController@getContent'))->where('all', '.*');

//Default per il 404
App::missing(function($exception) {

    echo "Pagina non trovata";

});


 