<?php

/*
|
| Author: Enrico Pettinao
| 04/01/2015
|
*/


class RouteController extends BaseController {

	
	//Torno pagina dinamica in base all'url
	public function getContent( $url ) {	

		Front::$url = explode( '/', $url);

		//Inizializzo scritte di contorno sito
		$text = new Texts();

		Front::$texts = $text->get();

		$langs = Front::$languages;

		//Diramo contenuti multilingua
		switch ( strlen(Front::$url[0]) ) {

			//Homepage lingua default
			case 0:
				
				Front::getHome();

			break;
			
			//Lingua straniera
			case 2:

				if(isset( $langs[Front::$url[0]])) {

					//Home lingua straniera
					if(!isset(Front::$url[1]))
						
						Front::getHome();
					
					//Skeleton lingua straniera
					else {

						if(isset(Front::$url[1]) && isset(Front::$url[2])) {

							//Cerco moduli custom in lingua
							if($this->getCustom(Front::$url[1], Front::$url[2])) {

								$module = "Front".Front::$url[1];
								$action = Front::$url[2];

								if(isset(Front::$url[3]))

									"Front".$module::$action(Front::$url[3]);
								
								else

									"Front".$module::$action();

								die();

							}

							else
								
								Front::findContent();

						}

						else

							Front::findContent();

					}
				}

				else {

					header("HTTP/1.0 404 Not Found");
					$array['page'] = array();
					echo View::make( 'skeleton', $array)->nest('content', '404')->render();

				}

			break;

			//Lingua default
			default:
				

				if(isset(Front::$url[0]) && isset(Front::$url[1])) {

					//Cerco moduli custom
					if($this->getCustom(Front::$url[0], Front::$url[1])) {

						$module = "Front".Front::$url[0];
						$action = Front::$url[1];

						if(isset(Front::$url[2]))

							$module::$action(Front::$url[2]);
						
						else

							$module::$action();
						
						die();

					}

					else
						
						Front::findContent();

				}

				else

					Front::findContent();

				break;
			}   
			
			die();

	}

	public function getCustom( $custom, $action ) {

		$class = "Front".$custom;
		//F::printer(Front::$url); die();

		//die($class." --> ".App::getLocale().$action);

		if(class_exists($class)) {

			//Per moduli front con slugs in lingua
			if( property_exists($class, 'slugs')) {

				//Se l'action in lingua è presente nell'array di slugs
				if($newAction = array_search($action, $class::$slugs[App::getLocale()])) {

					//Se siamo nella lingua di default
					if(App::getLocale() == array_keys(Front::$languages)[0]) {

						if(isset(Front::$url[2]))

							$class::$newAction(Front::$url[2]);

						else

							$class::$newAction();
					}

					//Lingua straniera
					else {

						if(isset(Front::$url[3]))

							$class::$newAction(Front::$url[3]);

						else

							$class::$newAction();

					}

					die();

				}

			} elseif(method_exists($class, $action))

				return true;

			else

				return false;

		}

		else

			return false;

	}

	public function login() {

		if (Auth::attempt(array('email' => Input::get('email'), 'password' => Input::get('password')), true)) {

			//L'utente stava effettuando il checkout
			if(isset($_SESSION['checkout']))

				$url = URL::to('cart/checkout');

			else

				$url = URL::to('users/profile');

		}

		else {

			echo "<div class='alert alert-danger'>Email o password errati.</div>";

		}
	}

	public function formFile($name) {

		//Se il file esiste nella cartella documents
		if(File::exists(__DIR__.'/../media/formfiles/'.$name))

			$fopen = __DIR__.'/../media/formfiles/'.$name;

		else {

			die("Documento non disponibile");

		}

		header("Content-Type:application/pdf");
		header("Content-Length: " .filesize($fopen));
		$fp = fopen($fopen, 'rb');
		fpassthru($fp);

	}

	public function formRequest($code) {

		$code = base64_decode($code);

		App::setLocale($_GET['lang']);

		$form = FormBuilder::with('title')->where('code', $code)->first();
		$return = array();
 		
 		$idNazione = '000000';

		$return['html'] = "";
		$return['error'] = 0;
		$return['data'] = Input::all();
		$return['data']['tag'] = 'Form Sito Web '.$form['title']['it'];
		$return['codist'] = F::encryptString(LabConfig::$labconfigs['codist']);
		$return['url'] = 'https://app.bconsole.com/Bgest/web/bcms_api.php/webtolead/insert';

		//Converto Nazione in id Naz Bconsole
		if(Input::has('nazione')) {

			switch (Input::get('nazione')) {

				case 'Italy':
					
					$idNazione = '999907';

				break;
				
				case 'United Kingdom':
					
					$idNazione = '999114';

				break;
				
				case 'France':
					
					$idNazione = '999110';

				break;
				
				case 'Spain':
					
					$idNazione = '999131';

				break;
				
				case 'Netherlands':
					
					$idNazione = '999126';

				break;
				
				case 'Russia':
					
					$idNazione = '999154';

				break;
				
				case 'United States of America':
					
					$idNazione = '999404';

				break;
				
				case 'Germany':
					
					$idNazione = '999112';

				break;

				case 'Austria':
					
					$idNazione = '999102';

				break;
				
				default:
				
					$idNazione = '000000';

				break;
			}

			$return['data']['nazione'] = $idNazione;

		}
		

		foreach($form->fields as $field) {

			if($field['name'] == 'email') {

				if (!filter_var(Input::get('email'), FILTER_VALIDATE_EMAIL) && Input::get('email') != "") {

					$return['error'] = 1;
					$return['html'] .="<li class=\"alert alert-danger\"><b>".$field['error'][App::getLocale()]."</b></li>";

				}

			}

			//Controllo su tipo file se l'estensione è supportata
			if($field['type'] == "file") {

				//File obbligatorio
				if($field['error'][App::getLocale()] != "") {

					if(!Input::hasFile($field['name'])) {

						$return['html'] .="<li class=\"alert alert-danger\"><b>".$field['error'][App::getLocale()]."</b></li>";

						$return['error'] = 1;

					} else {

						$allowedExtensions = Array(
							'png',
							'PNG',
							'jpg',
							'JPG',
							'gif',
							'GIF',
							'pdf',
							'PDF',
							'doc',
							'DOC',
						);

						$extension = Input::file($field['name'])->getClientOriginalExtension();		

						if(!in_array($extension, $allowedExtensions)) {

							$return['html'] .="<li class=\"alert alert-danger\"><b>File extension not supported.</b></li>";

							$return['error'] = 1;

						}

					}

				}

			}

			if(Input::get($field['name']) == "" && $field['error'][App::getLocale()] != "" && $field['type'] != "file") {

				$return['html'] .="<li class=\"alert alert-danger\"><b>".$field['error'][App::getLocale()]."</b></li>";

				$return['error'] = 1;

			}

		}

		if(!Input::has("privacy")) {

			$return['html'] .="<li class=\"alert alert-danger\"><b>".text('error_privacy', 
						array('it' => 'Accetta le condizioni sulla privacy'))."</b></li>";

			$return['error'] = 1;

		}

		if(!F::checkRecaptcha()) {

			$return['html'] .="<li class=\"alert alert-danger\"><b>reCaptcha challenge failed.</b></li>";

			$return['error'] = 1;

		}

		if($return['error'] == 0) {

			$fields = "";

			$fields .= "<b>Pagina</b>: ".Input::get('title').";<br/>";
			$fields .= "<b>Url</b>: <a target='_blank' href='".Input::get('url')."'>".Input::get('url')."</a>;<br/><br/>";

			foreach($form->fields as $field) {

				//Se è un file, salvo file ed il suo url
				if($field['type'] == "file") {

					if(!File::exists(__DIR__.'/../media/formfiles/'))

						File::makeDirectory(__DIR__.'/../media/formfiles/', $mode = 0777, true, true);

					$nameFile = time()."-".strtolower($form['code']).Input::file($field['name'])->getClientOriginalName();

					Input::file($field['name'])->move(__DIR__.'/../media/formfiles/', $nameFile);

					$fields .= "<b>".ucfirst($field['title'][App::getLocale()])."</b>: <a target='_blank' href='".url('formupload/'.$nameFile)."'>Visualizza file</a>;<br/>";

				} 
				//Se è un campo normale
				else

					$fields .= "<b>".ucfirst($field['title'][App::getLocale()])."</b>: ".Input::get($field['name']).";<br/>";

			}

			$replaces = array(

				"##TITLE##" => $form['title'][App::getLocale()],
				"##USERFIELDS##" => $fields

			);

			// Invio mail
			$admincontact = LabConfig::get('admincontacts', F::getenv());

			$recipient = Array(

				"to" => array(
					$admincontact['emailAdmin1'] => $admincontact['name'],
					$admincontact['email'] => $admincontact['name'],
				),

				"bcc" => array(

					$admincontact['emailCcn'] => $admincontact['nameCcn']

				)

			);

			if( $form['code'] == 'download_catalogo') {

				e::multiSend('reminderpdf', $recipient, $replaces);

			} else {

				e::multiSend('formrequest', $recipient, $replaces);

			}		
			
			$contacts = new Contacts();

			$contacts->user_id = 0;
			$contacts->pagetype = Input::get('type');
			$contacts->pageid = Input::get('page_id');
			$contacts->url = Input::get('url');

			if(Input::has('email'))

				$contacts->email = Input::get('email');

			else

				$contacts->email = "Utente anonimo";

			$contacts->form_builder_id = $form['id'];

			if(Input::has('request'))

				$contacts->request = Input::get('request');

			else

				$contacts->request = "Nessun messaggio specificato.";

			$contacts->fields = $fields;

			$contacts->save();

			F::addnotification('formrequest', 'Compilato un nuovo form '.$form['title'][App::getLocale()], 'È stato compilato un nuovo form '.$form['title'][App::getLocale()]);

			$return['html'] = "<li style=\"list-style:none;color:#fff;background: #56b555 none repeat scroll 0 0; border-radius: 5px;padding: 10px;\" class=\"alert alert-success\"><b>Richiesta inviata correttamente</b></li>";

		}

		echo json_encode($return);


		//F::printer($form);

	}

	public function postCustom( $custom, $action ) {

		if(isset($_GET['lang']))

			App::setLocale($_GET['lang']);

		$custom = 'Front'.$custom.'Post';
		$custom::$action($_POST);

	}


	public function htmlParser () {

		$template = html::getTheme();

		return View::make('parser.htmlParser', $template);


	}

}
