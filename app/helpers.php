<?php 

function text($key, $array = Array(), $simply = 0, $lang = "") {

    $langapp = App::getLocale();

    /*if($simply == 1) {

        $value = $key;

        $initial = trim(strip_tags($key));

        if(strpos($initial, " ", 0)) {

            $pos = strpos($initial, " ", 10);

            $string = substr($initial, 0, $pos ); 

        } else $string = $initial;
        
        $key = str_replace(" ", "_", strtolower($string));

        $array= array(array_keys(Front::$languages)[0] => $value);

    }*/

    if(isset(Front::$texts[trim($key)]))

    	return Front::$texts[$key];

    //Aggiungo scritta all'array
    else {

   		$newLangTexts = Texts::first();
 
    	foreach($array as $lang => $value) {
    	
    		$newLangTextsArray = unserialize(urldecode($newLangTexts[$lang]));

    		$newLangTextsArray[$key] = $value;

    		$newLangTexts->$lang = urlencode(serialize($newLangTextsArray));

    	}

    	$newLangTexts->save();

    	Front::$texts = unserialize(urldecode($newLangTexts[$langapp]));

    	if(!isset(Front::$texts[$key]))

    		return "??".$key."??";

    	else
    		
    		return Front::$texts[$key];

	}

}

function drawForm($code, $page) {

    return FrontendForm::drawForm($code, $page);
}

function lUrl($schema, $id) {


    F::getUrl($schema, $id);

}