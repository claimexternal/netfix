<?php

    if(isset($page)){
        $array = $page->{"formspage"};
        $array[] = $form['code'];
        $page->{"formspage"} = $array;
    }

?>

<style>

.btn-success {

	margin-top: 40px

}

</style>

<form id="form_{{$form['code']}}" action="{{ url('formrequest/'.base64_encode($form['code'])) }}" enctype="multipart/form-data" method="POST">
    @if($form['code'] != "newsletter")
    <div class="row">
    @endif

	{{ Form::token() }}
	
	<input type="hidden" name="title" value="{{ $page['title']}}">
	<input type="hidden" name="url" value="{{ URL::full() }}">
	<input type="hidden" name="type" value="{{ get_class($page) }}">
	<input type="hidden" name="page_id" value="{{ $page['id'] }}">
	
	<?php $i = 0 ?>


    @if($form['code'] <> "newsletter")
        
    	@foreach($form['fields'] as $field)
        
        
            @if($field['type'] == "text")
    				
                @if($field['name'] == "nazione" || $field['name'] == "provincia" || $field['name'] == "regione" )
                    
                    @include('default.select'.$field['name'], $field)
                    

                @else
                
                <div class="col-md-6">
                    <input class="form-control" type="text" name="{{$field['name']}}" placeholder="{{$field['title'][App::getLocale()]}}">
                </div>
               
                @endif

            @endif

            @if($field['type'] == "email")
                
                <div class="col-md-6">
                    <input class="form-control" type="text" id="email" @user value="{{ $_SESSION['user']['email'] }}" @enduser name="{{$field['name']}}" placeholder="{{$field['title'][App::getLocale()]}}">
                </div>                

            @endif

            @if($field['type'] == "textarea")
                
                <div class="col-md-12">
                    <textarea class="form-control" name="{{$field['name']}}" placeholder="{{$field['title'][App::getLocale()]}}"></textarea>
                </div>                

            @endif
        
            @if($field['type'] == "select")

                <div class="col-md-6">
                    <select class="form-control" name="{{$field['name']}}">

                        <option value="">{{$field['title'][App::getLocale()]}}...</option>

                        @foreach($field['options'] as $option)

                        <option value="{{$option}}">{{ $option}}</option>

                        @endforeach

                    </select>
                </div>

            @endif

            @if($field['type'] == "file")

                <div class="clearfix"></div>
                <label for="{{$field['name']}}">{{$field['title'][App::getLocale()]}}</label>
                <div class="clearfix"></div>
                <input style="padding:0" type="file" id="file_{{$form['code']}}" class="form-control" name="{{$field['name']}}">

            @endif
        
    	@endforeach
    	
        <div class="col-sm-8">
            <div class="form-group form-check type-check checkbox">
                <input type="checkbox" class="form-check-input" value="1" id="promozioni-chek1" name="privacy">
                <label class="label form-check-label lbl_filter text-left" for="promozioni-chek1"><a target="blank" href="{{ F::getUrl('Pages', 7) }}">{{ text('form_label_informativa', Array('it' => 'Dichiaro di aver preso visione dell’Informativa Privacy e ACCONSENTO al trattamento dei miei dati personali per finalità di marketing da parte di Sine Die.')) }}</a></label>
            </div>
        	<div style="transform:scale(0.77);-webkit-transform:scale(0.77);transform-origin:0 0;-webkit-transform-origin:0 0;" id="recaptcha{{ $form['code'] }}"></div>
        </div>

    	<div class="col-sm-4">
            <a id="submit-button" role="button" onclick="submitta('form_{{ $form['code'] }}');" class="btn btn-danger mt-5 px-5 py-3">{{ text('form_invia', Array('it' => 'Invia')) }}</a>
        </div>

    @else

        <div class="row">
            <div class="col-md-12">
              <div class="input-group">

                @foreach($form['fields'] as $field)
                @if($field['type'] == "email")

                <input class="form-control" type="text" id="email" @user value="{{ $_SESSION['user']['email'] }}" @enduser name="{{$field['name']}}" placeholder="{{$field['title'][App::getLocale()]}}">

                @endif
                @endforeach

                <div class="input-group-append">
                    <a id="submit-button" role="button" onclick="submitta('form_{{ $form['code'] }}');" class="btn btn-danger">{{ text('form_iscriviti', Array('it' => 'Iscriviti')) }}</a>
                </div>
            </div>
        </div>

        <div class="col-sm-12">
            <div class="form-group form-check type-check checkbox">
                <input type="checkbox" class="form-check-input" value="1" id="promozioni-chek1-nw" name="privacy">
                <label class="label form-check-label lbl_filter" for="promozioni-chek1-nw"><a href="{{ F::getUrl('Pages', 7) }}">{{ text('form_label_informativa', Array('it' => 'Dichiaro di aver preso visione dell’Informativa Privacy e ACCONSENTO al trattamento dei miei dati personali per finalità di marketing da parte di Sine Die.')) }}</a></label>
            </div>
            <div style="transform:scale(0.67);-webkit-transform:scale(0.67);transform-origin:0 0;-webkit-transform-origin:0 0; margin-top:10px;" id="recaptcha{{ $form['code'] }}"></div>
        </div>
    </div>

    @endif
    
    @if($form['code'] != "newsletter")  
    </div>
    @endif
</form>

<div style="margin-top:15px; list-style: none;" id="errors_{{$form['code']}}"></div>

<script>
    psubmit_form("form_{{$form['code']}}", "errors_{{$form['code']}}", null, null, function(data) {

        data = JSON.parse(data);    

        $("#errors_{{$form['code']}}").html(data.html);

        grecaptcha.reset(recaptcha{{ $form['code'] }});

    });
</script>