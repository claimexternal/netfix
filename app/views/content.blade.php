<?php
  
  $parentPage= F::byId('Pages', $page['parent_id']);

?>

<div class="hd-internal mt-3 pt-4 pb-5">
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="@urlHome">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">@pageTitle</li>
            </ol>
        </nav>
        <h1 class="pb-4">@pageTitle</h1>
    </div>
</div>

<section class="cnt-prodotto mt-5 py-5">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3 class="mt-4">@pageTitle</h3>
                @pageDescription

            </div>
        </div>
    </div>
</section>