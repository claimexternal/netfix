<table style="border:1px solid #dadada;width:100%;padding:30px">

	<thead>

		<tr>

			<th></th>
			<th style="text-align:center">Prodotto</th>
			<th style="text-align:center">Quantità</th>
			<th style="text-align:center">Prezzo</th>
			<th style="text-align:center"></th>

		</tr>

	</thead>

	<tbody>

		@foreach($order_items as $k => $item)

		<?php 

			$product = F::byId('Products', $item['product_id']);

		?>
			<tr style="paddding-bottom:10px;">

				<td style="padding:10px;text-align:center;border-bottom:1px solid #dadada"><img src="{{ I::getCopertine('Products', $item['product_id'], 80, 130)."/resize" }}" alt="{{ $product['title'] }}" /></td>

				<td style="text-align:center;border-bottom:1px solid #dadada">{{ $product['title'] }}</td>

				<td style="text-align:center;border-bottom:1px solid #dadada">{{ $item['quantity'] }}</td>

				<td style="text-align:center;border-bottom:1px solid #dadada">&euro;{{ $product['price'] }}</td>

				<td style="text-align:center;border-bottom:1px solid #dadada">{{ $item['note'] }}</td>

			</tr>

		@endforeach
	</tbody>

</table>