<div class="inner-header">
    <div class="container">
        <div class="row">
            <div class="col">
                <p class="text-center mt-2"><a href="@urlHome">Home</a> / @pageTitle</p>
                <h2 class="text-center mt-3">@pageTitle</h2>
            </div>
        </div>
    </div>
</div>

<section class="cnt-prodotti mt-5">
    <div class="container py-5">
        <div class="row">
            <div class="col-12">
                <h3>Pagina non trovata</h3>
                <p>Esegui un'altra ricerca</p>
            </div>
        </div>
    </div>
</section>