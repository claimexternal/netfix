<!-- fix footer -->
<?php $partner = F::byId('Pages', 23, array('title','description')) ?>

<section class="box-partner py-5">
    <div class="container">
        <div class="row">
            <div class="col-md-2">
                {{ $partner['description'] }}
            </div>
            <div class="col-md-10">
                <ul class="clearfix">
                    
                    @cicle('Pages,parent_id='.$partner['id'].',title-shortdescription,ordination-asc')
                    
                    <li class="float-left"><a href="@rowShortDescription" target="_blank"><img src="@rowImage('0,0')" alt="@rowTitle"></a></li>
                    
                    @endcicle
                    
                </ul>
            </div>
        </div>
    </div>
</section>

<footer class="py-5">
    <div class="container">
        <div class="row">
            <div class="col-md-10">
                <p><strong>{{ text('text_copyright', array('it' => '© 2020 Expyou S.r.l. - www.expyou.com')) }}</strong></p>
                <p>{{ text('text_footer', array('it' => 'Via dell’industria 14, 35020 Brugine (PD) - Italia - T. 049 5463216 - info@expyou.com')) }}</p>
            </div>
            <div class="col-md-2">
                <ul class="float-right">
                    <li><a href="/privacy-policy">Privacy Policy</a></li>
                    <li><a href="https://www.iubenda.com/privacy-policy/92138503/cookie-policy" target="_blank" rel="noopener nofollow">Cookie Policy</a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>

<!-- Return to Top -->
<a href="javascript:" id="return-to-top"><i class="fas fa-angle-up"></i></a>

<!-- Cookies -->
{{-- <script src="@asset('cookies/cookiechoices.js')"></script>
<link href="@asset('cookies/cookiechoices.css')" rel="stylesheet"> --}}

{{-- <script src="@asset('js/jquery-3.4.1.slim.min.js')"></script> --}}
<script src="@asset('js/popper.min.js')"></script>
<script src="@asset('js/bootstrap.min.js')"></script>
<script src="@asset('js/aos.js')"></script>
<script src="@asset('js/scripts.js')"></script>

<script>
    AOS.init();

</script>

<!-- non togliere mai -->
@if(isset($page['formspage']))

<script class="_iub_cs_activate" type="text/plain" data-iub-purposes="1" suppressedsrc='https://www.google.com/recaptcha/api.js?hl={{ App::getLocale() }}&onload=multiplesReca&render=explicit' async defer></script>
<script>

function multiplesReca() {

  @foreach($page['formspage'] as $code)
    var recaptcha$code;
  @endforeach

  @foreach($page['formspage'] as $code)

    recaptcha{{ $code }} = grecaptcha.render('recaptcha{{ $code }}', {

      'sitekey' : '{{ LabConfig::$labconfigs['recaptchakey']['public'] }}'

    });

  @endforeach

}

</script>

@endif

</body>

</html>
