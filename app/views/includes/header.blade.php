<!DOCTYPE html>
<html lang="{{ App::getLocale() }}">

<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>@if($page['metatitle'] != '') @pageMetaTitle @else @pageTitle @endif</title>
<meta name="description" content="@pageMetaDescription">
<link rel="shortcut icon" type="image/x-icon" href="@asset('images/favicon.png')">
<link rel="shortcut icon" href="@asset('images/favicon.ico')" type="image/x-icon">
<link rel="icon" href="@asset('images/favicon.ico')" type="image/x-icon">

<link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;0,800;1,300;1,400;1,600;1,700;1,800&display=swap" rel="stylesheet">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.10.1/css/all.css" crossorigin="anonymous">
<link rel="stylesheet" href="@asset('css/bootstrap.min.css')" crossorigin="anonymous">
<link rel="stylesheet" href="@asset('css/style.css?v=1.1')">
<link rel="stylesheet" href="@asset('css/responsive.css?v=1.1')">
<link rel="stylesheet" href="@asset('css/aos.css')">

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script id="scriptFile" lang="{{ App::getLocale() }}" src="@asset('js/script.js')?v=1.1"></script>
<script type="text/javascript" src="@asset('js/jquery.autocomplete.js')"></script>


<!-- Global site tag (gtag.js) - Google Analytics -->
<script class="_iub_cs_activate" type="text/plain" data-iub-purposes="4" async suppressedsrc="https://www.googletagmanager.com/gtag/js?id=UA-177755179-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-177755179-1');
</script>

{{-- iubenda --}}
<script type="text/javascript">
var _iub = _iub || [];
_iub.csConfiguration = {"invalidateConsentWithoutLog":true,"consentOnContinuedBrowsing":false,"whitelabel":false,"lang":"it","floatingPreferencesButtonDisplay":false,"siteId":2501072,"perPurposeConsent":true,"cookiePolicyId":92138503, "banner":{ "closeButtonRejects":true,"acceptButtonDisplay":true,"customizeButtonDisplay":true,"explicitWithdrawal":true,"position":"float-center" }};
</script>
<script type="text/javascript" src="//cdn.iubenda.com/cs/iubenda_cs.js" charset="UTF-8" async></script>

</head>
<body>

<header class="mt-3">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <a class="navbar-brand" href="@urlHome"><img src="@asset('images/logo-netfix.png')" alt="NetFix" class="img-fluid"></a>
            </div>
            <div class="col-md-8">
                <nav class="navbar navbar-expand-lg navbar-light mt-3">
                    <button class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse float-right" id="navbarSupportedContent">
                        <ul class="navbar-nav ml-auto">                            
                            <li class="nav-item"><a class="nav-link" href="@urlHome#comefunziona">Come funziona</a></li>
                            <li class="nav-item"><a class="nav-link" href="@urlHome#comeinstalla">Come si installa</a></li>
                            <li class="nav-item"><a class="nav-link" href="@urlHome/come-scegliere">Come scegliere</a></li>
                            <li class="nav-item"><a class="nav-link" href="@urlHome#plus">Plus</a></li>
                            <li class="nav-item"><a class="nav-link" href="@urlHome#componenti">Componenti</a></li>
                            <li class="nav-item"><a class="nav-link" href="@urlHome#contatti">Contatti</a></li>                            
                        </ul>
                    </div>
                </nav>
            </div>
            <div class="d-none d-md-block col-md-1 text-right">
                <p class="mt-3">Branded by</p>
                <img src="images/logo-expyou.png" alt="ExpYou" class="img-fluid">
            </div>
        </div>
    </div>
</header>