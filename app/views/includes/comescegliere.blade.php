<div class="hd-internal mt-3 pt-4 pb-5">
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="@urlHome">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">@pageTitle</li>
            </ol>
        </nav>
        <h1 class="pb-4">&nbsp;</h1>
    </div>
</div>

<section class="cnt-comescegliere mt-5 py-5">
    <div class="container">
        <h1>@pageTitle</h1>
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4">
                <h4 class="d-none d-sm-block mt-3">Versione H3</h4>
            </div>
            <div class="col-md-4">
                <h4 class="d-none d-sm-block mt-3">Versione H4</h4>
            </div>
            <div class="col-md-4">
                <h3 class="mt-5">1. Misura la testa della barriera</h3>
            </div>
            <div class="col-md-4">
                <h4 class="d-block d-sm-none mt-3">Versione H3</h4>
                <img src="@asset('images/barriera-h3-1.jpg')" alt="Barriera H3" class="mt-5 img-fluid">
            </div>
            <div class="col-md-4">
                <h4 class="d-block d-sm-none mt-3">Versione H4</h4>
                <img src="@asset('images/barriera-h4-1.jpg')" alt="Barriera H4" class="mt-5 img-fluid">
            </div>
            <div class="col-md-4">
                <h3 class="mt-5">2. Scegli la staffa corrispondente</h3>
            </div>
            <div class="col-md-4">
                <h4 class="d-block d-sm-none mt-3">Versione H3</h4>
                <img src="@asset('images/barriera-h3-2.jpg')" alt="Barriera H3" class="mt-5 img-fluid">
            </div>
            <div class="col-md-4">
                <h4 class="d-block d-sm-none mt-3">Versione H4</h4>
                <img src="@asset('images/barriera-h4-2.jpg')" alt="Barriera H4" class="mt-5 img-fluid">
            </div>
            <div class="col-md-4">
                <h3 class="mt-5">3. Aggiungi gli accessori mancanti</h3>
            </div>
            <div class="col-md-4">
                <h4 class="d-block d-sm-none mt-3">Versione H3</h4>
                <img src="@asset('images/barriera-h3-3.jpg')" alt="Barriera H3" class="mt-5 img-fluid">
            </div>
            <div class="col-md-4">
                <h4 class="d-block d-sm-none mt-3">Versione H4</h4>
                <img src="@asset('images/barriera-h4-3.jpg')" alt="Barriera H4" class="mt-5 img-fluid">
            </div>
            <div class="col-md-4">
                <h3 class="mt-5">4. Aggiungi gli accessori mancanti</h3>
            </div>
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-4">
                        <img src="@asset('images/accessori-1.jpg')" alt="Accessori Barriera" class="mt-5 img-fluid">
                    </div>
                    <div class="col-md-4">
                        <img src="@asset('images/accessori-2.jpg')" alt="Accessori Barriera" class="mt-5 img-fluid">
                    </div>
                    <div class="col-md-4">
                        <img src="@asset('images/accessori-3.jpg')" alt="Accessori Barriera" class="mt-5 img-fluid">
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <h3 class="mt-5 no-line">5. Hai creato il tuo kit!</h3>
            </div>
            <div class="col-md-3">
                <a href="#" class="btn btn-danger px-4 py-3 mt-5" data-toggle="modal" data-target="#modalPreventivo">{{ text('link_richiedi_un_preventivo', array('it' => 'Richiedi un preventivo')) }}</a>
                
                <!-- Modal -->
                <div class="modal fade" id="modalPreventivo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">{{ text('link_richiedi_un_preventivo', array('it' => 'Richiedi un preventivo')) }}</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                @drawForm('form_comescegliere')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-5"></div>
        </div>
    </div>
</section>