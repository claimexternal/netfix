@include('includes.header')

<div id="carouselExampleCaptions" class="carousel slide mt-4" data-ride="carousel">
    <div class="carousel-inner">

        <?php $i = 0; ?>

        @cicle('Slides,1=1,title-link-subtitle-slidedescription,ordination-asc')

        <div class="carousel-item @if($i == 0) active @endif">
            <img src="@rowImage('0,0')" class="d-block w-100" alt="@rowTitle">
            <div class="carousel-caption">
                <div class="container">
                    <div class="row">
                        <div class="col-8 col-md-4">
                            <h1>@rowTitle</h1>
                            <p>@rowSubTitle</p>
                            <a href="@rowLink" class="btn btn-danger px-4 py-3 mt-4" data-toggle="modal" data-target="#modalPreventivo{{$row['id']}}">{{ text('link_richiedi_un_preventivo', array('it' => 'Richiedi un preventivo')) }}</a>
                        </div>
                        <div class="col-md-8"></div>
                    </div>
                </div>
            </div>
        </div>        

        <!-- Modal -->
        <div class="modal fade" id="modalPreventivo{{$row['id']}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="z-index:999999 !important;">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">{{ text('link_richiedi_un_preventivo', array('it' => 'Richiedi un preventivo')) }}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        @drawForm('form_preventivo')
                    </div>
                </div>
            </div>
        </div>

        <?php $i++ ?>

        @endcicle
        
    </div>
    <!--<a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>-->
</div>

<?php $comefunziona = F::byId('Pages', 9, array('title','description')) ?>
@if($comefunziona != '')
<section class="box-funziona my-5" id="comefunziona">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h2 class="mt-5 pt-5">{{ $comefunziona['title'] }}</h2>
                <div class="mt-4">
                    {{ $comefunziona['description'] }}
                </div>
                <div class="row ml-5 pl-1">
                    <div class="col-md-4">
                        <img src="images/link-articolo-brevettato.jpg" alt="Articolo brevettato" class="button img-fluid">
                    </div>
                    <div class="col-6 col-md-4">
                        <img src="images/logo-giordano.gif" alt="Istituto Giordano" class="img-fluid mt-1">
                    </div>
                    <div class="col-6 col-md-4">
                        <img src="images/logo-aisico.gif" alt="Istituto Giordano" class="img-fluid mt-1">
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div id="carouselExampleControls" class="carousel slide" data-pause="false" data-ride="carousel" data-interval="3500">
                    <div class="carousel-inner">
                       
                        <?php $images = I::getImages("Pages", $comefunziona["id"], 0, 0, "resize"); ?>
                        <?php $ii = 0; ?>

                        @if( count($images) > 0 )

                        @foreach($images as $row)


                        <div class="carousel-item @if($ii == 0) active @endif">
                          <img src="{{ I::getImageById($row['id'], 0, 0) }}" class="d-block w-100" alt="@rowTitle">
                        </div>
                        
                        <?php $ii++ ?>

                        @endforeach

                        @endif
                        
                    </div>
                  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                      <span class="sr-only">Previous</span>
                  </a>
                  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                      <span class="carousel-control-next-icon" aria-hidden="true"></span>
                      <span class="sr-only">Next</span>
                  </a>
                  </div>
                <!--<img src="{{ I::getCopertine('Pages', $comefunziona['id'], 0, 0) }}" alt="{{ $comefunziona['title'] }}" class="img-fluid">-->
            </div>
        </div>
    </div>
</section>
@endif

<section class="box-barriera mt-5 py-5">
    <div class="container">
        <div class="row">
            <div class="col-md-2 text-center">
                <img src="@asset('images/double-arrow-right.png')" class="mt-3">
            </div>
            <div class="col-md-8">
                <h3><a href="/come-scegliere">{{ text('text_scopri_come_scegliere_componenti', array('it' => '<span>Scopri come scegliere i componenti</span> adatti alla tua barriera New Jersey')) }}</a></h3>
            </div>
            <div class="col-md-2 text-center">
                <img src="@asset('images/double-arrow-left.png')" class="mt-3">
            </div>
        </div>
    </div>
</section>

<?php $componenti = F::byId('Pages', 12, array('title','description')) ?>
@if($componenti != '')
<section class="box-componenti py-5" id="componenti">
    <div class="container">
        <h2 class="mt-4">{{ $componenti['title'] }}</h2>
        <div class="list-componenti">
            <div class="row">
                
                @cicle('Products,1=1,title-shortdescription,ordination-asc')
                
                <div class="col-md-4">
                    <div class="box clearfix">
                        <a href="@rowUrl"><i class="far fa-plus-square"></i></a>
                        <div class="box-img">
                            <a href="@rowUrl"><img src="@rowImage('0,0')" alt="@rowTitle" class="float-right"></a>
                        </div>
                    </div>
                    <p><a href="@rowUrl">@rowTitle</a></p>
                </div>
                
                @endcicle
                
            </div>
        </div>
    </div>
</section>
@endif

<?php $comeinstalla = F::byId('Pages', 10, array('title','description')) ?>
@if($comeinstalla != '')
<section class="box-installa" id="comeinstalla">
    <div class="container">
        <h2>{{ $comeinstalla['title'] }}</h2>
        <div class="row mt-5">
            <div class="col-md-12">
                
                @cicle('Pages,parent_id='.$comeinstalla['id'].',title-description-shortdescription,ordination-asc')
                
                <div class="box mt-3 clearfix"><span>@rowShortDescription</span>@rowDescription</div>
                
                @endcicle
                
            </div>
        </div>
    </div>
</section>
@endif

<?php $plus = F::byId('Pages', 11, array('title','description')) ?>
@if($plus != '')
<section class="box-plus mt-5 pt-5" id="plus">
    <div class="container">
        <h2>{{ $plus['title'] }}</h2>
        <div class="row">
            
            @cicle('Pages,parent_id='.$plus['id'].',title-description,ordination-asc')
            
            <div class="col-md-3">
                <div class="box mt-5 py-5 px-4 {{ $row['code'] }}">
                    <h3>@rowTitle</h3>
                    @rowDescription
                </div>
            </div>
            
            @endcicle
            
        </div>
    </div>
</section>
@endif

<?php $contatti = F::byId('Pages', 13, array('title','description')) ?>
@if($contatti != '')
<section class="box-form mt-5 py-5" id="contatti">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <h2 class="mt-4">{{ $contatti['title'] }}</h2>
                <div class="mt-4">
                    {{ $contatti['description'] }}
                </div>
            </div>
            <div class="col-md-8">
                @drawForm('form_contatti')
            </div>
        </div>
    </div>
</section>
@endif

@include('includes.footer')
