<?php $componenti = F::byId('Pages', 12, array('title','description')) ?>

<div class="hd-internal mt-3 pt-4 pb-5">
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="@urlHome">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">{{ $componenti['title'] }}</li>
            </ol>
        </nav>
        <h1 class="pb-4">@pageTitle</h1>
    </div>
</div>

<section class="cnt-prodotto mt-5 py-5">
    <div class="container">
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-4">
                <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        
                        <?php $images = I::getImages("Products", $page["id"], 0, 0, "resize"); ?>
                        <?php $i = 0; ?>

                        @if( count($images) > 0 )

                        @foreach($images as $row)


                        <div class="carousel-item @if($i == 0) active @endif">
                          <img src="{{ I::getImageById($row['id'], 0, 0) }}" class="d-block w-100" alt="@rowTitle">
                        </div>
                        
                        <?php $i++ ?>

                        @endforeach

                        @endif

                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
            <div class="col-md-6">
                <h3 class="mt-4">@pageTitle</h3>
                @pageDescription
                <a href="#" class="btn btn-danger px-4 py-3 mt-4" data-toggle="modal" data-target="#modalPreventivo">{{ text('link_richiedi_un_preventivo', array('it' => 'Richiedi un preventivo')) }}</a>

                <!-- Modal -->
                <div class="modal fade" id="modalPreventivo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">{{ text('link_richiedi_un_preventivo', array('it' => 'Richiedi un preventivo')) }}</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                @drawForm('form_preventivo')
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-md-1"></div>
        </div>        
        
        <?php if($page['id'] == '2'|| $page['id'] == '4') { ?>
        
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-10">
                <h4 class="mt-5"><strong>{{ text('title_come_scegliere_la_staffa', array('it' => 'Come scegliere la staffa corretta?')) }}</strong></h4>
                <p>{{ text('text_come_scegliere_la_staffa', array('it' => 'Misura la larghezza di testa della barriera New Jersey che possiedi e confrontala con il disegno dimensionale della nostra staffa.')) }}</p>
                <div class="text-center"><img src="@asset('images/misure-staffa_'.$page['id'].'.jpg')" class="img-fluid mt-5"></div>
            </div>
            <div class="col-md-1"></div>
        </div>
        
       <?php } elseif($page['id'] == '1' || $page['id'] == '3') { ?>
        
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-10">
                <h4 class="mt-5"><strong>{{ text('title_come_scegliere_lo_zoccolo', array('it' => 'Come scegliere lo zoccolo corretto?')) }}</strong></h4>
                <p>{{ text('text_come_scegliere_lo_zoccolo', array('it' => 'Misura la larghezza di testa della barriera New Jersey che possiedi e confrontala con il disegno dimensionale del nostro zoccolo.')) }}</p>
                <div class="text-center"><img src="@asset('images/misure-zoccolo.jpg')" class="img-fluid mt-5"></div>
            </div>
            <div class="col-md-1"></div>
        </div>
        
       <?php } ?>
        
        <div class="row mt-5 border-bottom pb-5">
            <div class="col-md-1"></div>
            <div class="col-md-8">
                <div class="box-brochure clearfix py-4 px-5 mt-5">
                    <h4 class="float-left">Per maggiori informazioni<br>
                        <strong>scarica la brochure completa</strong></h4>
                    <a href="@asset('pdf/brochure.pdf')" class="btn btn-danger px-5 py-3 mt-3 float-right" target="_blank">Scarica</a>
                </div>
            </div>
            <div class="col-md-2 d-none d-sm-block">
                <img src="@asset('images/brochure.jpg')" alt="Brochure">
            </div>
            <div class="col-md-1"></div>
        </div>
    </div>
</section>

<section class="box-barriera mt-5 py-5">
    <div class="container">
        <div class="row">
            <div class="col-md-3 text-center">
                <img src="@asset('images/double-arrow-right.png')" class="mt-3">
            </div>
            <div class="col-md-6">
                <h3><a href="/come-scegliere">{{ text('text_scopri_come_scegliere_componenti', array('it' => '<span>Scopri come scegliere i componenti</span> adatti alla tua barriera New Jersey')) }}</a></h3>
            </div>
            <div class="col-md-3 text-center">
                <img src="@asset('images/double-arrow-left.png')" class="mt-3">
            </div>
        </div>
    </div>
</section>
