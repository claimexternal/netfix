<?php

//Chiamate GET
class Frontmappins {

	static function loadmarkers() {

		App::setLocale('it');

		$state = Input::get('state');

		$markers = F::byWhere('MapPins', 'country != "Italia"', array('title', 'description'));
		$array = array();
		
		foreach($markers as $k => $marker) {

			$markerMap['src'] = I::getCopertine(get_class($marker), $marker['id'], 100, 100);

			$marker['description'] = str_replace("Tel.", "<br/>Tel.", $marker['description']);
			$marker['description'] = str_replace("Tel:", "<br/>Tel:", $marker['description']);
			$marker['description'] = str_replace("Fax.", "<br/>Fax.", $marker['description']);
			$marker['description'] = str_replace("Cell.", "<br/>Cell.", $marker['description']);
			$marker['description'] = str_replace("Zona:", "<br/>Zona:", $marker['description']);
			$marker['description'] = str_replace("Email:", "<br/>Email:", $marker['description']);

			$markerMap['description']['it'] = preg_replace('/([a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6})/', '<b><a href="mailto:$1">$1</a></b>', $marker['description']);
			$markerMap['title']['it'] = $marker['title'];
			$markerMap['lat'] = $marker['lat'];
			$markerMap['long'] = $marker['long'];

			//$marker['description']['it'] = preg_replace('/([a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6})/', '<a href="mailto:$1">$1</a>', $marker['description']);

			$array[$k] = $markerMap;

		}

		echo "var pins =".json_encode($array);

	}

	static function loaditalymarkers() {

		App::setLocale('it');

		$state = Input::get('state');

		$markers = F::byWhere('MapPins', 'country = "Italia"', array('title', 'description'));
		$array = array();

		foreach($markers as $k => $marker) {

			$markerMap['src'] = I::getCopertine(get_class($marker), $marker['id'], 100, 100);

			$marker['description'] = str_replace("Tel.", "<br/>Tel.", $marker['description']);
			$marker['description'] = str_replace("Tel:", "<br/>Tel:", $marker['description']);
			$marker['description'] = str_replace("Fax.", "<br/>Fax.", $marker['description']);
			$marker['description'] = str_replace("Cell.", "<br/>Cell.", $marker['description']);
			$marker['description'] = str_replace("Zona:", "<br/>Zona:", $marker['description']);
			$marker['description'] = str_replace("Email:", "<br/>Email:", $marker['description']);

			$markerMap['description']['it'] = preg_replace('/([a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6})/', '<b><a href="mailto:$1">$1</a></b>', $marker['description']);
			$markerMap['title']['it'] = $marker['title'];
			$markerMap['lat'] = $marker['lat'];
			$markerMap['long'] = $marker['long'];

			//$marker['description']['it'] = preg_replace('/([a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6})/', '<a href="mailto:$1">$1</a>', $marker['description']);

			$array[$k] = $markerMap;

		}

		echo "var pins =".json_encode($array);

	}

}
//Chiamate GET
class FrontmappinsPost {

	static function loadcountry($p) {
		
		App::setLocale('it');

		$country = Input::get('country');

		$markers = F::byWhere('MapPins', 'country = "'.$country.'"', array('title', 'description'));
		$array = array();

		foreach($markers as $k => $marker) {

			$markerMap['src'] = I::getCopertine(get_class($marker), $marker['id'], 100, 100);

			$marker['description'] = str_replace("Tel.", "<br/>Tel.", $marker['description']);
			$marker['description'] = str_replace("Tel:", "<br/>Tel:", $marker['description']);
			$marker['description'] = str_replace("Fax.", "<br/>Fax.", $marker['description']);
			$marker['description'] = str_replace("Cell.", "<br/>Cell.", $marker['description']);
			$marker['description'] = str_replace("Zona:", "<br/>Zona:", $marker['description']);
			$marker['description'] = str_replace("Email:", "<br/>Email:", $marker['description']);

			$markerMap['description']['it'] = preg_replace('/([a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6})/', '<b><a href="mailto:$1">$1</a></b>', $marker['description']);
			$markerMap['title']['it'] = $marker['title'];
			$markerMap['lat'] = $marker['lat'];
			$markerMap['long'] = $marker['long'];

			//$marker['description']['it'] = preg_replace('/([a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6})/', '<a href="mailto:$1">$1</a>', $marker['description']);

			$array[$k] = $markerMap;

		}

		echo json_encode($array);

	}

	static function loadstate($p) {

		App::setLocale('it');
		
		$state = Input::get('state');

		$markers = F::byWhere('MapPins', 'state = "'.$state.'"', array('title', 'description'));
		$array = array();

		foreach($markers as $k => $marker) {

			$markerMap['src'] = I::getCopertine(get_class($marker), $marker['id'], 100, 100);

			$marker['description'] = str_replace("Tel.", "<br/>Tel.", $marker['description']);
			$marker['description'] = str_replace("Tel:", "<br/>Tel:", $marker['description']);
			$marker['description'] = str_replace("Fax.", "<br/>Fax.", $marker['description']);
			$marker['description'] = str_replace("Cell.", "<br/>Cell.", $marker['description']);
			$marker['description'] = str_replace("Zona:", "<br/>Zona:", $marker['description']);
			$marker['description'] = str_replace("Email:", "<br/>Email:", $marker['description']);

			$markerMap['description']['it'] = preg_replace('/([a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6})/', '<b><a href="mailto:$1">$1</a></b>', $marker['description']);
			$markerMap['title']['it'] = $marker['title'];
			$markerMap['lat'] = $marker['lat'];
			$markerMap['long'] = $marker['long'];

			//$marker['description']['it'] = preg_replace('/([a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6})/', '<a href="mailto:$1">$1</a>', $marker['description']);

			$array[$k] = $markerMap;

		}

		echo json_encode($array);

	}

}