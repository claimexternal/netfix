<?php

//Chiamate GET
class Frontcart {

	public static $slugs = Array(

		"it" => Array(

			'view' => 'visualizza-carrello',
			'checkout' => 'effettua-checkout',
			'success' => 'pagamento-effettuato',
			'cancel' => 'pagamento-cancellato',

		),

		"en" => Array(

			'view' => 'view',
			'checkout' => 'checkout',
			'success' => 'payment-success',
			'cancel' => 'payment-cancel',

		)

	);

	static function init() {

		if (!isset($_SESSION['cart'])) {

				$cart = array(

					"total" => "0,00",
					"items" => array(), // key => array( productid, modelli e sizes, qtà)
					"fees" => "7,00"

				);

				$_SESSION['cart'] = $cart;
			    
		}

	}

	static function success() {

		$order = Orders::whereRaw('payment_code ="'.$_GET['paymentId'].'"')->first();

		if(!$order)

			die('Ordine non trovato.');

		$payment = PaymentMethods::find($order['payment_id']);

		if($payment['code'] == 'paypal') {

			if(!labpaypal::confirm($_GET)) 
				
				die("Errore nel pagamento");

		}

        $dt = new DateTime();

        $order->payed_on = DB::raw('CURRENT_TIMESTAMP');
        $order->save();

        if(isset($_SESSION['cart']['discountcode'])) {

			$promotionCode = PromotionsCodes::with('promotions.title')->whereHas('promotions', function( $promotion ) {	
				
				$promotion->whereRaw('online = 1 and start_date <="'.date("Y-m-d", time()).'" and end_date >="'.date("Y-m-d", time()).'"');

			})->whereRaw("used_on != '0' and code ='".$_SESSION['cart']['discountcode']['code']."'")->first();

	        $promotionCode->used_on = DB::raw('CURRENT_TIMESTAMP');
	        $promotionCode->used_by = $_SESSION['user']['id'];

	        $promotionCode->save();

    	}

    	$cart = array(

					"total" => "0,00",
					"items" => array(), // key => array( productid, modelli e sizes, qtà)
					"fees" => "7,00"

				);

		$_SESSION['cart'] = $cart;
        
        $orders_products = OrdersProducts::where('order_id', $order['id'])->get();

        foreach($orders_products as $order_product) {

        	$models = unserialize($order_product['models']);

        	foreach($models as $model) {

        		$modelObject = ProductsModels::find($model);
        		$modelObject->stock = $modelObject['stock'] - $order_product['quantity'];

        		$modelObject->save();

        	}

        }

    	$replace = array(

			"##NAME##" => $_SESSION['user']['name'],
			"##ORDER_ID##" => $order['id']

		);

		e::send('orderpayed', $_SESSION['user']['email'], $_SESSION['user']['name'], $replace);

		$contact = LabConfig::get('admincontacts', F::getenv());
		
		$data['order_items'] = $orders_products;
    	
    	$replaceadmin = array(

			"##NAME##" => $contact['name'],
			"##USER##" => "<a href='mailto:".$_SESSION['user']['email']."'>".$_SESSION['user']['name']."</a>",
			"##ORDER_ITEMS##" => View::make( 'emails.itemstable', $data )->render(),
			"##ORDER_TOTAL##" => $order['total'],
			"##SHIPPING_FEES##" => $order['shipping_fees']."&euro;"

		);

		e::send('orderconfirmadmin', 'ordini@sonten.com', $contact['name'], $replaceadmin);

        $tp['page'] = F::byId('Pages', 33, array('title', 'metatitle', 'description'));
        echo View::make( 'skeleton', $tp )->nest('content', 'includes.paymentsuccess', $tp)->render();


	}

	static function view() {

		$tp['page'] = F::byId('Pages', 31, array('title', 'metatitle', 'description'));

		//Disegno scheletro e contenuto 
		echo View::make( 'skeleton', $tp )->nest('content', 'includes.cartreview', $tp)->render();


	}

	static function checkout() {

		$tp['page'] = F::byId('Pages', 32, array('title', 'metatitle', 'description'));

		$_SESSION['checkout'] = 1;

		if(count($_SESSION['cart']['items']) > 0)

			echo View::make( 'skeleton', $tp )->nest('content', 'includes.checkout', $tp)->render();
		
		else

			F::redirect(F::getUrl('Pages', 13), 1);

	}

}

//Chiamate POST
class FrontcartPost {

	static function success($p) {

			echo "POST";

			F::printer($p);

	}

	static function payorder($p) {

		$order = Orders::find($p['id']);
    	$orders_products = OrdersProducts::where('order_id', $order['id'])->get();
    	
    	$payment = F::byId("PaymentMethods", $p['payment_id'], array('title', 'description'));
		
    	if($payment['code'] == "paypal") {

    		//Stampo metodo di pagamento
    		$url = labpaypal::init($order, $orders_products, "Pagamento ordine numero ".$order->id, "Esegui la transazione per confermare l'ordine");

    		echo $payment['description'];

    		$order->payment_code = $url->id;
    		$order->save();
    		
    		echo "<div class='alert alert-success'>Metodo di pagamento accettato.</div><script>window.location.href = '".$url->getApprovalLink()."';</script>";
    	
    	}	

    	if($payment['code'] == "carta") {

    		//Stampo metodo di pagamento
    		$url = labpaypal::init($order, $orders_products, "Pagamento ordine numero ".$order->id, "Esegui la transazione per confermare l'ordine");

    		echo $payment['description'];

    		$order->payment_code = $url->id;
    		$order->save();
    		
    		echo "<div class='alert alert-success'>Metodo di pagamento accettato.</div><script>window.location.href = '".$url->getApprovalLink()."';</script>";
    	
    	}

	}

	static function loadtotal($p) {

		echo View::make( 'includes.total' )->render();

	}

	static function estimatefees($p) {

		if(isset(LabConfig::$labconfigs['countryfees'][$p['country']])) {

			$_SESSION['cart']['fees'] = LabConfig::$labconfigs['countryfees'][$p['country']];

			//Sono nel checkout
			if(isset($_GET['checkout'])) {

				$array['html'] = LabConfig::$labconfigs['countryfees'][$p['country']]."&euro;";
				$array['total'] = View::make( 'includes.ordertable' )->render();

			//Sono nel carrello
			} else {

				$array['html'] = "Spese di spedizione: <strong>".LabConfig::$labconfigs['countryfees'][$p['country']]."&euro;</strong>";
				$array['fees'] = LabConfig::$labconfigs['countryfees'][$p['country']];
				$array['total'] = View::make( 'includes.total' )->render();

			}

			echo json_encode($array);

		}

	}

	static function estimatediscount($p) {

		$promotionCode = PromotionsCodes::with('promotions.title')->whereHas('promotions', function( $promotion ) {	
			
			$promotion->whereRaw('online = 1 and start_date <="'.date("Y-m-d", time()).'" and end_date >="'.date("Y-m-d", time()).'"');

		})->whereRaw("used_on != '0' and code ='".$p['code']."'")->first();

		if($promotionCode) {
			
			//Applico sconto al totale se non è già stato fatto
			if(!isset($_SESSION['cart']['discountcode'])) {
				
				//Salvo in sessione il codice
				$_SESSION['cart']['discountcode'] = array(

					'discount' => $promotionCode['promotions']['discount'],
					'code' => $p['code']

				);
			}

			//Sono nel checkout
			if(isset($_GET['checkout'])) {

				$array['html'] = "Promozione: <strong>".$promotionCode['promotions']['title'][App::getLocale()]."</strong><br/>Sconto: <strong>".$promotionCode['promotions']['discount']."&euro;</strong>";
				$array['total'] = View::make( 'includes.ordertable' )->render();

			//Sono nel carrello
			} else {

				$array['html'] = "Promozione: <strong>".$promotionCode['promotions']['title'][App::getLocale()]."</strong><br/>Sconto: <strong>".$promotionCode['promotions']['discount']."&euro;</strong>";
				$array['total'] = View::make( 'includes.total' )->render();

			}

		} else {

			$array['html'] = "Codice sconto invalido.";
			$array['total'] = View::make( 'includes.total' )->render();

		}

		echo json_encode($array);

	}

	static function insert( $p ) {
		
		$product = F::byId('Products', $p['product_id']);

		$total = F::money('tostring', (F::money('tonumber', $product['price']) * $p['quantity']) + F::money('tonumber', $_SESSION['cart']['total']));

//		$topModel = F::byId('ProductsModels', $p['customdata']['topModel'], array('title'));
//		$bottomModel = F::byId('ProductsModels', $p['customdata']['bottomModel'], array('title'));

		//Se c'è disponibilità per il prodotto
		if($product['stock'] > 0) {

			//Aggiungo prodotto al carrello
			$productArray = array(

				"product_id" => $p['product_id'],
				"quantity" => $p['quantity'],
				//"customdata" => $p['customdata']

			);

			//Aggiungo elemento al carrello
			$_SESSION['cart']['items'][] = $productArray;

			//Aggiorno totatle carrello
			$_SESSION['cart']['total'] = $total;

			//Sostituisco titolo popup carrello
			$data['title'] = $product['title'];
			

		} else {

			$data['nostock'] = 1;
			$data['title'] = text('carrello_scorte_esaurite', array('it' => 'Abbiamo finito le scorte per questo prodotto. Ci scusiamo per il disagio.'))."<br>".$product['title'];

		}

		$data['html'] = View::make('includes.cart')->render();
		$data['items'] = count($_SESSION['cart']['items']);
		
		$data['src'] = I::getCopertine('Products', $product['id'], 200, 200);

		echo json_encode($data);

	}

	static function deleteitem( $p ) {

		if(isset($_SESSION['cart']['items'][$p['index']])) {

			$product = Products::find($_SESSION['cart']['items'][$p['index']]['product_id']);
			$total = F::money('tonumber', $_SESSION['cart']['total']);

			$tosub = F::money('tonumber', $product['price']) * $_SESSION['cart']['items'][$p['index']]['quantity'];

			$_SESSION['cart']['total'] = F::money('tostring', ($total - $tosub));

			unset($_SESSION['cart']['items'][$p['index']]);

			$data['html'] = View::make( 'includes.cart' )->render();
			$data['items'] = count($_SESSION['cart']['items']);

			echo json_encode($data);

		}

	}

	static function deleteitemview( $p ) {

		if(isset($_SESSION['cart']['items'][$p['product']])) {

			$product = Products::find($p['product']);
			$total = F::money('tonumber', $_SESSION['cart']['total']);

			$tosub = F::money('tonumber', $product['price']) * $_SESSION['cart']['items'][$p['product']];

			$_SESSION['cart']['total'] = F::money('tostring', ($total - $tosub));

			unset($_SESSION['cart']['items'][$p['product']]);

			$data = View::make( 'Front::snippet.total' )->render();

			echo $data;

		}

	}

	static function confirm($p){

		$rules = array(

			'telephone' => 'required|min:8|regex:/^([0-9\s\-\+\(\)]*)$/',
			'address' => 'required|min:5',
			'city' => 'required',
			'post_code' => 'required|min:3',
			'province' => 'required|max:2',
			'country' => 'required|min:3',
			'cf' => 'required|min:16|max:16',
			'policy' => 'required',
			'payment_id' => 'required',

		);

		$messages = array(

			'telephone.required' => 'Il numero di telefono è obbligatorio',
			'telephone.min' => 'Il numero di telefono è troppo corto',
			'telephone.regex' => 'Inserisci un numero di telefono corretto',
			'address.required' => 'L\'indirizzo è obbligatorio',
			'address.min' => 'L\'indirizzo deve avere minimo 5 caratteri',
			'city.required' => 'La città è obbligatoria',
			'city.min' => 'La città deve avere minimo 5 caratteri',
			'post_code.required' => 'Il CAP è obbligatorio',
			'post_code.min' => 'Il CAP deve avere minimo 3 caratteri',
			'province.required' => 'La provincia è obbligatoria',
			'province.max' => 'La provincia deve avere massimo 2 caratteri',
			'country.required' => 'La nazione è obbligatoria',
			'country.min' => 'La nazione deve avere minimo 3 caratteri',
			'policy.required' => 'Accetta i termini e le condizioni.',
			'payment_id.required' => 'Seleziona un metodo di pagamento',
			'cf.required' => 'Inserisci il tuo codice fiscale',
			'cf.min' => 'Il codice fiscale deve avere minimo 16 caratteri',
			'cf.max' => 'Il codice fiscale deve avere massimo 16 caratteri',

		);
		
		$payment = F::byId("PaymentMethods", $p['payment_id'], array('title', 'description'));

	    $validator = Validator::make(Input::all(), $rules, $messages);

		//Creo Token Stripe
		if($payment['code'] == 'carta') {
			
			$token = LabStripe::createToken($p['creditcard']);
			
			if(isset($token['error'])) 

				$validator->getMessageBag()->add('carta', "C'è stato un errore durante il pagamento, controlla i dati della carta e il saldo disponibile.");

		}
		////
	    
	    $messages = $validator->messages();

	    if (count($messages) == 0) {

	    	//Update user
	    	$user = $_SESSION['user'];

	    	$user->address = Input::get('address');
	    	$user->city = Input::get('city');
	    	$user->post_code = Input::get('post_code');
	    	$user->province = Input::get('province');
	    	$user->country = Input::get('country');
	    	$user->telephone = Input::get('telephone');
	    	$user->cf = Input::get('cf');

	    	$user->save();

	    	$_SESSION['user'] = $user;

	    	//Creo ordine generale
	    	$order = new Orders();

	    	$order->total = (isset($_SESSION['cart']['discountcode'])) ? F::money('tostring', (F::money('tonumber', ($_SESSION['cart']['total'] + F::money('tonumber', $_SESSION['cart']['fees']))) - F::money('tonumber', $_SESSION['cart']['discountcode']['discount']))) : F::money('tostring', (F::money('tonumber', ($_SESSION['cart']['total'] + F::money('tonumber', $_SESSION['cart']['fees'])))));
	    	$order->shipping_fees = $_SESSION['cart']['fees'];
	    	$order->user_id = $user->id;
	    	$order->payment_id = Input::get('payment_id');

	    	$order->shipping_address = $user->address;
	    	$order->shipping_city = $user->city;
	    	$order->shipping_post_code = $user->post_code;
	    	$order->shipping_province = $user->province;
	    	$order->shipping_country = $user->country;
	    	

	    	/*
	    	|
			| Creo codici promozionali per concorso
			|
	    	 */

			//Conto numero di prodotti acquistati
			$quantity = 0;
			$codesString = "#";

			foreach($_SESSION['cart']['items'] as $item) {

				//$quantity +=  $item['quantity'];
				$quantity = $quantity + $item['quantity'];

			}

			for($i = 1; $i <= $quantity; $i++) {
	
				$codes[] = $code = strtoupper(Str::random(8));
				$codesString .= $code."#";
			}

			/////////
			
			if(isset($_SESSION['cart']['discountcode']))
				
				$order->promotion_codes = $_SESSION['cart']['discountcode']['code'];

	    	$order->save();

	    	$data['promotion_codes'] = $codes;

	    	//Creo subordini
	    	foreach($_SESSION['cart']['items'] as $k => $customdata) {

	    		$orders_product = new OrdersProducts();

	    		$orders_product->product_id = $customdata['product_id'];
	    		$orders_product->quantity = $customdata['quantity'];
	    		$orders_product->order_id = $order->id;
	    		//$orders_product->models = serialize(array($customdata['customdata']['topModel'], $customdata['customdata']['bottomModel']));
	    		
	    		//Custom note
	    		$note = "";
	    		$gift = (Input::get('gift') == 1) ? 'Si' : 'No';

	    		//$topModel = F::byId('ProductsModels', $customdata['customdata']['topModel'], array('title'));
 		        //$bottomModel = F::byId('ProductsModels', $customdata['customdata']['bottomModel'], array('title'));

 		        $note .= 

 		        	//"<b>Pezzo Superiore</b>: ".$topModel['title']."<br>".
 		        	//"<b>Pezzo Inferiore</b>: ".$bottomModel['title']."<br>".
 		        	"<b>Confezione regalo</b>: ".$gift."<br>";

	    		$orders_product->note = $note;
	    		////
	    		
	    		$orders_product->save();

	    		$orders_products[] = $orders_product;

	    	}
			
	    	/*
	    	-
			- Invio Mail con replace dinamico
			-
	    	*/

	    	$data['order_items'] = $orders_products;
	    	
	    	$replace = array(

				"##NAME##" => $_SESSION['user']['name'],
				"##ORDER_ITEMS##" => View::make( 'emails.itemstable', $data )->render(),
				"##ORDER_TOTAL##" => $order['total'],
				"##ORDER_PAYMENTMETHOD##" => $payment['title'],
				"##SHIPPING_FEES##" => $order['shipping_fees']."&euro;",
				"##PROMOTION_CODES##" => View::make( 'emails.promotion_codes', $data )->render()

			);

			e::send('orderconfirm', $_SESSION['user']['email'], $_SESSION['user']['name'], $replace);
			
			////

	    	if($payment['code'] == "paypal") {

	    		//Stampo metodo di pagamento
	    		$url = labpaypal::init($order, $orders_products, "Pagamento ordine numero ".$order->id, "Esegui la transazione per confermare l'ordine");

	    		echo $payment['description'];

	    		$order->payment_code = $url->id;
	    		$order->save();
	    		
	    		echo "<script>window.location.href = '".$url->getApprovalLink()."';</script>";
	    	
	    	}

	    	if($payment['code'] == "carta") {

	    		//Stampo metodo di pagamento
	    		$result = LabStripe::pay($token, $order, $orders_products);

	    		if(isset($result['error']))

	    			echo "<div class='alert alert-danger'>C'è stato un errore nell'operazione, controlla i dati della tua carta o il saldo a tua disposizione.</div>";

	    		else {

		    		echo $payment['description'];

		    		$order->payment_code = $result['charge']['id'];
		    		$order->save();
		    		
		    		$url = F::getUrl('Cart', 'success')."?paymentId=".$result['charge']['id'];

		    		echo "<script>window.location.href = '".$url."';</script>";
	    		
	    		}

	    	}

	    	if($payment['code'] == "bonifico") {

	    		//Stampo metodo di pagamento
	    		echo $payment['description'];

	    	}

	    	F::addnotification('order', "Nuovo ordine confermato da ".$_SESSION['user']['name'], "Ordine n°".$order['id']." per un totale di &euro;".$order['total'], 0);

	    	echo "<div class='alert alert-success'>Ordine creato con successo.</div>";

	    }

	    else

			F::errormessages($messages);

	}

}