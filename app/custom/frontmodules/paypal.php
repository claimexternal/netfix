<?php

use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;
use PayPal\Api\Payer;
use PayPal\Api\Amount;
use PayPal\Api\CreditCard;
use PayPal\Api\Transaction;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Payment;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\FundingInstrument;

use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;

//http://sonten.bcommunication.it/cart/success?paymentId=PAY-5K220143VY0071310K3YWKSQ&token=EC-9LW87131XE022224X&PayerID=3MWUZFZXXEAKN

class labpaypal {

	static function init($order, $orders_products, $title, $description) {

//		PPHttpConfig::$DEFAULT_CURL_OPTS[CURLOPT_SSLVERSION] = 6;

		if(F::getenv() == "dev" || strpos( Request::server('HTTP_HOST'), '.bcommunication')) {

			$mode = "sandbox";
			$loglevel = "DEBUG";
			$keys = LabConfig::get('paypal', 'dev');
		}

		else {

			$mode = "live";
			$loglevel = "FINE";
			$keys = LabConfig::get('paypal', 'prod');
		}	
		
		$return = F::getUrl('Cart', 'success');
		$cancel = F::getUrl('Users', 'orderhistory');

	    $apiContext = new ApiContext(
	        new OAuthTokenCredential(

	        	$keys['clientid'],
	        	$keys['secret']
	            
	        )
	    );

		$apiContext->setConfig(
		
	        array(

	            'mode' => $mode,
	            'log.LogEnabled' => true,
	            'log.FileName' => 'PayPal.log',
	            'log.LogLevel' => $loglevel, //USARE FINE IN PRODUZIONE
	            'validation.level' => 'log',
	            'cache.enabled' => true,
	            // 'http.CURLOPT_CONNECTTIMEOUT' => 30
	            // 'http.headers.PayPal-Partner-Attribution-Id' => '123123123'
	        )
		
		);

		$payer = new Payer();

		$payer->setPaymentMethod("paypal");

		$total = 0;

		$items = array();

		$i = 0;

		foreach($orders_products as $k => $orders_product) {

			$product = F::byId('Products', $orders_product['product_id']);
		
			$items[$i] = new Item();
			$items[$i]->setName($product['title'])
			    ->setCurrency('EUR')
			    ->setQuantity($orders_product['quantity'])
			    ->setPrice(F::money('topaypal', $product['price']));

			$total += (F::money('tonumber', $product['price']) * $orders_product['quantity']);
			$i++;

		}

		$subtotal = F::money('tostring', $total);
		$shippingFees = (isset($_SESSION['cart']['discountcode'])) ? '0,00' : $order->shipping_fees;
		$grandTotal = $subtotal + F::money('tonumber', $shippingFees);
		$description = (isset($_SESSION['cart']['discountcode'])) ? $description." Spese di spedizione azzerate con coupon." : $description;

		$itemList = new ItemList();
		$itemList->setItems($items);
		
		$details = new Details();
		$details->setShipping(F::money('topaypal', $shippingFees))
				->setSubtotal(F::money('topaypal', $subtotal));

		$amount = new Amount();
		$amount->setCurrency("EUR")
		    ->setTotal(F::money('topaypal', $grandTotal))
		    ->setDetails($details);

		$transaction = new Transaction();
		$transaction->setAmount($amount)
		    ->setItemList($itemList)
		    ->setDescription($description." Spese di spedizione gratuite tramite codice coupon.")
		    ->setInvoiceNumber(uniqid());

		$redirectUrls = new RedirectUrls();
		$redirectUrls->setReturnUrl($return)
		    ->setCancelUrl($cancel);

		
		$payment = new Payment();
		$payment->setIntent("sale")
		    ->setPayer($payer)
		    ->setRedirectUrls($redirectUrls)
		    ->setTransactions(array($transaction));

		$request = clone $payment;
		
		//F::printer($payment); die();

		try {

		    $payment->create($apiContext);

		} catch (PayPalConnectionException $ex) {

			echo "Creazione pagamento con Paypal fallita. Contatta l'amministratore del sito segnalando il seguente errore:";

			echo $ex;
			//F::printer($ex->getData());

			exit(1);

		}

		$approvalUrl = $payment->getApprovalLink();

		return $payment;

		}

		static function credicard($order, $orders_products, $creditcard) {

			if(F::getenv() == "dev" || strpos( Request::server('HTTP_HOST'), '.bcommunication')) {

				$mode = "sandbox";
				$loglevel = "DEBUG";
				$keys = LabConfig::get('paypal', 'dev');
			}

			else {

				$mode = "live";
				$loglevel = "FINE";
				$keys = LabConfig::get('paypal', 'prod');
			}	
			
			$return = F::getUrl('Cart', 'success');
			$cancel = F::getUrl('Users', 'orderhistory');

		    $apiContext = new ApiContext(
		        new OAuthTokenCredential(

		        	$keys['clientid'],
		        	$keys['secret']
		            
		        )
		    );

			$apiContext->setConfig(
			
		        array(

		            'mode' => $mode,
		            'log.LogEnabled' => true,
		            'log.FileName' => 'PayPal.log',
		            'log.LogLevel' => $loglevel, //USARE FINE IN PRODUZIONE
		            'validation.level' => 'log',
		            'cache.enabled' => true,
		            // 'http.CURLOPT_CONNECTTIMEOUT' => 30
		            // 'http.headers.PayPal-Partner-Attribution-Id' => '123123123'
		        )
			
			);

			$card = new CreditCard();
			$card->setType($creditcard['type'])
					->setNumber($creditcard['number'])
					->setExpireMonth($creditcard['expire_month'])
					->setExpireYear($creditcard['expire_year'])
					->setFirstName($creditcard['name'])
					->setLastName($creditcard['surname'])
					->setCvv2($creditcard['cvv2']);

			$fundingInstrument = new FundingInstrument();
			$fundingInstrument->setCreditCard($card);

			$payer = new Payer();
			$payer->setPaymentMethod("credit_card")
				  ->setFundingInstruments(array($fundingInstrument));


			$total = 0;

			$items = array();

			$i = 0;

			foreach($orders_products as $k => $orders_product) {

				$product = F::byId('Products', $orders_product['product_id']);
			
				$items[$i] = new Item();
				$items[$i]->setName($product['title'])
				    ->setCurrency('EUR')
				    ->setQuantity($orders_product['quantity'])
				    ->setPrice(F::money('topaypal', $product['price']));

				$total += (F::money('tonumber', $product['price']) * $orders_product['quantity']);
				$i++;

			}

			$subtotal = F::money('tostring', $total);

			$itemList = new ItemList();
			$itemList->setItems($items);
			
			$details = new Details();
			$details->setShipping(F::money('topaypal', $order->shipping_fees))
					->setSubtotal(F::money('topaypal', $subtotal));

			$amount = new Amount();
			$amount->setCurrency("EUR")
			    ->setTotal(F::money('topaypal', $order->total))
			    ->setDetails($details);

			$transaction = new Transaction();
			$transaction->setItemList($itemList);
			$transaction->setAmount($amount);
			$transaction->setDescription("creating a direct payment with credit card");

			$payment = new Payment();
			$payment->setIntent("sale");
			$payment->setPayer($payer);
			$payment->setTransactions(array($transaction));

			$request = clone $payment;

			try {
			    $payment->create($apiContext);
			} catch (PayPalConnectionException $ex) {
			    // NOTE: PLEASE DO NOT USE RESULTPRINTER CLASS IN YOUR ORIGINAL CODE. FOR SAMPLE ONLY
			 	//ResultPrinter::printError('Create Payment Using Credit Card. If 500 Exception, try creating a new Credit Card using <a href="https://ppmts.custhelp.com/app/answers/detail/a_id/750">Step 4, on this link</a>, and using it.', 'Payment', null, $request, $ex);
			    
				echo $ex;
			    exit(1);
			}
			// NOTE: PLEASE DO NOT USE RESULTPRINTER CLASS IN YOUR ORIGINAL CODE. FOR SAMPLE ONLY
			 //ResultPrinter::printResult('Create Payment Using Credit Card', 'Payment', $payment->getId(), $request, $payment);

			 return $payment;

		}

		
		static function confirm() {

			if(F::getenv() == "dev" || strpos( Request::server('HTTP_HOST'), '.bcommunication')) {

				$mode = "sandbox";
				$loglevel = "DEBUG";
				$keys = LabConfig::get('paypal', 'dev');
			}

			else {

				$mode = "live";
				$loglevel = "FINE";
				$keys = LabConfig::get('paypal', 'prod');
			}

		    $apiContext = new ApiContext(
		        new OAuthTokenCredential(

		        	$keys['clientid'],
		        	$keys['secret']
		            
		        )
		    );

			$apiContext->setConfig(
			
		        array(
		            'mode' => $mode,
		            'log.LogEnabled' => true,
		            'log.FileName' => 'PayPal.log',
		            'log.LogLevel' => $loglevel, //USARE FINE IN PRODUZIONE
		            'validation.level' => 'log',
		            'cache.enabled' => true,
		            // 'http.CURLOPT_CONNECTTIMEOUT' => 30
		            // 'http.headers.PayPal-Partner-Attribution-Id' => '123123123'
		        )
			
			);


			$paymentId = $_GET['paymentId'];
			$payment = Payment::get($paymentId, $apiContext);

			$execution = new PaymentExecution();
			$execution->setPayerId($_GET['PayerID']);

			try {

				$result = $payment->execute($execution, $apiContext);

				try {

	            $payment = Payment::get($paymentId, $apiContext);

	        } catch (Exception $ex) {

	 	        echo "Errore Nel Pagamento, contattare l'admin";

	            exit(1);
	        }


			} catch (Exception $ex) {

				F::printer($ex->getData());

				F::redirect(F::getUrl('Users', 'orderhistory'), 1);
				//echo "Errore Nel Pagamento, contattare l'admin2";


			exit(1);

			}			 

			return true;			

		}		

}
