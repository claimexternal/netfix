<?php

//Chiamate GET
class Frontusers {

	public static $slugs = Array(

		"it" => Array(

			'signin' => 'registrazione-utente',
			'login' => 'login-utente',
			'profile' => 'profilo-utente',
			'orderhistory' => 'storico-ordini',
			'logout' => 'logout',
			'getdocument' => 'getdocument',
			'resetpassword' => 'resetpassword',

		),

		"en" => Array(

			'signin' => 'user-registration',
			'login' => 'user-login',
			'profile' => 'user-profile',
			'orderhistory' => 'orderhistory',
			'logout' => 'logout',
			'resetpassword' => 'resetpassword',

		),

		"fr" => Array(

			'signin' => 'enregistrement',
			'login' => 'utilisateur-login',
			'profile' => 'utilisateur-profil',
			
			'logout' => 'logout',
			'resetpassword' => 'resetpassword',

		),

		"es" => Array(

			'signin' => 'registrarse',
			'login' => 'iniciar-sesion',
			'profile' => 'user-profile',
			'logout' => 'logout',
			'resetpassword' => 'resetpassword',

		),

		"hu" => Array(

			'signin' => 'regisztracio',

		),

	);

	//Pagina registrazione-login utente
	static function signin() {

		//Se è autenticato rimando a pagina profili
		if(isset($_SESSION['user']))
			
			F::redirect(F::getUrl('Users', 'profile'), 1);
		
		$tp['page'] = F::byId('Pages', 75, array("title", "metatitle", "metadescription", "description"));
		$tp['page']['type'] = 'Users';
		$tp['page']['id'] = 'signin';

		//Disegno scheletro e contenuto 
		echo View::make( 'skeleton', $tp )->nest('content', 'includes.signin', $tp)->render();

	}

	//Pagina reminder password utente
	static function resetpassword() {

		//Se è autenticato rimando a pagina profili
		if(isset($_SESSION['user']))
			
			F::redirect(F::getUrl('Users', 'profile'), 1);
		
		$tp['page'] = F::byId('Pages', 78, array("title", "metatitle", "metadescription", "description"));
		$tp['page']['type'] = 'Users';
		$tp['page']['id'] = 'resetpassword';

		//Disegno scheletro e contenuto 
		echo View::make( 'skeleton', $tp )->nest('content', 'includes.passwordreminder', $tp)->render();

	}

	//Pagina registrazione-login utente
	static function login() {

		// Se è autenticato rimando a pagina profili
		if(isset($_SESSION['user']))
			
			F::redirect(F::getUrl('Users', 'profile'), 1);
		
		$tp['page'] = F::byId('Pages', 77, array("title", "metatitle", "metadescription", "description"));
		$tp['page']['type'] = 'Users';
		$tp['page']['id'] = 'login';

		//Disegno scheletro e contenuto 
		echo View::make( 'skeleton', $tp )->nest('content', 'includes.login', $tp)->render();

	}
	

	static function logout() {

		unset($_SESSION['user']);

		F::redirect(F::getUrl('Pages', '1'), 1);

	}

	//Pagina profilo utente
	static function profile() {

		if(!isset($_SESSION['user'])) {	
			
			F::redirect(F::getUrl('Users', 'login'), 1);

			die();
		}


		$tp['page'] = F::byId('Pages', 76, array("title", "metatitle", "metadescription", "description"));
		$tp['page']['type'] = 'Users';
		$tp['page']['id'] = 'profile';

		//Documenti scaricati
		$tp['downloads'] = Visits::whereRaw('type="document" and user_id ='.$_SESSION['user']->id)->orderBy('created_at', 'desc')->get();

		echo View::make( 'skeleton', $tp )->nest('content', 'includes.userprofile', $tp)->render();

	}

	//Loggo Download documento
	static function getdocument($id) {

		if (isset($_SESSION['user'])) {

			$rootdomain = LabConfig::$labconfigs['rootdomain'][F::getEnv()];
			$document = Medias::find($id);
			$codist = LabConfig::$labconfigs['codist'];

			if(!$document)

				die('Document doesn\'t exist.');

			if(Visits::whereRaw("user_id =".$_SESSION['user']->id." and typeid =".$document['id'])->count() == 0) {

				$visit = new Visits();

				$visit->type = 'document';
				$visit->typeid = $document['id'];
				$visit->ip = Request::getClientIp();
				$visit->useragent = $_SERVER['HTTP_USER_AGENT'];
				$visit->user_id = $_SESSION['user']->id;

				$visit->save();

			}

			$url = $rootdomain.'/documents/'.$document['name'].'/'.$codist.'/';

			F::redirect($url, 1);

		} else die('Permission denied.');

	}

	static function orderhistory() {

		if(!isset($_SESSION['user']))
			
			F::redirect('users/signin');

		$tp['page'] = F::byId('Pages', 75, array("title", "metatitle", "metadescription", "description"));
		$tp['page']['type'] = 'Users';
		$tp['page']['id'] = 'orderhistory';

		$tp['orders'] = Orders::whereRaw('user_id ='.$_SESSION['user']->id)->orderBy('updated_at', 'desc')->get();

		echo View::make( 'skeleton', $tp )->nest('content', 'includes.orderhistory', $tp)->render();

	}

}

//Chiamate POST
class FrontusersPost {

	static function resetpassword() {

		$rules = array(

			'email' => 'required|email|max:128'

		);

		$messages = array(

			'email' => 'Inserisci correttamente l\'email',
			'email.required' => 'Inserisci l\'email',

		);

		$validator = Validator::make(Input::all(), $rules, $messages);

		$user = User::whereRaw('email ="'.Input::get('email').'" and role = "user" and online = 1')->first();

		if(!$user)

	    	$validator->getMessageBag()->add('email', 'L\'email inserita non esiste.');

		$messages = $validator->messages();

		if (!count($messages) > 0) {

			$newPassword = Str::random(8);

			$user->password = Hash::make($newPassword);

			$user->save();

			$replace = array(

				"##NAME##" => $user['name'],
				"##PASSWORD##" => $newPassword,
				"##RESERVED_AREA_LINK##" => F::getUrl('Users', 'login')

			);

			e::send('passwordreminder', $user['email'], $user['name'], $replace);

			echo "<div class='alert alert-success'>".text('password_confermata', array('it' => 'Password inviata con successo! Controlla la tua email.'))."</div>";

		}

		else

			F::errormessages($messages);

	}

	static function login() {

		$user = User::whereRaw('email ="'.Input::get('email').'" and role = "user" and online = 1')->first();

		if (Auth::attempt(array('email' => Input::get('email'), 'password' => Input::get('password'), 'online' => 1, 'role' => 'user'), true)) {

			$url = F::getUrl('Users', 'profile');
			$user = User::find(Auth::user()['id']);

			$_SESSION['user'] = $user;

			if(isset($_SESSION['checkout'])) {
		
				$url = F::getUrl('Cart', 'checkout');
				unset($_SESSION['checkout']);

			}

			echo "<div class='alert alert-success'>Login ok, stai per essere reindirizzato.</div><script>window.location.href = '".$url."';</script>";

		}
		////

		else {

			echo "<div style='margin-top:15px' class='alert alert-danger'>Email o password errati.</div>";

		}

	}

	//Chiamata per la registrazione utente
	static function register($p) {

		$return = array();

		$rules = array(

			'name' => 'required|min:3',
			'surname' => 'required|min:3',
			'email' => 'required|email|max:128',
			'password' => 'required|min:8',
			'privacy' => 'required',

		);

		$messages = array(

			'name.required' => text('error_name_required', array('it' => 'Inserisci il nome')),
			'surname.required' => text('error_surname_required', array('it' =>'Inserisci il cognome')),
			'privacy.required' => text('errorprivacy_required', array('it' =>'E\' necessario autorizzare il trattamento alla privacy dei tuoi dati')),
			'email' => text('error_email', array('it' =>'Inserisci correttamente l\'email')),
			'email.required' => text('error_email_required', array('it' =>'Inserisci l\'email')),
			'password.required' => text('error_password_required', array('it' =>'La password è obbligatoria')),
			'password.min' => text('error_password_min', array('it' =>'La password deve avere minimo 8 caratteri')),
			'privacy.required' => text('error_privacy_min', array('it' =>'Il campo privacy è obbligatorio')),

		);

	    $validator = Validator::make(Input::all(), $rules, $messages);

	    $mail = User::whereRaw('email = "'.Input::get('email').'" and role="user"')->first();

	    if($mail)

	    	$validator->getMessageBag()->add('email', text('error_email_esistente', array('it' =>'L\'email inserita è già presente nel nostro sistema')));

	    if(Input::get('password') != Input::get('repeat_password') )

	    	$validator->getMessageBag()->add('email', text('error_password_non_coincide', array('it' =>'Le due password non coincidono.')));

	    if(!F::checkRecaptcha())

	    	$validator->getMessageBag()->add('recaptcha', text('error_recaptcha', array('it' =>'Dimostraci che non sei un robot, clicca su "Non sono un robot" e continua')));

	    $messages = $validator->messages();

	    //Salvo l'utente
	    if (!count($messages) > 0) {

	    	$return['error'] = 0;

	    	$array = Input::all();
	    	
	    	$array['role'] = "user";
	    	$array['online'] = 0;
	    	$array['password'] = Hash::make(Input::get('password'));

	    	$user = F::saveObject(array('User', 'users'), $array);
			
			$return['data']['lang'] = App::getLocale();
			$return['data']['email'] = Input::get('email');
			$return['data']['nome'] = Input::get('name');
	    	$return['data']['cognome'] = Input::get('surname');
	    	$return['data']['ragione_sociale'] = Input::get('company_name');
	    	$return['data']['citta'] = Input::get('city');
	    	$return['data']['provincia'] = Input::get('province');
	    	$return['data']['regione'] = Input::get('state');
	    	$return['data']['tel'] = Input::get('telephone');

			$return['data']['tag'] = 'Form Registrazione Sito Web ';
			$return['codist'] = F::encryptString(LabConfig::$labconfigs['codist']);
			$return['url'] = 'https://app.bconsole.com/Bgest/web/bcms_api.php/webtolead/insert';

		//Converto Nazione in id Naz Bconsole
		if(Input::has('country')) {

			switch (Input::get('country')) {

				case 'Italy':
					
					$idNazione = '999907';

				break;
				
				case 'United Kingdom':
					
					$idNazione = '999114';

				break;
				
				case 'France':
					
					$idNazione = '999110';

				break;
				
				case 'Spain':
					
					$idNazione = '999131';

				break;
				
				case 'Netherlands':
					
					$idNazione = '999126';

				break;
				
				case 'Russia':
					
					$idNazione = '999154';

				break;
				
				case 'United States of America':
					
					$idNazione = '999404';

				break;
				
				case 'Germany':
					
					$idNazione = '999112';

				break;

				case 'Austria':
					
					$idNazione = '999102';

				break;
				
				default:
				
					$idNazione = '000000';

				break;
			}

			$return['data']['nazione'] = $idNazione;

		}

	    	F::addnotification('newuser', $user['name']." si è registrato alla piattaforma", $user['name']." si è registrato alla piattaforma", 2);

	    	$replace = array(

				"##NAME##" => Input::get('name'),
				"##EMAIL##" => Input::get('email'),
				"##SURNAME##" => Input::get('surname'),
				"##COMPANY##" => Input::get('company_name', 'Privato'),
				"##SECTOR##" => Input::get('sector', 'Privato'),
				"##TELEPHONE##" => Input::get('telephone'),
				"##CITY##" => Input::get('city'),
				"##PROVINCE##" => Input::get('province')

			);

			e::send('registerok', Input::get('email'), Input::get('name'), $replace);


			// Invio notifica registrazione
			$admincontact = LabConfig::get('admincontacts', F::getenv());

			$recipient = Array(

				"to" => array($admincontact['email-registrazione'] => $admincontact['name']),
				
				"bcc" => array(
					
					"angela.patania@expoclima.net" => "Angela Patania",

				)

			);


			$confirmlinkYes = url('usersconfirmation/userconfirm?a=1&e='.Crypt::encrypt(Input::get('email')).'&i='.Crypt::encrypt($user['id']));
			$confirmlinkNo = url('usersconfirmation/userconfirm?a=-1&e='.Crypt::encrypt(Input::get('email')).'&i='.Crypt::encrypt($user['id']));

			$replace2 = array(

				"##FIRSTLINE##" => "L'utente ".Input::get('name')." ".Input::get('surname')." Ha richiesto di potersi registrare al sito Panther Safety ed è in attesa di una tua approvazione:",
				"##LINK_YES##" => '<a href="'.$confirmlinkYes.'">Abilita</a>',
				"##LINK_NO##" => '<a href="'.$confirmlinkNo.'">Non abilitare</a>',
				"##NAME##" => Input::get('name'),
				"##EMAIL##" => Input::get('email'),
				"##SURNAME##" => Input::get('surname'),
				"##COMPANY##" => Input::get('company_name', 'Privato'),
				"##SECTOR##" => Input::get('sector', 'Privato'),
				"##TELEPHONE##" => Input::get('telephone'),
				"##CITY##" => Input::get('city'),
				"##PROVINCE##" => Input::get('province')

			);

			e::multiSend('userconfirmation', $recipient, $replace2);

			$return['html'] = "<div class='alert alert-success'>".text('registrazione_corretta', Array('it' => 'Registrazione avvenuta con successo. Un incaricato la prenderà subito in considerazione. Riceverai presto una mail di notifica.'))."</div>";

	    
	    } 

	    else {

	    	$return['error'] = 1;

			$return['html'] = F::errormessages($messages, 1);

		}

		echo json_encode($return);
	}

	static function profileedit( $p ) {

		$rules = array(

			'name' => 'required|min:3',
			'city' => 'required|min:3',
			'province' => 'required',
			'state' => 'required',
			'country' => 'required',
			'privacy' => 'required',
			'email' => 'required|email|max:128|unique:users',
			'password' => 'required|min:8',
			'repeat_password' => 'same:password',

		);

		$messages = array(

			'name.required' => 'Inserisci il nominativo',
			'province.required' => 'Inserisci la provincia',
			'state.required' => 'Inserisci la regione',
			'country.required' => 'Inserisci la nazione',
			'privacy.required' => 'E\' necessario autorizzare il trattamento alla privacy dei tuoi dati',
			'city.required' => 'Inserisci la città',
			'city.min' => 'Campo città troppo corto',
			'name.min' => 'Il nominativo deve avere minimo 3 caratteri',
			'email' => 'Inserisci correttamente l\'email',
			'email.required' => 'Inserisci l\'email',
			'email.unique' => 'L\'email inserita è già presente nel nostro sistema',
			'password.required' => 'La password è obbligatoria',
			'password.min' => 'La password deve avere minimo 8 caratteri'

		);

	    $validator = Validator::make(Input::all(), $rules, $messages);

	    $mail = User::whereRaw('email = "'.Input::get('email').'"')->first();

	    if($mail && Input::get('email') != $_SESSION['user']->email)

	    	$validator->getMessageBag()->add('email', 'L\'email inserita è già presente nel nostro sistema');

	    $messages = $validator->messages();

	    if (!$validator->fails()) {

	    	$user = $_SESSION['user'];

	    	$user->email = Input::get('email');
	    	$user->name = Input::get('name');
	    	$user->telephone = Input::get('telephone');
	    	$user->address = Input::get('address');
	    	$user->city = Input::get('city');
	    	$user->post_code = Input::get('post_code');
	    	$user->province = Input::get('province');
	    	$user->country = Input::get('country');

	    	$user->save();

	    	$_SESSION['user'] = $user;

	    	echo "<div class='alert alert-success'>".text('profilo_corretto', Array('it' => 'Profilo aggiornato con successo.'))."</div>";

	    }

	    else

			F::errormessages($messages);

	}

}