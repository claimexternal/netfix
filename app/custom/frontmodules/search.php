<?php


class Frontsearch {
	
	static function products() {
		
		$objects = array();
		$strings = array('title', 'metatitle', 'metadescription', 'shortdescription');
		
		$tp['type'] = "Pages";
		$tp['page'] = F::byId('Pages', 1, $strings);
		$tp['page']['title'] = "Ricerca";
		$tp['page']['metatitle'] = "Ricerca";

		$searchVal = Input::get('search');

		if($searchVal != ""){

			$where = array("1=1", "LIKE '%".$searchVal."%'");

			$objects = F::byWherePagination('Products', $where , $strings);

		}

		if(count($objects) == 0)

			$objects = array();

		$tp['objects'] = $objects;
		$tp['search'] = $searchVal;

		//Disegno scheletro e contenuto 
		echo View::make( 'skeleton', $tp )->nest('content', 'search', $tp)->render();

		die();

	}



	// Funzione per filtrare la lista dei prodotti

	static function filterProd(){

		$objects = array();
		$strings = array('title', 'metatitle', 'metadescription', 'shortdescription');		
		
		$tp['type'] = "Pages";
		$tp['page'] = F::byId('Pages', 1, $strings);
		$tp['page']['title'] = "Ricerca";
		$tp['page']['metatitle'] = "Ricerca";

		$where = F::makeWhereByUrl();

		$objects = F::byWherePagination('Products', $where , $strings);

		if(count($objects) == 0)

			$objects = array();

		$tp['objects'] = $objects;

		//Indico che il tipo di ricerca è "trova la tua scarpa"
		$tp['search'] = "";

		//Disegno solo il contenuto 
		if (Request::ajax())
			
			echo View::make( 'search', $tp )->render();

		 else{

			if( get_class($tp['page']) == "Categories" ) echo View::make( 'skeleton', $tp )->nest('content', 'category', $tp)->render();
			else echo View::make( 'skeleton', $tp )->nest('content', 'search', $tp)->render();

		}

	}



}

class FrontsearchPost {

	static function loadsizes($p) {

		$array['sizes'] = array();

		$model = F::byId('ProductsModels', $p['id'], array('title'));

		$array['sizes'] = unserialize($model['size']);
		$array['model'] = $model;

		echo View::make( 'includes.loadsizes', $array )->render();

	}

}