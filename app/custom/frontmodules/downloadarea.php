<?php

class Frontdownloadarea {

	static function loadproducts() {

		$products = F::byWhere('Products', 'online=1');

		foreach($products as $k => $product) {
			
			$subcategory = F::byId('Categories', $product['category_id']);
			$category = F::byId('Categories', $subcategory['parent_id']);
			
			$array[] = array(

				'value'=> $category['title']." / ".$subcategory['title']." - ".$product['title'],
				'data' => array('category' => $category['title'], 'question_id' => $product['id'], 'category_id' => $subcategory['id']) 

			);

		}

		echo json_encode($array);

	}

}

//Chiamate POST
class FrontdownloadareaPost {

	static function loadAccessoriesDocuments($p) {

		App::setLocale($_GET['lang']);

		$array['documents'] = Array();
		$documents = Array();

		$documents = D::getAllDocuments('Accessories', $p['id'], 1000);

		foreach ($documents as $k => $document) {

			$array['documents'][$k] = $document;

			$object = Medias::with('title')->find($document['id']);

			$array['documents'][$k]['langs'] = "";

			foreach($object['relations']['title']['attributes'] as $column => $value) {

				if($column != "id" && $column != "deleted_at" && $column != "updated_at" && $column != "created_at" && $value != "")

					$array['documents'][$k]['langs'] .= "<b>".strtoupper($column)."</b> / ";

			}

		}

		$array['html'] = View::make('includes.loadaccessoriesdocuments', $array)->render();
		
		echo json_encode($array); 

	}

	static function loadElements($p) {

		App::setLocale($_GET['lang']);

		switch ($p['element']) {
			
			case 'products':
				
				$firstValue = text('seleziona_prodotto', array('it' => 'Seleziona un prodotto...'));

				$array[0] = array('id' => 0, 'title' => $firstValue);
				/*$where = "";

				$categories = Categories::where('parent_id', '=', $p['parent_id'])->get();

				foreach($categories as $category) 

					$where .= "category_id = ".$category['id']." or ";

				$where = substr($where, 0, -4);*/

				$products = Products::with('title')->whereRaw("online = 1 and category_id =".$p['parent_id'])->get();
				
				foreach($products as $product)

					$newProducts[$product['id']] = $product['title'][App::getLocale()];

				//Ordino per titolo
				asort($newProducts);
				reset($newProducts);

				foreach($newProducts as $id => $product) 

					$array[] = array( "id" => $id, "title" => $product);

				echo json_encode($array);

			break;
			
			default:
			
				echo json_encode(array());			

			break;
		}


	}

	static function loaddocuments($p) {

		App::setLocale($p['language']); 

	
		$schema = "Products";
		$parent_id = $p['product-id'];		

		$array['documents'] = array();

		$documents = D::getAllDocuments($schema, $parent_id, 1000);

		foreach ($documents as $k => $document) {

			$array['documents'][$k] = $document;

			$object = Medias::with('title')->find($document['id']);

			$array['documents'][$k]['langs'] = "";

			foreach($object['relations']['title']['attributes'] as $column => $value) {

				if($column != "id" && $column != "deleted_at" && $column != "updated_at" && $column != "created_at" && $value != "")

					$array['documents'][$k]['langs'] .= "<b>".strtoupper($column)."</b> / ";

			}

		}

		echo View::make('includes.loaddocuments', $array)->render();	

	}

}