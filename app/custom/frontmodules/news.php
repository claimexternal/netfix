<?php


//Chiamate GET
class Frontnews {

	public static $slugs = Array(

		"it" => Array(

			'events' => 'eventi',
			'realizations' => 'realizzazioni',
			'regulations' => 'normative',
			'newsletter' => 'newsletter',
			'casestudy' => 'casestudy',
			'news' => 'dettaglio',
			'videos' => 'videos',

		),

		"en" => Array(

			'events' => 'events',
			'realizations' => 'realizations',
			'regulations' => 'regulations',
			'newsletter' => 'newsletter',
			'casestudy' => 'casestudy',
			'news' => 'detail',
			'videos' => 'videos',

		),

		"fr" => Array(

			'events' => 'evenements',
			'realizations' => 'realisations',
			'regulations' => 'reglements',
			'newsletter' => 'newsletter',
			'casestudy' => 'casestudy',
			'news' => 'detail',
			'videos' => 'video',

		),

		"es" => Array(

			'events' => 'events',
			'realizations' => 'realizations',
			'regulations' => 'regulations',
			'newsletter' => 'newsletter',
			'casestudy' => 'casestudy',
			'news' => 'detail',
			'videos' => 'videos',

		),

		"de" => Array(

			'events' => 'events',
			'realizations' => 'realizations',
			'regulations' => 'regulations',
			'newsletter' => 'newsletter',
			'casestudy' => 'casestudy',
			'news' => 'detail',
			'videos' => 'videos',

		),

		"hu" => Array(

			'events' => 'events',
			'realizations' => 'realizations',
			'regulations' => 'regulations',
			'newsletter' => 'newsletter',
			'casestudy' => 'casestudy',
			'news' => 'detail',
			'videos' => 'videos',

		),

	);

	static function events($url) {

		$tp['page'] = F::byUrlWhere('Newss', $url, 'type = "events"', array('metatitle', 'metadescription', 'metakeywords', 'title', 'description'));

		if($tp['page'])
			//Disegno scheletro e contenuto 
			echo View::make( 'skeleton', $tp )->nest('content', 'news', $tp)->render();

		else {

			header("HTTP/1.0 404 Not Found");
			echo View::make( 'skeleton', $tp )->nest('content', '404', $tp)->render();

		}
	}

	static function regulations($url) {

		$tp['page'] = F::byUrlWhere('Newss', $url, 'type = "regulations"', array('metatitle', 'metadescription', 'metakeywords', 'title', 'description'));
		
		if($tp['page'])
			//Disegno scheletro e contenuto 
			echo View::make( 'skeleton', $tp )->nest('content', 'news', $tp)->render();

		else

			echo View::make( 'skeleton', $tp )->nest('content', '404', $tp)->render();

	}

	static function news($url) {

		$tp['page'] = F::byUrlWhere('Newss', $url, 'type = "news"', array('metatitle', 'metadescription', 'metakeywords', 'title', 'description'));

		if($tp['page'])
			//Disegno scheletro e contenuto 
			echo View::make( 'skeleton', $tp )->nest('content', 'news', $tp)->render();

		else {

			header("HTTP/1.0 404 Not Found");
			echo View::make( 'skeleton', $tp )->nest('content', '404', $tp)->render();

		}

	}


	static function casestudy($url) {

		$tp['page'] = F::byUrlWhere('Newss', $url, 'type = "casestudy"', array('metatitle', 'metadescription', 'metakeywords', 'title', 'description'));

		if($tp['page'])
			//Disegno scheletro e contenuto 
			echo View::make( 'skeleton', $tp )->nest('content', 'news', $tp)->render();

		else {

			header("HTTP/1.0 404 Not Found");
			echo View::make( 'skeleton', $tp )->nest('content', '404', $tp)->render();

		}

	}

	static function newsletter($url) {

		$tp['page'] = F::byUrlWhere('Newss', $url, 'type = "newsletter"', array('metatitle', 'metadescription', 'metakeywords', 'title', 'description'));

		if($tp['page'])
			//Disegno scheletro e contenuto 
			echo View::make( 'skeleton', $tp )->nest('content', 'news', $tp)->render();

		else

			echo View::make( 'skeleton', $tp )->nest('content', '404', $tp)->render();

	}

	static function realizations($url) {

		$tp['page'] = F::byUrlWhere('Newss', $url, 'type = "realizations"', array('metatitle', 'metadescription', 'metakeywords', 'title', 'description'));

		if($tp['page'])
			//Disegno scheletro e contenuto 
			echo View::make( 'skeleton', $tp )->nest('content', 'news', $tp)->render();

		else

			echo View::make( 'skeleton', $tp )->nest('content', '404', $tp)->render();

	}

	static function videos($url) {

		$tp['page'] = F::byUrlWhere('Newss', $url, 'type = "video"', array('metatitle', 'metadescription', 'metakeywords', 'title', 'description'));

		if($tp['page'])
			//Disegno scheletro e contenuto 
			echo View::make( 'skeleton', $tp )->nest('content', 'news', $tp)->render();

		else

			echo View::make( 'skeleton', $tp )->nest('content', '404', $tp)->render();

	}

}
