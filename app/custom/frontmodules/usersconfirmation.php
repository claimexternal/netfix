<?php

//Chiamate GET
class Frontusersconfirmation {

	static function userconfirm() {

		$email = Crypt::decrypt(Input::get('e'));
		$id = Crypt::decrypt(Input::get('i'));
		$user = User::whereRaw('id = '.$id.' and email = "'.$email.'"')->first();
		
		if($user) {

			$user->online = $_GET['a'];

			$user->save();

			$tp['page'] = array(

				"title" => "Utente confermato con successo",
				"metatitle" => "Utente confermato con successo",
				"metadescription" => "Utente confermato con successo",
				"user" => $user

			);

			$replace = array(

				"##NAME##" => $user['name']." ".$user['surname']

			);

			if($_GET['a'] == "1") {
				
				e::send('userconfirmationok', $user['email'], $user['name']." ".$user['surname'], $replace);
				echo "Utente ".$user['mail']." confermato con successo";
			}
			elseif($_GET['a'] == "-1") {

				e::send('userconfirmationnot', $user['email'], $user['name']." ".$user['surname'], $replace);
				echo "Utente ".$user['mail']." disabilitato con successo";
			
			}

			

		} else echo "Utente non esistente";
		//echo View::make( 'skeleton', $tp )->nest('content', 'emails.userconfirmation', $tp)->render();

	}

}