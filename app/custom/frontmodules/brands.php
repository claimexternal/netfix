<?php


//Chiamate GET
class Frontbrands {

	public static $slugs = Array(

		"it" => Array(

			'detail' => 'dettaglio',
			

		),

		"en" => Array(

			'detail' => 'detail',

		)
	);

	static function detail($url) {

		$tp['brand'] = Brands::with('title', 'description')->where('online', 1)->where('code', $url)->first();
		$tp['page'] = F::byId('Pages', 5 ,array('metatitle', 'metadescription', 'metakeywords', 'title', 'description'));

		echo View::make( 'skeleton', $tp )->nest('content', 'includes.brand_detail', $tp)->render();

	}

}
