<?php

use Stripe\Stripe;
use Stripe\Token;
use Stripe\Charge;
use Stripe\Customer;
use Stripe\Error\Card;

/*Testing
|
| Number 	Card type
| 4242424242424242	Visa
|
*/

//Pagamento diretto con carta di Credito utilizzando Stripe
class LabStripe {

	static function createToken($card) {

		$return = Array();

		if(F::getenv() == "dev") 

			Stripe::setApiKey(LabConfig::$labconfigs['stripe']['dev']['publishable_key']);
		
		else

			Stripe::setApiKey(LabConfig::$labconfigs['stripe']['prod']['publishable_key']);

		try {

		    $token = Token::create(array(
		  
			  "card" => array(
			    "number" => $card['number'],
			    "exp_month" => $card['expire_month'],
			    "exp_year" => $card['expire_year'],
			    "cvc" => $card['cvv2']
			  )

			));

			$return['token'] = $token;
		
		} catch (Card $e) {

		  $body = $e->getJsonBody();
		  $err  = $body['error'];

		  $return['error'] = $err['code'];
		  
		}

		return $return;

	}

	static function pay($token, $order, $orders_products) {

		if(F::getenv() == "dev") 

			Stripe::setApiKey(LabConfig::$labconfigs['stripe']['dev']['secret_key']);
		
		else

			Stripe::setApiKey(LabConfig::$labconfigs['stripe']['prod']['secret_key']);
			
		$total = 0;

		$items = array();

		$i = 0;

		foreach($orders_products as $k => $orders_product) {

			$product = F::byId('Products', $orders_product['product_id']);
		
			$total += (F::money('tonumber', $product['price']) * $orders_product['quantity']);

			$i++;

		}

		$total += F::money('tonumber', $order['shipping_fees']);

		$total = (isset($_SESSION['cart']['discountcode'])) ? $total - F::money('tonumber', $_SESSION['cart']['discountcode']['discount']) : $total;

		$total = F::money('tostripe', F::money('tostring', $total));

	    $token  = $token['token']['id'];

	    $customer = Customer::create(array(
	      
	       'email' => User::get('email'),
	       'card'  => $token
	    
	    ));

		try {

		    $charge = Charge::create(array(
	    
		       'customer' => $customer->id,
		       'amount'   => $total,
		       'currency' => 'eur',
		       'metadata' => array('order_id' => $order['id']),
		       'description' => 'Sonten.com - Pagamento ordine n '.$order['id']." tramite carta di credito."
	    
	    	));

			$return['charge'] = $charge;
		
		} catch (Card $e) {

		  $body = $e->getJsonBody();
		  $err  = $body['error'];

		  $return['error'] = $err['code'];
		  
		}

		return $return;

	}
}
