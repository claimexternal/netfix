<?php 

//Array di spese spedizione per paese
$countriesFees = array(

	"ITALIA" => "7,00",
	"AFGHANISTAN" => "37,00",
	"ALBANIA" => "39,00",
	"ALGERIA" => "39,00",
	"ANGOLA" => "51,00",
	"ANGUILLA" => "41,00",
	"ANTIGUA AND BARBUDA" => "51,00",
	"ARGENTINA" => "51,00",
	"ARMENIA" => "37,00",
	"ARUBA" => "51,00",
	"AUSTRALIA" => "51,00",
	"AUSTRIA" => "13,00",
	"AZERBAIJAN" => "37,00",
	"AZORES ISLANDS" => "41,00",
	"BAHAMAS" => "51,00",
	"BAHRAIN" => "39,00",
	"BANGLADESH" => "41,00",
	"BARBADOS" => "51,00",
	"BELARUS" => "37,00",
	"BELGIUM" => "13,00",
	"BELIZE" => "51,00",
	"BENIN" => "41,00",
	"BERMUDA" => "51,00",
	"BOLIVIA" => "41,00",
	"BOSNIA AND HERZEGOVINA" => "37,00",
	"BOTSWANA" => "51,00",
	"BRAZIL" => "51,00",
	"BRUNEI DARUSSALAM" => "51,00",
	"BULGARIA" => "19,00",
	"BURKINA FASO" => "41,00",
	"BURUNDI" => "41,00",
	"BUSINGEN" => "41,00",
	"BUTHAN" => "41,00",
	"CAMBODIA" => "41,00",
	"CAMEROON" => "41,00",
	"CANADA" => "41,00",
	"CANARY ISLANDS" => "41,00",
	"CAPE VERDE" => "41,00",
	"CAYMAN ISLANDS" => "51,00",
	"CEUTA" => "41,00",
	"CHAD" => "41,00",
	"CHILE" => "51,00",
	"CHINA" => "41,00",
	"COLOMBIA" => "41,00",
	"COMOROS" => "51,00",
	"COSTA RICA" => "51,00",
	"CROATIA" => "19,00",
	"CUBA" => "51,00",
	"CYPRUS" => "28,00",
	"CZECH REPUBLIC" => "16,00",
	"DEMOC. REP. OF CONGO" => "41,00",
	"DENMARK" => "16,00",
	"DJIBUTI" => "41,00",
	"DOMINICA" => "41,00",
	"DOMINICAN REPUB." => "41,00",
	"ECUADOR" => "51,00",
	"EGYPT" => "39,00",
	"EL SALVADOR" => "51,00",
	"EQUATORIAL GUINEA" => "51,00",
	"ERITREA " => "41,00",
	"ESTONIA" => "20,00",
	"ETHIOPIA" => "39,00",
	"FIJI" => "51,00",
	"FINLAND" => "21,00",
	"FRANCE" => "19,00",
	"FRENCH POLYNESIA" => "51,00",
	"GABON" => "41,00",
	"GAMBIA " => "41,00",
	"GEORGIA" => "41,00",
	"GERMANY" => "13,00",
	"GHANA" => "39,00",
	"GIBRALTAR" => "39,00",
	"GREECE" => "21,00",
	"GRENADA" => "51,00",
	"GUATEMALA" => "51,00",
	"GUINEA" => "51,00",
	"GUINEA BISSAU" => "41,00",
	"GUYANA" => "41,00",
	"HAITI" => "51,00",
	"HELGOLAND" => "41,00",
	"HONDURAS" => "51,00",
	"HONG KONG" => "41,00",
	"HUNGARY" => "16,00",
	"INDIA" => "39,00",
	"INDONESIA" => "51,00",
	"IRAN" => "41,00",
	"IRAQ" => "41,00",
	"IRELAND" => "21,00",
	"ISOLE BALEARI" => "41,00",
	"ISRAEL" => "39,00",
	"IVORY COAST" => "41,00",
	"JAMAICA" => "51,00",
	"JAPAN" => "51,00",
	"JORDAN" => "39,00",
	"KAZAKHSTAN" => "37,00",
	"KENYA" => "41,00",
	"KIRIBATI" => "51,00",
	"KOSOVO" => "37,00",
	"KUWAIT" => "39,00",
	"LAO DEMOCRATIC REP." => "51,00",
	"LATVIA" => "15,00",
	"LEBANON" => "39,00",
	"LESOTHO" => "51,00",
	"LIBERIA" => "41,00",
	"LIBYA" => "39,00",
	"LIECHTENSTEIN" => "15,00",
	"LITHUANIA" => "21,00",
	"LUXEMBURG" => "13,00",
	"MACAO" => "51,00",
	"MADAGASCAR" => "51,00",
	"MADEIRA" => "41,00",
	"MALAWI" => "41,00",
	"MALAYSIA" => "41,00",
	"MALDIVES" => "51,00",
	"MALI" => "41,00",
	"MALTA" => "21,00",
	"MAURITANIA" => "39,00",
	"MAURITIUS" => "41,00",
	"MELILLA" => "41,00",
	"MEXICO" => "51,00",
	"MOLDOVA REP" => "37,00",
	"MONGOLIA" => "41,00",
	"MONTE ATHOS" => "41,00",
	"MONTENEGRO" => "37,00",
	"MOROCCO" => "39,00",
	"MOZAMBIQUE" => "51,00",
	"MYNAMAR BURMA" => "51,00",
	"NEPAL" => "41,00",
	"NETHERLAND" => "14,00",
	"NETHERLANDS ANTILLES" => "51,00",
	"NEW ZEALAND" => "51,00",
	"NIGER" => "39,00",
	"NIGERIA" => "39,00",
	"NORWAY" => "29,00",
	"OMAN" => "39,00",
	"PAKISTAN" => "39,00",
	"PALESTINIAN NAT.AUTH." => "39,00",
	"PANAMA" => "51,00",
	"PAPUA NEW GUINEA" => "51,00",
	"PARAGUAY" => "51,00",
	"PERU'" => "51,00",
	"PHILIPPINES" => "51,00",
	"POLAND" => "16,00",
	"PORTUGAL" => "21,00",
	"PRINCIPALITY OF MONACO" => "19,00",
	"PUERTO RICO" => "41,00",
	"QATAR" => "39,00",
	"REP. CENTRAFRICANA" => "41,00",
	"REP. OF MACEDONIA" => "37,00",
	"REPUBL. OF CONGO" => "41,00",
	"ROMANIA" => "21,00",
	"RUSSIAN FEDERATION" => "39,00",
	"RWANDA" => "39,00",
	"SAINT KITTS AND NEVIS" => "41,00",
	"SAINT LUCIA" => "51,00",
	"SAINT VINCENT AND GRENADINES" => "51,00",
	"SAMOA" => "51,00",
	"SAO TOME AND PRINCIPE" => "51,00",
	"SAUDI ARABIA" => "39,00",
	"SENEGAL" => "39,00",
	"SERBIA" => "19,00",
	"SEYCHELLES" => "41,00",
	"SIERRA LEONE" => "41,00",
	"SLOVAKIA" => "16,00",
	"SLOVENIA" => "16,00",
	"SOLOMON ISLANDS" => "51,00",
	"SOUTH AFRICAN REP." => "41,00",
	"SOUTH COREA" => "51,00",
	"SPAIN" => "19,00",
	"SRI LANKA" => "41,00",
	"SUDAN" => "39,00",
	"SURINAME" => "51,00",
	"SWAZILAND" => "51,00",
	"SWEDEN" => "21,00",
	"SWITZERLAND" => "16,00",
	"SYRIA" => "39,00",
	"TAIWAN" => "41,00",
	"TAJIKISTAN" => "37,00",
	"TANZANIA" => "41,00",
	"THAILAND" => "41,00",
	"TOGO" => "41,00",
	"TONGA" => "51,00",
	"TRINIDAD AND TOBAGO" => "41,00",
	"TUNISIA" => "39,00",
	"TURKEY" => "39,00",
	"TURKMENISTAN " => "37,00",
	"UGANDA" => "41,00",
	"UKRAINE" => "37,00",
	"UNITED ARAB EMIRATES" => "41,00",
	"UNITED KINGDOM" => "19,00",
	"UNITED STATES" => "41,00",
	"URUGUAY" => "51,00",
	"UZBEKISTAN" => "37,00",
	"VANUATU" => "51,00",
	"VENEZUELA" => "41,00",
	"VIETNAM" => "51,00",
	"YEMEN" => "39,00",
	"ZAMBIA" => "51,00",
	"ZIMBABWE" => "41,00"

);