<?php

 /**
   * labconfig
   * Racchiude tutte le impostazioni statiche e dinamiche dell'installazione attiva
   *
   * 
   * 02/03/2015
   * 
   * @package    bCMS
   * @subpackage Custom classes
   * @author     Enrico Pettinao <enricopettinao@gmail.com>
   */

class F {
	
	public static $count = 0;

	static function printer ( $array ) {

		echo "<pre>";

		print_r($array);

		echo "<pre>";


	}

	//Ritorno ambiente di sviluppo
	static function getenv() {

		if( strpos( Request::server('HTTP_HOST'), 'localhost') ||
		    strpos( Request::server('HTTP_HOST'), '.dev') || 
		    strpos( Request::server('HTTP_HOST'), '.test') || 
		    strpos( Request::server('HTTP_HOST'), '127.0.0.1') ||
		    strpos( Request::server('HTTP_HOST'), LabConfig::get('devdomain'))
		  ) 

			return "dev";

		else

			return "prod";
	}

	/**
	* Effettua redirect all'interno dell'applicazione
	*
	* @param  string  $path
	*/

	static function redirect( $path, $ext = 0 ) {

		if($ext == 0)

			header("location: ".URL::to($path));
		
		else

			header("location: ".$path);
		
		die();
		
	}

	/**
	* Ritorna un oggetto (page, product, category) dato l'url in lingua
	*
	* @param   string  $schema      //Schema db dove effettuare le operazioni
	* @param   string  $url         //Slug url dove cercare
	* @param   array   $strings     //Strings da includere
	* @return  array   $object      //Oggetto db formato array
	*/

	static function byUrl($schema, $url, $strings = array('url'), $lang = "") {

		$lang = ($lang == "") ? App::getLocale() : $lang;

		$object = $schema::with($strings)->where("online", "=", 1)->whereHas('url', function( $urlz ) use ($url) {	
			
			$urlz->where(App::getLocale(), '=', $url);

		})->first();
		
		foreach($strings as $value) {

			if(isset($object['relations'][$value]))
				
				$object[$value] = $object['relations'][$value][$lang];


		}

		return $object;

	} 

	/**
	* Ritorna un oggetto (page, product, category) dato l'url in lingua con clausula where
	*
	* @param   string  $schema      //Schema db dove effettuare le operazioni
	* @param   string  $url         //Slug url dove cercare
	* @param   string  $where       //Clausula where per query
	* @param   array   $strings     //Strings da includere
	* @return  array   $object      //Oggetto db formato array
	*/

	static function byUrlWhere($schema, $url, $where, $strings = array('url'), $lang = "") {

		$lang = ($lang == "") ? App::getLocale() : $lang;

		$object = $schema::with($strings)->whereHas('url', function( $urlz ) use ($url) {	
			
			$urlz->where(App::getLocale(), '=', $url);

		})->whereRaw($where." and online = 1")->first();
			
		foreach($strings as $value) {

			if(isset($object['relations'][$value]))
				
				$object[$value] = $object['relations'][$value][$lang];


		}

		return $object;

	} 


	/**
	* Ritorna un insieme di oggetti con lingue (page, product, category) data la condizione nel where
	*
	* @param   string  $schema      //Schema db dove effettuare le operazioni
	* @param   string  $where       //Condizione per la query
	* @param   array   $strings     //Strings da includere
	* @param   array   $order       //Order by della query
	* @param   string  $limit       //Limite elementi della query
	* @param   string  $skip        //Salta n elementi
	* @return  array   $objects     //Array di oggetti db formato array
	*/

	static function byWhere($schema, $where, $strings = array('title'), $order = array("updated_at" , "desc"), $limit = "18446744073709551615", $skip = "0") {

		$lang = App::getLocale();
		
		$objects = $schema::with($strings)->whereHas('title', function( $titlez ) use($lang) {	
			
			$titlez->where($lang, '!=', '');

		})->whereRaw($where." and online = 1")->orderBy($order[0], $order[1])->take($limit)->skip($skip)->get()->toArray();

		foreach($objects as $key => $object) {
			
			foreach($strings as $value) $objects[$key][$value] = $object[$value][$lang];

			$objects[$key]['shematype'] = $schema;

		}

		return $objects;

	}

	/**
	* Ritorna un insieme di oggetti con lingue e paginazione (page, product, category) data la condizione nel where
	*
	* @param   string  $schema      //Schema db dove effettuare le operazioni
	* @param   string  $where       //Condizione per la query
	* @param   array   $strings     //Strings da includere
	* @param   array   $order       //Order by della query
	* @param   string  $limit       //Limite elementi della query
	* @param   string  $skip        //Salta n elementi
	* @return  array   $objects     //Array di oggetti db formato array
	*/

	static function byWherePagination($schema, $where = "1 = 1", $strings = array('title'), $order = array("updated_at" , "desc"), $limit = "", $skip = "0") {

		if( $limit == "" ) $limit = LabConfig::$labconfigs["limit"];

		$lang = App::getLocale();

		$whereStr = (!is_array($where)) ? $where : $where[0];

		$condTitle = "";

		if(is_array($where))

			$condTitle = (isset($where[1])) ? " and ".$lang." ".$where[1] : "";

		switch($order[0]){

			case "price":

				$objects = $schema::with($strings)->whereHas('title', function( $titlez ) use($lang, $condTitle) {	
			
					$titlez->whereRaw($lang. " != ''".$condTitle);

				})->whereRaw($whereStr." and online = 1 order by CAST(price AS decimal(6,2)) ".$order[1])->paginate($limit);

				break;

			case "title":

				$model = new $schema();				

				$objects = $schema::with($strings)->select($model->getTable().".*",$lang)->join('strings', 'title_id', '=', 'strings.id')

				->whereRaw($lang. " != ''".$condTitle." and ".$whereStr." and online = 1")

				->orderBy($lang , $order[1])->paginate($limit);

				break;

			default:

				$objects = $schema::with($strings)->whereHas('title', function( $titlez ) use($lang, $condTitle) {	
				
					$titlez->whereRaw($lang. " != ''".$condTitle);

				})->whereRaw($whereStr." and online = 1")->orderBy($order[0], $order[1])->paginate($limit);

		}


		foreach($objects as $key => $object) {
			
			foreach($strings as $value) $objects[$key][$value] = $object[$value][$lang];

			$objects[$key]['shematype'] = $schema;

		}

		return $objects;

	}


	/**
	* Ritorna un insieme di oggetti con determinati join data la condizione nel where
	*
	* @param   string  $schema      //Schema db dove effettuare le operazioni
	* @param   string  $where       //Condizione per la query
	* @param   array   $with        //Campi da includere nel join
	* @param   array   $order       //Order by della query
	* @param   string  $limit       //Limite elementi della query
	* @param   string  $skip        //Salta n elementi
	* @return  array   $objects     //Array di oggetti db formato array
	*/

	static function byWhereWith($schema, $where, $with = array(''), $order = array("updated_at" , "desc"), $limit = "18446744073709551615", $skip = "0") {

		$lang = App::getLocale();

		$objects = $schema::with($with)->whereRaw($where)->orderBy($order[0], $order[1])->take($limit)->skip($skip)->get();
		
		foreach($objects as $key => $object) {

			foreach($with as $value) {

				if(isset($object['relations'][$value]))
					
					$objects[$key][$value] = $object['relations'][$value];

			}

		}

		return $objects;


	}


	/**
	* Ritorna un oggetto con lingue(page, product, category) dato l'id
	*
	* @param   string  $schema      //Schema db dove effettuare le operazioni
	* @param   string  $id          //Id row database
	* @param   array   $strings     //Strings da includere
	* @return  array   $object      //Oggetto db formato array
	*/

	static function byId($schema, $id, $strings = array('title', 'url'), $lang = "") {

		if($lang == "")

			$lang = App::getLocale();
		
		$object = $schema::with($strings)->whereHas('title', function( $titlez ) use($lang) {	
			
			$titlez->where($lang, '!=', '');

		})->whereRaw("id =".$id." and online = 1")->first();

		//$object = $schema::with($strings)->find($id);
		
		foreach($strings as $value) {

			if(isset($object[$value]))
				
				$object[$value] = $object['relations'][$value][$lang];

		}

		return $object;

	}


	/**
	* Ritorna l'url in formato stringa dato lo schema e l'id
	*
	* @param   string  $schema      //Schema db dove effettuare le operazioni
	* @param   string  $id          //Id della row
	* @param   string  $lang        //Lingua nella quale ottenere l'url
	* @return  string  $url         //Url finale dell'oggetto richiesto
	*/

	static function getUrl($schema, $id, $lang = "") {

		$lang = ($lang == "") ? App::getLocale() : $lang;

		switch ($schema) {

			case 'Search':

				$langSlug = (array_search($lang, array_keys(Front::$languages)) != 0) ? $lang."/" : "";

				$url = url($langSlug.'search/'.$id);

				return $url;

			break;

			case 'Users':

				$langSlug = (array_search($lang, array_keys(Front::$languages)) != 0) ? $lang."/" : "";

				$url = (isset(Frontusers::$slugs[$lang][$id])) ? url($langSlug.'users/'.Frontusers::$slugs[$lang][$id]) : url($langSlug.'users/'.$id);

				return $url;
			
			break;

			case 'Brands':

				$currentObj = F::byId($schema, $id, array('title'), $lang);
				
				$langSlug = (array_search($lang, array_keys(Front::$languages)) != 0) ? $lang."/" : "";

				if($currentObj)

					$url = url($langSlug.'brands/'.Frontbrands::$slugs[$lang]['detail']."/".$currentObj['code']);

				else

					$url = url($langSlug);

				return $url;
			
			break;

			case 'Cart':

				$langSlug = (array_search($lang, array_keys(Front::$languages)) != 0) ? $lang."/" : "";

				$url = (isset(Frontcart::$slugs[$lang][$id])) ? url($langSlug.'cart/'.Frontcart::$slugs[$lang][$id]) : url($langSlug.'cart/'.$id);

				return $url;
			
			break;

			case 'Newss':

				$currentObj = F::byId($schema, $id, array('url'), $lang);
				
				$langSlug = (array_search($lang, array_keys(Front::$languages)) != 0) ? $lang."/" : "";

				$type = ($currentObj['type'] != "") ? $currentObj['type'] : 'news';

				if($currentObj)

					$url = url($langSlug.'news/'.Frontnews::$slugs[$lang][$type]."/".$currentObj['url']);

				else

					$url = url($langSlug);

				return $url;
			
			break;
			

			default:

				$url = array();

				if($schema == "" || $id == "") 

					return "#";

				$parent = 'parent_id';

				$parent_schema = $schema;

				if($schema == 'Products') {

					$parent = 'category_id';

					$parent_schema = 'Categories';

				}

				$currentObj = F::byId($schema, $id, array('url'), $lang);
				
				$url[] = $currentObj['url'];

				getUrl: if( $currentObj[$parent] != 0 ) {
					
					if($schema == 'Products') 

						$schema = 'Categories';

					$currentObj = F::byId($schema, $currentObj[$parent], array('url'), $lang);

					$url[] = $currentObj['url'];

					$parent = 'parent_id';

					goto getUrl;

				}

				//Se non siamo nella lingua default aggiungo lingua al primo posto
				if(array_search($lang, array_keys(Front::$languages)) != 0)

					$url[] = $lang;

				return URL::to(implode("/", array_reverse($url)));

			break;		

		}

	}

	static function arrayPagination($array) {



	}

	static function getBreadCrumb($page) {

		$elements = array();

		$schema = get_class($page);
		$parent = 'parent_id';

		$parent_schema = $schema;

		if($schema == 'Products') {

			$parent = 'category_id';

			$parent_schema = 'Categories';

		}

		$currentObj = $page;
		$elements[] = $currentObj;
		
		getBread: if( $page[$parent] != 0 ) {
			
			if($schema == 'Products') 

				$schema = 'Categories';

			$currentObj = F::byId($schema, $currentObj[$parent]);

			$elements[] = $currentObj;

			$parent = 'parent_id';

			goto getBread;

		}

		return array_reverse($elements);

	}

	static function checkRecaptcha() {
		
		if(Input::has('g-recaptcha-response') && Input::get('g-recaptcha-response') != "") {
		
			$response = json_decode(

				file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".LabConfig::$labconfigs['recaptchakey']['secret']."&response=".Input::get('g-recaptcha-response')."&remoteip=".$_SERVER['REMOTE_ADDR']

			), true);
	        
	        return $response['success'];

	    } else return false;		

	}

	static function errormessages( $messages, $html = 0 ) {

		$string = "";

		foreach ($messages->all() as $message) {
			 	
			 	if($html == 0)

			 		echo "<li style=\"list-style:none\" class=\"alert alert-danger\"><b>".$message."</b></li>";

			 	else {

			 		$string .= "<li style=\"list-style:none\" class=\"alert alert-danger\"><b>".$message."</b></li>";
			 		return $string;

			 	}


		}

	}

	static function saveObject($schema, $input) {

		if(is_array($schema)) {

				$table = $schema[1];
				$schema = $schema[0];

		} else

		$table = strtolower( $schema );
		
		$object = new $schema;

		//Ciclo tutti i campi passati
		foreach($input as $field => $value) {
			
			if(!$value)

				$value = "";
			
			if( Schema::hasColumn($table, $field))

				$object->$field = $value;

		}

		$object->save();

		return $object;

	}

	/**
	* Salva un oggetto (page, product, category) compreso le stringhe
	*
	* @param   string  $schema      //Schema db dove effettuare le operazioni, se contiene "-",
	*								 la stringa dopo il carattere corrisponde alla tabella in database
	* @param   array   $input       //Campi dell'oggetto da salvare
	* @param   array   $strings     //Strings da creare e includere nell'oggetto
	* @return  array   $object      //Oggetto db appena inserito         
	*/

	//Non è il metodo giusto guardare perchè il push() non funziona
	//Deve sempre avere in input lang
	static function savestrings( $schema, $input, $strings = array('title')) {

		if(is_array($schema)) {

				$table = $schema[1];
				$schema = $schema[0];

		} else

		$table = strtolower( $schema );

		$object = new $schema;
		
		if(isset($input['lang']))

			$lang = $input['lang'];

		else

			$lang = App::getLocale();

		//Ciclo tutti i campi passati
		foreach($input as $field => $value) {
			
			if(!$value)

				$value = "";
			
			//Se il campo corrente è contenuto nell'array stringhe
			//Salvo col metodo related
			if( in_array($field, $strings)) {

				$field .= "_id";

				$string = new Strings;
				
				$string->$lang = $value;

				$string->save();

				$object->$field = $string->id;
				

			}
			elseif( Schema::hasColumn($table, $field))

				$object->$field = $value;

		}

		$object->save();

		if( Schema::hasColumn($table, 'ordination'))
		{
			
			$object->ordination = $object->id;
			$object->save();

		}

		return $object;
	}

	/**
	* Esegue l'update di un oggetto (page, product, category) compreso le stringhe dato lo schema e id
	*
	* @param   string  $schema      //Schema db dove effettuare le operazioni, se contiene "-",
	*								 la stringa dopo il carattere corrisponde alla tabella in database
	* @param   array   $input       //Campi dell'oggetto da salvare
	* @param   array   $strings     //Strings da creare e includere nell'oggetto
	* @return  -       -           
	*/

	//Deve sempre avere in input id e lang
	static function editstrings( $schema, $input, $strings) {

		if(is_array($schema)) {

				$table = $schema[1];
				$schema = $schema[0];

		} 
		else

		$table = strtolower( $schema );

		$object = $schema::find($input['id']);

		if(isset($input['lang']))

			$lang = $input['lang'];

		else

			$lang = App::getLocale();

		//Ciclo tutti i campi passati
		foreach($input as $field => $value) {
			
			//Se il campo corrente è contenuto nell'array stringhe
			//Salvo col metodo related
			if( in_array($field, $strings))
			
			{
				$field .= "_id";
				
				$string = Strings::find($object[$field]);

				$string->$lang = $value;

				$string->save();
				
			}

			elseif( Schema::hasColumn($table, $field) && $field != "id")

				$object->$field = $value;

				
		}

		$object->save();

		return $object;
	}

	static function loadobject( $p, $html = "", $lang = "") {

		if($lang == "")

			$lang = App::getLocale();
		
		$fields = explode(",", preg_replace('/\s+/', '', $p['fields']));

		$strings = Array();

		foreach($fields as $k => $value)

		{

			if(strpos($value, '@') === 0)

				$strings[] = substr($value , 1);
			
		}

		$object = F::byId($p['schema'], $p['id'], $strings, $lang);

		if(count($strings)>0)
		{

			foreach($strings as $value)

				$stringsobj[$value] = $object[$value];

			$object['objectstrings'] = $stringsobj;

		}

		if(isset($object['publish_at']))

			$object['publish_at'] = date("d-m-Y", strtotime($object['publish_at']));


		$object['media'] = I::getimages($p['schema'], $p['id']);

		//Se html è vuoto di default carico la gallery
		if($html == "")
		{
			
			if (View::exists('Back::views.gallery'))
			{

				$object['what'] = $p;
				
				$view = View::make('Back::views.gallery', $object);

				$object['html'] = $view->render();

			}
			else

				echo "La view non esiste";

		} else

			$object['html'] = $html;

		echo json_encode($object);

	}

	static function deleteobject( $p ) {

		$object = $p['schema']::find($p['id']);

		$object->delete();


	}

	static function loadgallery( $p ) {

		if (View::exists('Back::views.gallery'))

			{
					
				$object['media'] = I::getimages($p['schema'], $p['id']);
				
				$object['what'] = $p;

				$view = View::make('Back::views.gallery', $object);

				$object = $view->render();

			}

			else

				echo "La view non esiste";

			echo $object;

	}

	static function notify( $type, $string, $xpos = "center", $ypos = "center" ) {

        echo "<script>";

        echo 'jSuccess( "'.$string.'", {
            HorizontalPosition: "'.$xpos.'",
            VerticalPosition: "'.$ypos.'",
            ShowOverlay: true,
            TimeShown: 2000,
            OpacityOverlay: 0.5,
            MinWidth: 250
        });';

		echo "</script>";
	
	}

	static function parsedate($timestamp, $type, $limit = 255) {

		$strings = Array(

			"mesi" => Array(

				"it" => Array("Gennaio","Febbraio","Marzo","Aprile","Maggio","Giugno","Luglio","Agosto","Settembre","Ottobre","Novembre","Dicembre"),
				"en" => Array("January","February","March","April","May","June","July","August","September","October","November","December"),
				"de" => Array("Januar","Februar","März","April","Mai","Juni","Juli","August","September","Oktober","November","Dezember"),
				"fr" => Array("janvier","février","mars","avril","mai","juin","juillet","août","septembre","octobre","novembre","décembre")

			),

			"giorni" => Array(

				"it" => Array("Domenica","Lunedì","Martedì","Mercoledì","Giovedì","Venerdì","Sabato"),
				"en" => Array("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"),
				"de" => Array("Sonntag","Montag","Dienstag","Mittwoch","Donnerstag","Freitag","Samstag"),
				"fr" => Array("dimanche","lundi","mardi","mercredi","jeudi","vendredi","samedi")				

			)

		);

		$date = date_parse( $timestamp );

		switch ($type) {

			case 'giorni':

				return $date['day'];	

			break;

			case 'giorniint':

				return $date['day'];	

			break;

			case 'giornistring':

				return substr($strings['giorni'][App::getLocale()][date( "w", strtotime($timestamp))], 0, $limit);
	

			break;

			case 'mesi':

				return substr($strings['mesi'][App::getLocale()][$date['month']-1], 0, $limit);

			break;

			case 'anno':

				return $date['year'];

			break;
			
			default:
				
				return substr($date[$type], 0, $limit);
				
			break;
		}


	}


	static function money( $type, $string ) {

		switch ($type) {

			case 'tostring':
				
				return number_format((float) $string, 2, ',', '.');

			break;

			case 'tonumber':

				//Converto "," in "."
			    $s = str_replace(',', '.', $string);
			
			    //Rimuovo tutto tranne numeri e punti "."
			    $s = preg_replace("/[^0-9\.]/", "", $s);
			
			    //Rimuovo tutti i separatori dalla prima parte e prendo la fine
			    $s = str_replace('.', '',substr($s, 0, -3)) . substr($s, -3);
				
			    return (float) $s;

			break;

			case 'topaypal':

				$sostcosa = array( '.', ',');
				$sostcon = array( ',', '.');
				$prezzo_pay = str_replace($sostcosa, $sostcon, $string);

				return floatval($prezzo_pay);

			break;

			case 'tostripe':

				$number = self::money('tonumber', $string);

				return $number * 100;

			break;
			
			default:
			
			break;

		}

		

	}

	/**
	* Calcola le spese di spedizione a seconda del numero di oggetti nel carrello
	*
	* @return  string  //Campo price corrispondente nel db
	*/

	static function shippingfees() {

		$items = 0;

		foreach($_SESSION['cart']['items'] as $product => $quantity)

			$items += $quantity;

		$fees = ShippingFees::whereRaw('fromz <='.$items.' and toz >='.$items)->first();

		return $fees['price'];

	}

	/**
	* Restituisce la stringa in formato slug url
	*
	* @param   string  $string        //Stringa da convertire in url
	* @return  string  preg_replace   //Stringa convertita in slug           
	*/

	static function escapeFromUrl($string) {

		$ts = array("/[À-Å]/","/Æ/","/Ç/","/[È-Ë]/","/[Ì-Ï]/","/Ð/","/Ñ/","/[Ò-ÖØ]/","/×/","/[Ù-Ü]/","/[Ý-ß]/","/[à-å]/","/æ/","/ç/","/[è-ë]/","/[ì-ï]/","/ð/","/ñ/","/[ò-öø]/","/÷/","/[ù-ü]/","/[ý-ÿ]/","/ /");
    	$tn = array("A","AE","C","E","I","D","N","O","X","U","Y","a","ae","c","e","i","d","n","o","x","u","y","-");
    	return preg_replace($ts, $tn, $string);

	}


	/**
	* Restituisce la stringa cryptata
	*
	* @param   string  $string      //Stringa da cryptare
	* @return  string  $string   	//Stringa cryptata           
	*/

	static function encryptString($string) {

	   $from = array("g", "a", "e", "i", "o", "c");
	   $to = array("@", "-", "*", "#", "§", "^_");

	   $hashed = str_replace($from, $to, $string);

	   return base64_encode(serialize(base64_encode(serialize(base64_encode($hashed)))));

	}

	/**
	* Restituisce la stringa cryptata
	*
	* @param   string  $string      //Stringa da cryptare
	* @return  string  $string   	//Stringa cryptata           
	*/

	static function decryptString($string) {

	   $from = array("@", "-", "*", "#", "§", "^_");
	   $to = array("g", "a", "e", "i", "o", "c");

	   $hashed = str_replace($from, $to, $string);

	   return $hashed;

	}

	/**
	* Restituisce l'ultima query eseguita
	*
	* @return  string  //Ultima query eseguita in formato stringa
	*/

	static function lastQuery() {

		$queries = DB::getQueryLog();
		
		$last_query = end($queries);

		return $last_query['query'];

	}


	/**
	* Restituisce la data in formato corretto per il db
	*
	* @param   string  $date      	//Data in stringa da convertire
	* @return  string  $newdate     //Data convertita per il db           
	*/

	static function mysqldate($date) {

		$db_date = date("Y-m-d", strtotime($date));

		return $db_date;

	}


	static function subtime($days) {

		return date("Y-m-d", time() - (24 * $days * 60 * 60));

	}

	/**
	 *
	 *@param [] $[name] [<description>]
	 * 
	 */

	static function addnotification($type, $title, $description, $priority = 0) {

		F::savestrings('Notifications', array("type" => $type, "title" => $title, "description" => $description, "priority" => $priority), array("title", "description"));

	}

	static function getSubObj($id, $model, $delimiters, $subDelimiters, $count = 0) {

		$array = F::byWhere($model, 'parent_id = '.$id, array('title'), array("ordination" , "asc"));

		if(count($array) > 0) {

			//Ciclo elementi
			foreach($array as $object) {
				
				//Se è id padre 0 o ha semplicemente figli, stampo grassetto
				if($model::where("parent_id", $object['id'])->count() > 0) {

					echo "<option onclick='getBtype(".$object['id'].")' data-idconsole='".$object['idconsole']."' id='option_".$object['id']."' style='font-weight:bold;padding-left:".(20 * $count)."px' value='".$object['id']."' data-btype='".$object['btype']."'>".$object['title']."</option>";

					F::getSubObj($object['id'], $model, $delimiters, $subDelimiters, ($count + 1));


				} elseif($object['parent_id'] == 0)

					echo "<option onclick='getBtype(".$object['id'].")' data-idconsole='".$object['idconsole']."' id='option_".$object['id']."' style='font-weight:bold;margin:5px 0 5px 0' value='".$object['id']."' data-btype='".$object['btype']."'>".$object['title']."</option>";

				else

					echo "<option onclick='getBtype(".$object['id'].")' data-idconsole='".$object['idconsole']."' id='option_".$object['id']."' data-btype='".$object['btype']."' value='".$object['id']."' style='padding-left:".(20 * $count)."px'>".$object['title']."</option>"; 
					
			}

		}

	}

	/**
	* Esegue ricerca di uno o più parametri su un array multidimensionale
	*
	* @param   array   $array      	//Array dove eseguire la ricerca
	* @param   array   $search      //Array di parametri da cercare
	* @param   string  $elements    //Numero di elementi da ritornare, primo o tutti
	* @param   string  $cast      	//Tipo di cast da eseguire ad ogni iterazione
	* @return  array   $return      //Array cercato di ritorno         
	*/

	static function arrayMultiSearch($array, $search, $elements = 'first', $cast = 'toArray' ) {

	    //Inizializzo
	    $result = array();

        //Foreccio elementi array
        foreach ($array as $key => $value) {

        	switch ($cast) {

        		case 'toArray':
        			
    				$value = (array) $value;

        		break;
        	}
	        
	        //Foreccio la condizione di ricerca
	        foreach ($search as $k => $v) {

		        // Se non trovo la condizione di ricerca continuo avanti
		        if (!isset($value[$k]) || $value[$k] != $v)
		        
		        	continue 2;
	           

	        }

	        // Aggiungo chiave all'array finale
	        $result[] = $value;

        }

       	switch ($elements) {

    		case 'first':
    			
    			if(isset($result[0]))
	
					return $result[0];
				
				else

					return false;

    		break;

    		case 'all':
    			
				return $result;

    		break;

    	}

        

    }





    // Funzione ricorsiva che data una categoria calcola le categorie 'foglie' ad essa figlie

	static function subCats( $cat ){

		$idSubCats = Categories::select('id')->whereRaw('parent_id = ' . $cat)->get();

		if( count($idSubCats) == 0 )

			return $cat.",";

		else {

			$strSubCat = "";

			foreach ($idSubCats as $idSubCat) {

				$strSubCat .= F::subCats($idSubCat['id']);
				
			}

			return ($strSubCat);

		}

	}



	// Funzione che attraverso la funzione ricorsiva 'subCats' mi costruisce l'array delle categorie foglie

	static function subCatsArray( $cat ){

		$subCatsId = self::subCats( $cat );

		$subCatsId = substr( $subCatsId, 0, strlen($subCatsId) -1 );

		return "(" . $subCatsId . ")";


	}




    /**
	* Restituisce i prodotti appartenenti alle categorie foglie della categoria data
	*
	* @param   int      //Id categoria
	* @return  array   //Array prodotti cercati
	*/

	static function getProductsOfCat($cat){

		$cats = self::subCatsArray($cat);

		return F::byWhere('Products', 'category_id in '.$cats, array('title', 'shortdescription'));


	}




	/**
	* Costruisce la condizione where prendendo i dati dall'url della pagina
	*
	* @return  
	*/

	static function makeWhereByUrl(){

		$where = " 1=1 ";

		if( Input::get('settore') && Input::get('settore') != "all"){

			$tags = RelatedProductsTags::select('product_id')
  				->where('tag_id', Input::get('settore') )
  				->groupBy('product_id')
  				->get();

  			$tagsId = json_decode($tags,true);

  			$value = implode(",", array_column($tagsId, 'product_id'));

  			$where = "products.id IN (" .$value. ") ";			

		}



		$filtri = array('certificazione', 'suola', 'tipologiamodello', 'sesso');

		if( Input::get("sesso") == "F" )
			Input::merge(array('sesso' => 'M/F'));

		foreach( $filtri as $filtro ){

			if( Input::get($filtro) && Input::get($filtro) != "all" ){

				$products = Products::select('id')
	  				->where($filtro, Input::get($filtro) )
	  				->groupBy('id')
	  				->get();

	  			$productsId = json_decode($products,true);

	  			$value = implode(",", array_column($productsId, 'id'));

	  			if( $value != "" )

	  				$where = $where." and products.id IN (" .$value. ")";

	  			else

	  				$where = " 1 != 1 ";

			}

		}
		

		$cat = Input::get('category_id');

		if($cat != 0){

			$idSubCat = F::subCats($cat);
			$idSubCat = substr( $idSubCat, 0, strlen($idSubCat) -1 );

			$where = $where . ' and category_id in (' . $idSubCat .')';

		}



		return $where;

	}





}
