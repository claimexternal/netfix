<?php

 /**
   * labconfig
   * Racchiude tutte le impostazioni statiche e dinamiche dell'installazione attiva
   *
   * 
   * 02/03/2015
   * 
   * @package    bCMS
   * @subpackage Custom classes
   * @author     Enrico Pettinao <enricopettinao@gmail.com>
   */

class LabConfig {
	
	static $labconfigs = array(
		
		//Definisco dominio per lo sviluppo
		'devdomain' => 'bcommunication.it',

        'repository' => 'https://martinamainer:M5e6qSfDeLFbW6jxpajN@bitbucket.org/claimexternal/netfix.git',

		'rootdomain' => array(

			'dev' => 'http://cms.bconsole.test:8888',
			'demo' => 'https://devcms.bconsole.com',
			'prod' => 'https://cms.bconsole.com'

		),

		'codist' => 'netfix',

		//Definisco i database da utilizzare
		'database' => array(

			'dev' => array(

					'host' => 'localhost',
					'name' => 'cms_netfix',
					'user' => 'root',
					'password' => 'root'
					//'port' => '39341'

				),

			'demo' => array(

					'host' => '46.28.2.100',
					'name' => 'cms_netfix',
					'user' => 'netfix_cms',
					'password' => '0w8ym!R5'

				),

			'prod' => array(

					'host' => '46.28.2.100',
					'name' => 'cms_netfix',
					'user' => 'netfix_cms',
					'password' => '0w8ym!R5'

				)
		),

		'recaptchakey' => array(

			'public' => '6Lfz2qwZAAAAAPVztJkP2x-Hpj4D6zIT1EijlgyK',
			'secret' => '6Lfz2qwZAAAAABZ-uJJWN_oQzmfXgeF_3Saxsl6n'

		),

		//Definisco le configurazioni per l'invio delle email
		'email' => array(

			'dev' => array(

					'host' => 'pro.eu.turbo-smtp.com',
					'domain' => 'bcommunication.it',
					'secret' => 'key-6561931fbcc83e913135c9e8e6d517ed',
					'port' => '465',
					'from' => 'bcms@bcommunication.it',
					'name' => 'Website Admin'
					//'port' => '39341'

				),

			'demo' => array(

					'host' => 'pro.eu.turbo-smtp.com',
					'domain' => 'bcommunication.it',
					'secret' => 'key-6561931fbcc83e913135c9e8e6d517ed',
					'port' => '465',
					'from' => 'bcms@bcommunication.it',
					'name' => 'Website Admin'
					//'port' => '39341'

				),

			'prod' => array(

					'host' => 'pro.eu.turbo-smtp.com',
					'domain' => 'bconsole.com',
					'secret' => 'c2e6a8a4070f287bb01c9f17c8ddfab4-cb3791c4-bbb0024d',
					'port' => '465',
					'from' => 'bcms@bconsole.com',
					'name' => 'Website Admin'

				)
		),

		//Istanze url con relativa view
		"routertables" => array(

			'pages' => 'skeleton,content',
			'categories' => 'skeleton,category',
			'products' => 'skeleton,product'

		),

		//Contatti admin
		"admincontacts" => array(

			"dev" => array(

				"email" => "martina.mainer@bcommunication.it",
				"emailCcn" => "martina.mainer@bcommunication.it",
				//"emailAdmin1" => "martina.mainer@bcommunication.it",
				//"emailAdmin2" => "martina.mainer@bcommunication.it",
				"name" => "Developer",
				"nameCcn" => "Developer"

			),

			"demo" => array(

				"email" => "martina.mainer@bcommunication.it",
				"emailCcn" => "martina.mainer@bcommunication.it",
				//"emailAdmin1" => "martina.mainer@bcommunication.it",
				//"emailAdmin2" => "martina.mainer@bcommunication.it",
				"name" => "Developer",
				"nameCcn" => "Developer"

			),


			"prod" => array(

				"email" => "info@expyou.com ",
				"emailCcn" => "form@claimbs.com",
				"emailAdmin1" => "info@expyou.com ",
				// "emailAdmin2" => "anuparp.furlan@bcommunication.it",
				"name" => "NetFix",
				"nameCcn" => "Form Claim"

			),

		),		

		"limit" => 8,


	);

	static function get( $indexkey, $secondindex = "" ) {

		if( $secondindex == "" )

			return self::$labconfigs[$indexkey];

		else

			return self::$labconfigs[$indexkey][$secondindex];

	}

	//Funzioni custom da inizializzare per l'applicazione
	static function init( $array ) {

		foreach($array as $class => $action) {

			$class::$action();

		}

	}


	static function deployment() {

		$branch = (strpos(Request::url(), '.bcommunication.it')) ? "dev" : "master";

		exec("git pull ".self::$labconfigs['repository']." ".$branch);

	}


}


