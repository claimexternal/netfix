<?php
	
 use Illuminate\Database\Eloquent\SoftDeletingTrait;

 class EmailTemplates extends Eloquent {

 	use SoftDeletingTrait;

 	protected $table = 'emailtemplates';
 	protected $dates = ['deleted_at'];


 	public function subject()
 		
 		{
 			
 			return $this->belongsTo('Strings');

 		}

 	public function body()
 		
 		{

 			return $this->belongsTo('Strings');

 		}


}
