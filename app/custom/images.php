<?php


 /**
   * I placeholder per Images
   * Funzioni relative alla manipolazione e estrapolazione media
   *
   * 
   * 09/04/2015
   * 
   * @package    bFrontEnd
   * @subpackage Custom Classes
   * @author     Enrico Pettinao <enricopettinao@gmail.com>
   */

class I {

	static function getImages($schema, $id, $width, $height, $limit = "18446744073709551615", $copertine = 1) {

		$lang = App::getLocale();

		$codist = LabConfig::$labconfigs['codist'];

		$env = (strpos(Request::server('HTTP_HOST'), LabConfig::$labconfigs['devdomain'])) ? 'prod' : F::getEnv();

		$rootdomain = LabConfig::$labconfigs['rootdomain'][$env];

		$includecopertine = ($copertine == 0) ? " and copertine = 0" : "";

		if($limit != 1)
		
			$objects = Medias::with('title')->whereRaw('parent_table ="'.$schema.'"'.$includecopertine.' and type="image" and parent_id ='.$id)->orderBy('ordination')->take($limit)->get();
		
		else

			$objects = Medias::with('title')->whereRaw('parent_table ="'.$schema.'" and type="image" and copertine = 1 and parent_id ='.$id)->orderBy('ordination')->take($limit)->get();

		$tmparray = array();

		foreach($objects as $key => $object) {

			$tmparray[$key]['id'] = $object['id'];
			$tmparray[$key]['created_at'] = $object['created_at'];
			$tmparray[$key]['name'] = $object['name'];
			$tmparray[$key]['online'] = $object['online'];
			$tmparray[$key]['copertine'] = $object['copertine'];
			$tmparray[$key]['parent_table'] = $object['parent_table'];
			$tmparray[$key]['parent_id'] = $object['parent_id'];

			if($width != "" && $height != "")

				$tmparray[$key]['src'] = $rootdomain.'/image/'.$width.'/'.$height.'/'.$object['name'].'/'.$codist;

			else

				$tmparray[$key]['src'] = $rootdomain.'/image/0/0/'.$object['name'].'/'.$codist;				

			if(isset($object['relations']['title']))
					
				$tmparray[$key]['title'] = $object['relations']['title'][$lang];

		}

		return $tmparray;

	}

	static function getCopertine($schema, $id, $width, $height) {

		$codist = LabConfig::$labconfigs['codist'];

		$env = (strpos(Request::server('HTTP_HOST'), LabConfig::$labconfigs['devdomain'])) ? 'prod' : F::getEnv();
		
		$rootdomain = LabConfig::$labconfigs['rootdomain'][$env];

		$object = Medias::with('title')->whereRaw('parent_table ="'.$schema.'" and type="image" and copertine=1 and parent_id ='.$id)->first();

		if(!$object)

			return $rootdomain.'/image/'.$width.'/'.$height.'/No image/'.$codist;

		if($width != "" && $height != "")

			return $rootdomain.'/image/'.$width.'/'.$height.'/'.$object['name'].'/'.$codist;

		else

			return $rootdomain.'/image/0/0/'.$object['name'].'/'.$codist;

	}


	static function geturlcopertine($schema, $id, $width, $height, $default = "No Image") {

		$object = Medias::with('title')->whereRaw('parent_table ="'.$schema.'" and type="image" and copertine=1 and parent_id ='.$id)->first();

		if($object)

			return URL::to('image/'.$width.'/'.$height.'/'.$object['name']);

		//Creo immagine di default
		else
		{	
						
			return URL::to('image/'.$width.'/'.$height.'/'.$default.'.jpg');

		}
	}

	static function getNaturalImage($name) {

		$codist = LabConfig::$labconfigs['codist'];

		$env = (strpos(Request::server('HTTP_HOST'), LabConfig::$labconfigs['devdomain'])) ? 'prod' : F::getEnv();
		
		$rootdomain = LabConfig::$labconfigs['rootdomain'][$env];

		return $rootdomain.'/image/0/0/'.$name.'/'.$codist;



	}

	static function getImageById($id, $width = 0, $height = 0) {

		$codist = LabConfig::$labconfigs['codist'];

		$env = (strpos(Request::server('HTTP_HOST'), LabConfig::$labconfigs['devdomain'])) ? 'prod' : F::getEnv();
		
		$rootdomain = LabConfig::$labconfigs['rootdomain'][$env];

		$object = Medias::with('title')->whereRaw('type="image" and id ='.$id)->first();

		return $rootdomain.'/image/'.$width.'/'.$height.'/'.$object['name'].'/'.$codist;

	}

	static function getCurl($url, $name) {

		$source = $url;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $source);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_SSLVERSION,3);
		$data = curl_exec ($ch);
		$error = curl_error($ch); 
		curl_close ($ch);

		$destination = __DIR__.'/../media/'.$codist.'/'.$name;
		$file = fopen($destination, "w+");
		fputs($file, $data);
		fclose($file);



	}


}