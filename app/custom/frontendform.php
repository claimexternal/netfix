<?php

 /**
   * FrontendForm
   * Costruisco i form in html direttamente dal builder in backend
   *
   * 
   * 01/10/2015
   * 
   * @package    BfrontEnd
   * @subpackage Custom classes
   * @author     Enrico Pettinao <enricopettinao@gmail.com>
   */

class FrontendForm {


   static function replaceDescription($start, $end, $page) {

         $matches = array();
         
         $regex = "/$start([a-zA-Z0-9_]*)$end/";
         
         $description = $page['description'];

         preg_match_all($regex, $description, $matches);
         
         if(count($matches[1]) > 0) {

            foreach($matches[1] as $formcode) {

               $formBuilder = FormBuilder::with('title')->where('code', $formcode)->first();

               $array['page'] = $page;
               
               $array['form'] = (array) $formBuilder['attributes'];

               $array['form']['title'] = $formBuilder['title'][App::getLocale()];

               foreach($formBuilder->fields as $field) {

                  if($field['options'] != "")
            
                     $field['options'] = unserialize($field['options']);

                  else 

                     $field['options'] = "";

                  $field['title'] = Strings::find($field['title_id']);
                  $field['error'] = Strings::find($field['error_id']);

                  $array['form']['fields'][] = $field;

               }

               $description = str_replace($start.$formcode.$end, View::make( 'default.formbuilder', $array )->render(), $description);

            }

         }

         return $description;

   }

   static function drawForm($code, $page) {

      $formBuilder = FormBuilder::with('title')->where('code', $code)->first();

      if(!$formBuilder)

         return "form ".$code." non trovato.";

      $array['page'] = $page;
      
      $array['form'] = (array) $formBuilder['attributes'];

      $array['form']['title'] = $formBuilder['title'][App::getLocale()];

      foreach($formBuilder->fields as $field) {

         if($field['options'] != "")
   
            $field['options'] = unserialize($field['options']);

         else 

            $field['options'] = "";

         $field['title'] = Strings::find($field['title_id']);
         $field['error'] = Strings::find($field['error_id']);

         $array['form']['fields'][] = $field;

      }

      return View::make( 'default.formbuilder', $array )->render();
      
   }

}