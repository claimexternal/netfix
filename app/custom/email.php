<?php

/*
|
| Author: Enrico Pettinao
| 04/01/2015
| class f instead Email
*/


class e {


	static function send($code, $to, $toname, $replaces = "") {

		$email = EmailTemplates::with('subject', 'body')->whereRaw('code ="'.$code.'"')->first();

		$admincontact = LabConfig::get('admincontacts', F::getEnv());

		if($email['sender'] != "")

			$from = $email['sender'];

		else

			$from = $admincontact['email'];

		if($replaces == "")

			$content['content'] = $email['body'][App::getLocale()];

		else

			$content['content'] = self::getreplace($email['body'][App::getLocale()], $replaces);

		$recipient = Array(

			"from" => $from,
			"adminname" => $admincontact['name'],
			"to" => $to,
			"toname" => $toname,
			"subject" => $email['subject'][App::getLocale()]

		);

		Mail::send('emails.default', array('content' => $content['content']), function($message) use($recipient, $to) {
			
			$message->from($recipient['from'], $recipient['adminname']);
			
			if(!is_array($to))

			    $message->to($recipient['to'], $recipient['toname'])->subject($recipient['subject']);

			else {

				$i = 0;

				foreach($to as $email => $name) {

					if($i == 0)
						
						$message->to("enricopettinao@gmail.com", $name)->subject($recipient['subject']." --> ".$email." ".$name);

					else

						$message->bcc("enricopettinao@gmail.com", $name)->subject($recipient['subject']." --> ".$email." ".$name);

					$i++;
				}


			}
		
		});

	}

	static function multiSend($code, $to, $replaces = "") {

		$email = EmailTemplates::with('subject', 'body')->whereRaw('code ="'.$code.'"')->first();

		$admincontact = LabConfig::get('admincontacts', F::getEnv());

		if($email['sender'] != "")

			$from = $email['sender'];

		else

			$from = $admincontact['email'];

		if($replaces == "")

			$content['content'] = $email['body'][App::getLocale()];

		else

			$content['content'] = self::getreplace($email['body'][App::getLocale()], $replaces);

		$recipient = Array(

			"from" => $from,
			"adminname" => $admincontact['name'],
			"subject" => $email['subject'][App::getLocale()]

		);

		Mail::send('emails.default', array('content' => $content['content']), function($message) use($recipient, $to) {
			
			$message->from($recipient['from'], $recipient['adminname']);
			
			$i = 0;

			if(isset($to['to'])) foreach($to['to'] as $email => $name) $message->to($email, $name)->subject($recipient['subject']);

			if(isset($to['bcc'])) foreach($to['bcc'] as $email => $name) $message->bcc($email, $name)->subject($recipient['subject']);

			if(isset($to['cc'])) foreach($to['cc'] as $email => $name) $message->cc($email, $name)->subject($recipient['subject']);

		});

	}

	static function getreplace( $content, $replaces ) {

		foreach($replaces as $what => $with) {

			if (strpos($content, $what) !== false){

				$content = str_replace($what, $with, $content);

			}

		}

		return $content;

	}


}