<?php

class labdatabase {
	
	static function initializedb() {
		
		function langscolumns( $table ) {

			$languages = Front::$languages;

			foreach( $languages as $code => $label ) {

				$table->text( $code );

			}

		}

		function editlangscolumns( $code, $table ) {

			//Creo in automatico le colonne di lingua
			if ( !Schema::hasColumn('strings', $code) )

				$table->text($code);		

		}

				/*
				|
				| Inizializzo DB
				|
				*/

				/*-------------------------------------------------
				|
				| CREAZIONE TABELLA STRINGHE MULTILINGUA
				|
				*/

				if( !Schema::hasTable('strings') )	{

					Schema::create('strings', function( $table )
					{

						$table->increments('id');
						$table->softDeletes();
						$table->timestamps();

						langscolumns($table);


					});

				} else {

					//Aggiungo lingue in db in caso ne trovo di nuove nella config 
					Schema::table('strings', function( $table )
						{	
							$languages = Front::$languages;

							foreach( $languages as $code => $label )

								editlangscolumns( $code, $table );
							
						});

				}

				/*
				|
				|
				-------------------------------------------------- */

				//Media
				if( !Schema::hasTable('medias') )	{

					Schema::create('medias', function( $table )
					{

						$table->increments('id');
						$table->softDeletes();
						$table->timestamps();
						$table->integer('parent_id');
						$table->string('parent_table');
						$table->string('name');
						$table->boolean('visible')->default(1);
						$table->boolean('copertine')->default(0);
						$table->integer('ordination');
						$table->unsignedInteger('title_id'); //S
						
						//Foreign key
						$table->foreign('title_id')->references('id')->on('strings');
						
					});

				}

				//Notifiche
				if( !Schema::hasTable('notifications') )	{

					Schema::create('notifications', function( $table )
					{

						$table->increments('id');
						$table->softDeletes();
						$table->timestamps();
						$table->string('type'); //Order, message, contact
						$table->integer('priority');
						$table->boolean('seen')->default(0);
						$table->unsignedInteger('title_id'); //S
						$table->unsignedInteger('description_id'); //S
						
						//Foreign key
						$table->foreign('title_id')->references('id')->on('strings');
						$table->foreign('description_id')->references('id')->on('strings');
						
					});

				}


				/* ------------------------------------------------------------------ */


				//Pagine Menu
				if( !Schema::hasTable('pages') )	{

					Schema::create('pages', function( $table )
					{

						$table->increments('id');
						$table->softDeletes();
						$table->timestamps();
						$table->boolean('visible')->default(1);
						$table->integer('parent_id');
						$table->boolean('inmenu')->default(1);
						$table->integer('ordination');
						$table->boolean('online')->default(1);
						$table->unsignedInteger('title_id'); //S
						$table->unsignedInteger('description_id'); //S
						$table->unsignedInteger('shortdescription_id'); //S
						$table->unsignedInteger('url_id');
						$table->unsignedInteger('metatitle_id');
						$table->unsignedInteger('metadescription_id');
						$table->unsignedInteger('metakeywords_id');
						//$table->primary('id');

						//Foreign key
						$table->foreign('title_id')->references('id')->on('strings');
						$table->foreign('description_id')->references('id')->on('strings');
						$table->foreign('shortdescription_id')->references('id')->on('strings');
						$table->foreign('url_id')->references('id')->on('strings');
						$table->foreign('metatitle_id')->references('id')->on('strings');
						$table->foreign('metadescription_id')->references('id')->on('strings');
						$table->foreign('metakeywords_id')->references('id')->on('strings');

					});

					//Creo Homepage
					F::savestrings('Pages', array('title' => 'Homepage',
												 'description' => '',
												 'shortdescription' => '', 
												 'url' => '', 
												 'metatitle' => 'Homepage', 
												 'metadescription' => '', 
												 'metakeywords' => '',
												 'lang' => 'it'),

										     array('title','description','shortdescription','url','metatitle','metadescription','metakeywords','metadescription'));

				}


				/* ------------------------------------------------------------------ */


				//Categorie
				if( !Schema::hasTable('categories') )	{

					Schema::create('categories', function( $table )
					{

						$table->increments('id');
						$table->softDeletes();
						$table->timestamps();
						$table->boolean('visible')->default(1);
						$table->integer('visits')->default(0);
						$table->integer('parent_id');
						$table->integer('ordination');
						$table->unsignedInteger('title_id'); //S
						$table->unsignedInteger('description_id'); //S 
						$table->unsignedInteger('shortdescription_id'); //S
						$table->unsignedInteger('url_id'); //S
						$table->unsignedInteger('metatitle_id');
						$table->unsignedInteger('metadescription_id');
						$table->unsignedInteger('metakeywords_id');
						//$table->primary('id');

						//Foreign key
						$table->foreign('title_id')->references('id')->on('strings');
						$table->foreign('description_id')->references('id')->on('strings');
						$table->foreign('shortdescription_id')->references('id')->on('strings');
						$table->foreign('url_id')->references('id')->on('strings');
						$table->foreign('metatitle_id')->references('id')->on('strings');
						$table->foreign('metadescription_id')->references('id')->on('strings');
						$table->foreign('metakeywords_id')->references('id')->on('strings');

					});

				}

				
				/* ------------------------------------------------------------------ */
				

				//Prodotti
				if( !Schema::hasTable('products') )	{

					Schema::create('products', function( $table )
					{

						$table->increments('id');
						$table->softDeletes();
						$table->timestamps();
						$table->integer('visits')->default(0);
						$table->boolean('visible')->default(1);
						$table->boolean('inhome')->default(0);
						$table->boolean('sale')->default(0);
						$table->unsignedInteger('category_id');
						$table->integer('ordination');
						$table->unsignedInteger('title_id'); //S
						$table->unsignedInteger('description_id'); //S
						$table->unsignedInteger('shortdescription_id'); //S
						$table->string('price');
						$table->string('code');
						$table->unsignedInteger('url_id'); //S
						$table->unsignedInteger('metatitle_id');
						$table->unsignedInteger('metadescription_id');
						$table->unsignedInteger('metakeywords_id');
						//$table->primary('id');

						//Foreign key
						$table->foreign('title_id')->references('id')->on('strings');
						$table->foreign('description_id')->references('id')->on('strings');
						$table->foreign('shortdescription_id')->references('id')->on('strings');
						$table->foreign('url_id')->references('id')->on('strings');
						$table->foreign('category_id')->references('id')->on('categories');
						$table->foreign('metatitle_id')->references('id')->on('strings');
						$table->foreign('metadescription_id')->references('id')->on('strings');
						$table->foreign('metakeywords_id')->references('id')->on('strings');

					});

				}

				
				/* ------------------------------------------------------------------ */
				

				//Users
				if( !Schema::hasTable('users') )	{

					Schema::create('users', function( $table )
					{

						$table->increments('id');
						$table->softDeletes();
						$table->timestamps();
						$table->boolean('visible')->default(1);
						$table->string('remember_token', 100);
						$table->string('name');
						$table->string('email');
						$table->string('password', 60);
						$table->string('role')->default('user');
						$table->date('birthday');
						$table->string('telephone');
						$table->string('address');
						$table->string('city');
						$table->string('post_code');
						$table->string('province');
						$table->string('country');

						$table->string('username');


					});

					$user = new User();

					$user->name = "Admin";
					$user->email = "enricopettinao@gmail.com";
					$user->password = Hash::make('00bb335add');
					$user->role = "admin";
					$user->username = "admin";

					$user->save();

				}

				/* ------------------------------------------------------------------ */

				//Visite
				if( !Schema::hasTable('visits') )	{

					Schema::create('visits', function( $table )
					{

						$table->increments('id');
						$table->softDeletes();
						$table->timestamps();
						$table->integer('typeid'); 
						$table->string('type'); 
						$table->string('useragent');
						$table->string('ip');
						$table->integer('user_id');
						
					});

				}

				/**/

				/* ------------------------------------------------------------------ */

				//News
				if( !Schema::hasTable('newss') )	{

					Schema::create('newss', function( $table )
					{

						$table->increments('id');
						$table->softDeletes();
						$table->timestamps();
						$table->date('publish_at');
						$table->boolean('visible')->default(1);
						$table->integer('ordination');
						$table->unsignedInteger('title_id'); //S
						$table->unsignedInteger('description_id'); //S 
						$table->unsignedInteger('shortdescription_id'); //S
						$table->unsignedInteger('url_id'); //S
						$table->unsignedInteger('metatitle_id');
						$table->unsignedInteger('metadescription_id');
						$table->unsignedInteger('metakeywords_id');
						//$table->primary('id');

						//Foreign key
						$table->foreign('title_id')->references('id')->on('strings');
						$table->foreign('description_id')->references('id')->on('strings');
						$table->foreign('shortdescription_id')->references('id')->on('strings');
						$table->foreign('url_id')->references('id')->on('strings');
						$table->foreign('metatitle_id')->references('id')->on('strings');
						$table->foreign('metadescription_id')->references('id')->on('strings');
						$table->foreign('metakeywords_id')->references('id')->on('strings');

					});

				}


				/* ------------------------------------------------------------------ */

				//Email templates
				if( !Schema::hasTable('emailtemplates') )	{

					Schema::create('emailtemplates', function( $table )
					{

						$table->increments('id');
						$table->softDeletes();
						$table->timestamps();
						$table->boolean('visible')->default(1);
						$table->integer('ordination');
						$table->unsignedInteger('subject_id'); //S
						$table->unsignedInteger('body_id'); //S 
						$table->string('code');
						$table->string('sender');
						

						//Foreign key
						$table->foreign('subject_id')->references('id')->on('strings');
						$table->foreign('body_id')->references('id')->on('strings');
						
					});

					F::savestrings('EmailTemplates', array(

						"subject" => "Ordine confermato", 
						"body" => "<p>Ciao ##NAME##,</p>

									<p>ecco la lista dei prodotti acquistati:</p>

									<p>##ORDER_ITEMS##</p>

									<p>Metodo di pagamento: ##ORDER_PAYMENTMETHOD##</p>

									<p>Per un totale di ##ORDER_TOTAL## Euro</p>

									<p>Le nostre news</p>

									<p>Lo Staff</p>

									<p>LineaEmmeZeta Parfum</p>

									<p>&nbsp;</p>
									",

						"code" => "orderconfirm",

					), array("subject", "body"));


					F::savestrings('EmailTemplates', array(

						"subject" => "Il tuo ordine è stato spedito", 
						"body" => "<p>Ciao ##NAME##,</p>

									<p>il tuo ordine con id ##ORDER_ID## &egrave; stato processato e spedito all&#39;indirizzo che ci hai segnalato.</p>

									<p>Restiamo a tua completa disposizione. Linea emme zeta</p>

									<p>Saluti</p>

									<p>Lo Staff</p>",

						"code" => "ordersent",

					), array("subject", "body"));


				}

				/* ------------------------------------------------------------------ */

				//Slides
				if( !Schema::hasTable('slides') )	{

					Schema::create('slides', function( $table )
					{

						$table->increments('id');
						$table->softDeletes();
						$table->timestamps();
						$table->integer('ordination');
						$table->string('animation')->default("none");
						$table->unsignedInteger('title_id'); 
						$table->unsignedInteger('slidedescription_id');
						$table->unsignedInteger('subtitle_id'); 
						$table->unsignedInteger('link_id'); 

						$table->foreign('title_id')->references('id')->on('strings');
						$table->foreign('slidedescription_id')->references('id')->on('strings');
						$table->foreign('subtitle_id')->references('id')->on('strings');
						$table->foreign('link_id')->references('id')->on('strings');
						
					});

				}


				/* ------------------------------------------------------------------ */



				/*
				   ECOMMERCE TABLES
				*/


				//Metodi di pagamento
				if( !Schema::hasTable('payment_methods') )	{

					Schema::create('payment_methods', function( $table )
					{

						$table->increments('id');
						$table->softDeletes();
						$table->timestamps();
						$table->boolean('visible')->default(1);
						$table->string('code');
						$table->string('plus')->default('0,00');
						$table->unsignedInteger('title_id');//s
						$table->unsignedInteger('description_id');//s

						$table->foreign('title_id')->references('id')->on('strings');
						$table->foreign('description_id')->references('id')->on('strings');
						
					});

					F::savestrings('PaymentMethods', array(

						"title" => "Pagamento con Paypal", 
						"description" => "Stai per essere reindirizzato alla pagina di pagamento",
						"code" => "paypal",
						"plus" => "0,00"

					), array("title", "description"));

					F::savestrings('PaymentMethods', array(

						"title" => "Pagamento con Bonifico Bancario", 
						"description" => "Di seguito visualizzerai le coordinate bancarie:",
						"code" => "bonifico",
						"plus" => "0,00"

					), array("title", "description"));

				}


				/* ------------------------------------------------------------------ */


				//Ordini
				if( !Schema::hasTable('orders') )	{

					Schema::create('orders', function( $table )
					{

						$table->increments('id');
						$table->softDeletes();
						$table->timestamps();
						$table->string('total');
						$table->string('shipping_fees')->default("0,00");
						$table->unsignedInteger('user_id'); //User id
						$table->unsignedInteger('payment_id'); //Payment method id
						$table->timestamp('payed_on')->default(null);
						$table->timestamp('delivered_on')->default(null);;

						$table->string('payment_code');

						$table->string('shipping_address');
						$table->string('shipping_city');
						$table->string('shipping_post_code');
						$table->string('shipping_province');
						$table->string('shipping_country');

						$table->foreign('user_id')->references('id')->on('users');
						$table->foreign('payment_id')->references('id')->on('payment_methods');
						
						

					});

				}


				/* ------------------------------------------------------------------ */


				//Ordini prodotti
				if( !Schema::hasTable('orders_products') )	{

					Schema::create('orders_products', function( $table )
					{

						$table->increments('id');
						$table->softDeletes();
						$table->timestamps();
						$table->integer('quantity');
						$table->unsignedInteger('product_id'); //Product id 
						$table->unsignedInteger('order_id'); //Order id 

						$table->foreign('product_id')->references('id')->on('products');
						$table->foreign('order_id')->references('id')->on('orders');
						
					});

				}


				/* ------------------------------------------------------------------ */


				//Spese di spedizione
				if( !Schema::hasTable('shipping_fees') )	{

					Schema::create('shipping_fees', function( $table )
					{

						$table->increments('id');
						$table->softDeletes();
						$table->timestamps();
						$table->boolean('visible')->default(1);
						$table->string('fromz');
						$table->string('toz');
						$table->string('price');
						
					});

					$ship = new ShippingFees();

					$ship->fromz = '0';
					$ship->toz = '9999';
					$ship->price = '0,00';

					$ship->save();

				}	

				////

			}

		static function import($type) {

			//Settaggio database da importare

			$host = "localhost";
			$db_user = "pettroot";
			$db_password = " ";
			$db_name = "lineaemmezeta";

			////

			//Creo connessione aggiuntiva
			$conn = array(
			    'driver'    => 'mysql',
			    'host'      => $host,
			    'database'  => $db_name,
			    'username'  => $db_user,
			    'password'  => $db_password,
			    'charset'   => 'utf8',
			    'collation' => 'utf8_general_ci',
			    'prefix'    => '',
			);

			Config::set('database.connections.DB_CONFIG_NAME', $conn);

			$DB = DB::connection('DB_CONFIG_NAME');

			if($type == "products")
			{

				//PRODOTTI
				$products = $DB->select('select * from prodotti');

				foreach($products as $k => $product) {

					if($product->id_padre == 5)

						$cat = 2;

					if($product->id_padre == 6)

						$cat = 3;

					if($product->id_padre == 8)

						$cat = 1;

					$productsave = F::savestrings('Products', array(

						"visible" => $product->abilitato, 
						"inhome" => $product->in_home, 
						"sale" => $product->in_home2,
						"code" => $product->codice,
						"ordination" => $product->posizione,
						"description" => $product->descrizione,
						"title" => $product->titolo,
						"metatitle" => "Profumo ".$product->titolo,
						"metadescription" => "Profumo ".$product->titolo.", linea emme zeta parfum a soli 12,00 €! Entra nel sito per maggiori info e/o acquistare ".$product->titolo.".",
						"metakeywords" => $product->titolo.", profumo ".$product->titolo,
						"shortdescription" => $product->titolo,
						"url" => $product->url,
						"price" => $product->prezzo,
						"category_id" => $cat


					), array(

						"description", "title", "url", "metatitle", "metadescription", "metakeywords", "shortdescription"

					));


					//Media
					$medias = $DB->select('select * from prodotti_img where id_gallery ='.$product->id);

					F::savestrings('Medias', array(

						"parent_id" => $productsave['id'],
						"parent_table" => 'Products',
						"name" => $medias[0]->immagine,
						"copertine" => $medias[0]->is_default,
						"visible" => $medias[0]->abilitato,
						"title" => $product->titolo

					), array( "title"));

				}

			}

			if($type == "users")
			{

				//UTENTI
				$users = $DB->select('select * from utenti');

				foreach($users as $k => $user) {

					$new = new User();

					$new->name = $user->nome." ".$user->cognome;
					$new->email = $user->email;
					$new->password = $user->password;
					$new->role = 'user';
					$new->telephone = $user->telefono;
					$new->address = $user->indirizzo.", ".$user->civico;
					$new->post_code = $user->cap;
					$new->city = $user->citta;
					$new->province = $user->provincia;
					$new->country = $user->stato;

					$new->save();

				}

			}

			if($type == "orders")
			{

				$orders = $DB->select('select * from ordini');

				foreach($orders as $k => $ord) {

					$order = new Orders();
					$user = User::where('email', $ord->email)->first();

					if($ord->metodo == 2)

						$metodo = 1;

					else

						$metodo = 2;

					$order->user_id = $user['id'];
					$order->payment_id = $metodo;
					$order->total = F::money('tostring', F::money('tonumber', $ord->totale));

					if($ord->pagato == 1)

						$order->payed_on = DB::raw('CURRENT_TIMESTAMP');

					if($ord->spedito == 1)

						$order->delivered_on = DB::raw('CURRENT_TIMESTAMP');

					$order->save();

					$orders_products = $DB->select('select * from ordini_prodotti where id_ordine ='.$ord->id);

					foreach($orders_products as $k => $orders_product) {

						$product = $DB->select('select * from prodotti where id ='.$orders_product->id_prodotto);

						//F::printer($product);
						foreach($product as $p)

							$obb = $p;

						$productz = F::byUrl('Products', $obb->url);

						$obj = new OrdersProducts();

						$obj->quantity = $orders_product->quantita;
						$obj->order_id = $order['id'];
						$obj->product_id = $productz['id'];

						$obj->save();

					}


				}

			}


		}

}