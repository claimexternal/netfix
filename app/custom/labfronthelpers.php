<?php


class LabFrontHelpers {

	static public $cicleHelpers = Array(

		/*

		@cicle($model, $where, $strings, $order, $limit, $skip)
		@endcicle
		
		
		* Ciclo oggetti data condizione statica + inclusione stringhe, ordinamento, limit e skip
		*
		* @param   string   $model      //Schema db dove effettuare le operazioni
		* @param   string   $where      //Condizione where
		* @param   array    $strings    //Strings da includere
		* @param   array    $order      //Ordinamento
		* @param   string   $limit      //Limite oggetti
		* @param   string   $skip       //Oggetti da escludere
		* 
		

		*/
		'cicle' => '$1<?php

		    	$where = "online=1";
		    	$order = array("created_at", "desc");
		    	$strings = array("title");
		    	$limit = "18446744073709551";
		    	$skip = "";

		    	$whats = explode(",", $2);

		    	$schema = $whats[0];

		    	//WHERE
		    	if(isset($whats[1]))

		    		$where = "online=1 and ".$whats[1];

		    	//STRINGS
		    	if(isset($whats[2])) {

		    		$strings = explode("-", trim($whats[2]));
		    		
		    		$strings = array_filter(array_map("trim",$strings));

		    	}

		    	//ORDER
		    	if(isset($whats[3]))
		    	
		    		$order = explode("-", $whats[3]);

		    	
		    	//LIMIT
		    	if(isset($whats[4]))

		    		$limit = $whats[4];

		    	//SKIP
		    	if(isset($whats[5]))

		    		$skip = $whats[5];

		    	if($schema == "Newss") 

		    		$where = "publish_at <=curdate() and ".$where;

		    	
		    	$cicle = F::byWhere($schema, $where, $strings, $order, $limit, $skip);

		    	

		    	foreach($cicle as $k => $row) { 

			?>',
		////


		/*

		@menu
		@endmenu
		
		
		* Ciclo menu
		*
		* @param   string   $model      //Schema db dove effettuare le operazioni
		* @param   string   $where      //Condizione where
		* @param   array    $strings    //Strings da includere
		* @param   array    $order      //Ordinamento
		* @param   string   $limit      //Limite oggetti
		* @param   string   $skip       //Oggetti da escludere
		* 
		

		*/
		'menu' => '$1<?php

		    	$cicle = F::byWhere("Pages", "inmenu = 1 and online = 1 and parent_id = 0", array("title", "url"), array("ordination", "asc"));

		    	foreach($cicle as $k => $row) { 
			 
			 ?>',

		////

		/*

		@images
		@endimages
		
		
		* Ciclo immagini pagina
		*
		* @param   int   $width    //Larghezza immagini
		* @param   int   $height   //Altezza immagini
		* @param   int   $limit    //Limite loop immagini
		* 
		
		*/

		'images' => '$1<?php

		    	$whats = explode(",", $2);

		    	$width = 100;
		    	$height = 200;
		    	$limit = "18446744073709551";
		    	$copertine = "1";

		    	if(isset($whats[0])) 

		    		$width = $whats[0];


		    	if(isset($whats[1])) 

		    		$height = $whats[1];


		    	if(isset($whats[2])) 

		    		$limit = $whats[2];

		    	if(isset($whats[3])) 

		    		$copertine = $whats[3];


    			$cicle = I::getImages(get_class($page), $page["id"], $width, $height, $limit, $copertine);


		    	foreach($cicle as $k => $row) { 
				
			?>',

		////

		/*

		@documents
		@enddocuments
		
		
		* Ciclo documenti pagina
		*
		* @param   int   $limit      //Limit loop documenti
		* 

		*/

		'documents' => ' $1<?php $limit = $2; $cicle = D::getDocuments(get_class($page), $page["id"], $limit); foreach($cicle as $k => $row) {  ?>',
		/*

		@privatedocuments
		@endprivatedocuments
		
		
		* Ciclo documenti privati pagina
		*
		* @param   int   $limit      //Limit loop documenti
		* 

		*/

		'privatedocuments' => ' $1<?php $limit = $2; $cicle = D::getDocuments(get_class($page), $page["id"], $limit, "private"); foreach($cicle as $k => $row) {  ?>',		

		/*

		@relatedproducts
		@endrelatedproducts
		
		
		* Ciclo prodotti correlati al prodotto corrente
		*
		* @param   int   $limit      //Limit loop documenti
		* 

		*/

		'relatedproducts' => ' $1<?php $cicle = RelatedProducts::with("related_product.title", "related_product.shortdescription")->whereRaw("product_id =".$page["id"])->get(); foreach($cicle as $k => $row) { $row = $row["related_product"]  ?>',

		/*

		@plusproducts
		@endplusproducts
		
		
		* Ciclo plus prodotti correlati al prodotto corrente
		*
		* @param   int   $limit      //Limit loop documenti
		* 

		*/

		'plusproducts' => ' $1<?php $cicle = F::byWhere("RelatedProducts", "product_id =".$page["id"], array("related_product.title", "related_product.shortdescription")); foreach($cicle as $k => $row) { $row = $row["related_product"]  ?>',

		/*

		@news
		@endnews
		
		
		* Ciclo le news in base al tipo
		*
		* @param   string   $type      //Tipo di news
		* @param   int   	$limit     //Limit news
		* 

		*/
			
		'news' => '$1<?php 

			$whats = explode(",", $2);

			$type = $whats[0];
			$limit = 999;

			if(isset($whats[1]))
	
				$limit = $whats[1];

			$where = "publish_at <=curdate() and online = 1 and type = \'".$type."\'";
		    	
	    	$cicle = F::byWhere("Newss", $where, array("title", "shortdescription"), array("publish_at", "desc"), $limit);

	    	foreach($cicle as $k => $row) { 

		?>',

		/*

		@breadcrumb
		@endbreadcrumb
		
		
		* Costruisco la breadcrumb della pagina corrente
		*
		* @param   bool   	$home     //Link home in prima posizione
		* 

		*/
			
		'breadcrumb' => '$1<?php 
	
	    	$cicle = F::getBreadCrumb($page);

	    	foreach($cicle as $k => $row) { 

		?>'
	);
	
	static public $dinamicHelpers = Array(

		//Disegna form dato il code
		'drawForm' => ' $1<?php echo FrontEndForm::drawForm($2, $page); ?>',

		//Ritorna src immagine di default page
		'pageImage' => '$1<?php $whats = explode(",", $2); echo I::getCopertine(get_class($page), $page["id"], $whats[0], $whats[1]); ?>',

		//Ritorna src immagine di default row
		'rowImage' => '$1<?php $whats = explode(",", $2); echo I::getCopertine($row["shematype"], $row["id"], $whats[0], $whats[1]); ?>',

		//Scrive campo text facilitato
		'pageText' => '$1<?php text($2, Array(), 1); ?>',

		//Da mettere per gli asset
		'asset' => '$1<?php echo url($2); ?>',

		//Url pagina in lingua passata
		'urlLang' => '$1<?php echo F::getUrl(get_class($page), $page["id"], $2);    ?>',

		//Helper per gli assets
		'pageAssets' => '$1<?php

			$whats = explode(",", $2);

			$string = ""; foreach(Front::$assets[$whats[0]][$whats[1]] as $asset) $string .= $asset.","; $string = substr($string, 0, -1);

			if($whats[1] == "css")

        		echo "<link href=\'".url("min/?b=css&f=".$string)."\' rel=\'stylesheet\' type=\'text/css\'>";

        	else

        		echo "<script id=\'scriptFile\' lang=\'".App::getLocale()."\' src=\'".url("min/?b=js&f=".$string)."\'></script>";

		?>'

	);

	static public $staticHelpers = Array(

		'pageTitle' => 'echo $page["title"]',
		'pageMetaTitle' => 'echo $page["metatitle"]',
		'pageMetaDescription' => 'echo $page["metadescription"]',
		'pageMetaKeywords' => 'echo $page["metakeywords"]',
		'pageRobots' => 'echo $page["robots"]',
		'pageShortDescription' => 'echo $page["shortdescription"]',
		'pageDescription' => 'echo $page["description"]',
		'pageDocument' => '$document = D::getDefault(get_class($page), $page["id"]); echo $document["src"]',
		'pageDocumentTitle' => 'echo $document["title"]',

		'rowId' => 'echo $row["id"]',
		'rowTitle' => 'echo $row["title"]',
		'rowSubTitle' => 'echo $row["subtitle"]',
		'rowSlideDescription' => 'echo $row["slidedescription"]',
		'rowShortDescription' => 'echo $row["shortdescription"]',
		'rowDescription' => 'echo $row["description"]',
		'rowLink' => 'echo $row["link"]',
		'rowSrc' => 'echo $row["src"]',
		'rowUrl' => 'echo F::getUrl($row["shematype"], $row["id"])',
		'rowDocument' => '$document = D::getDefault($row["shematype"], $row["id"]); echo $document["src"]',
		'rowDocumentTitle' => 'echo $document["title"]',

		'urlHome' => 'echo F::getUrl("Pages", 1)',

		'user' => 'if(isset($_SESSION["user"])) {',
		'notuser' => '} else {',
		'enduser' => '}',

	);

	static function initMatchers() {

		foreach(self::$cicleHelpers as $replace => $code) {

			Blade::extend(function($view, $compiler) use ($replace, $code) {

				$pattern = $compiler->createMatcher($replace);
				return preg_replace($pattern, $code, $view);

			});

			Blade::extend(function($view) use ($replace) {	

				return preg_replace('/(\s*)@(end'.$replace.')(\s*)/', '$1<?php } ?>$3', $view);

			});

		}

		foreach(self::$dinamicHelpers as $replace => $code) {

			Blade::extend(function($view, $compiler) use ($replace, $code) {

				$pattern = $compiler->createMatcher($replace);
				return preg_replace($pattern, $code, $view);

			});

		}

		foreach(self::$staticHelpers as $replace => $code) {

			Blade::extend(function($view) use ($replace, $code) {	

				return preg_replace('/(\s*)@('.$replace.')(\s*)/', '$1<?php '.$code.'; ?>$3', $view);

			});

		}

	}


}