<?php
	
 use Illuminate\Database\Eloquent\SoftDeletingTrait;

 class Newss extends Eloquent {

 	use SoftDeletingTrait;

 	protected $table = 'newss';
 	protected $dates = ['deleted_at'];

 	public function url()
 		
 		{
 			
 			return $this->belongsTo('Strings');

 		}

 	public function title()
 		
 		{

 			return $this->belongsTo('Strings');

 		}

  	public function description()
 		
 		{

 			return $this->belongsTo('Strings');

 		}  

 	public function shortdescription()
 		
 		{

 			return $this->belongsTo('Strings');

 		}

  	public function metatitle()
 		
 		{

 			return $this->belongsTo('Strings');

 		}

 	public function metadescription()
	
		{

			return $this->belongsTo('Strings');

		}

 	public function metakeywords()
 		
		{

			return $this->belongsTo('Strings');

		}

 	public function annoinstallazione()
 		
		{

			return $this->belongsTo('Strings');

		}

 	public function luogoinstallazione()
 		
		{

			return $this->belongsTo('Strings');

		}

 	public function potenzaimpianto()
 		
		{

			return $this->belongsTo('Strings');

		}

 	public function tipodiimpianto()
 		
		{

			return $this->belongsTo('Strings');

		}


 	public function getMedias($limit = "18446744073709551615", $width, $height) {

 		if($limit == 1)

			return I::getCopertine(get_class($this), $this->id, $width, $height);

		else

			return I::getImages(get_class($this), $this->id, $width, $height, $limit);

	} 

 	

}
