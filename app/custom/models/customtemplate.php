<?php
	
 use Illuminate\Database\Eloquent\SoftDeletingTrait;

 class CustomTemplate extends Eloquent {

 	use SoftDeletingTrait;

 	protected $table = 'custom_template';
 	protected $dates = ['deleted_at']; 	


 }