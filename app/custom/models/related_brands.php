<?php

 class RelatedBrands extends Eloquent {

 	protected $table = 'related_brands';
 
 	public function product() {

		return $this->belongsTo('Products');

	}

  	public function brand() {

		return $this->belongsTo('Brands');

	}
  
}