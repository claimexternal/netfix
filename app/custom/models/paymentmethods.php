<?php

 use Illuminate\Database\Eloquent\SoftDeletingTrait;
 class PaymentMethods extends Eloquent {

 	use SoftDeletingTrait;
 	
 	protected $table = 'payment_methods';
 	protected $dates = ['deleted_at'];

 	public function title()
 		
 		{
 			
 			return $this->belongsTo('Strings');

 		}

 	public function description()
 		
 		{
 			
 			return $this->belongsTo('Strings');

 		}
 }