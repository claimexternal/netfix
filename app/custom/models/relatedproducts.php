<?php

 class RelatedProducts extends Eloquent {

 	protected $table = 'related_products';

 	public static $dbTable = 'related_products';

 	public static $fields = Array(

 		"id" => Array("increments"),
 		"product_id" => Array("foreign" => "products", "default" => 1),
 		"related_product_id" => Array("foreign" => "products", "default" => 1),

 	);

 	public function product()
 		
 		{

 			return $this->belongsTo('Products');

 		}

  	public function related_product()
 		
 		{

 			return $this->belongsTo('Products');

 		}  


}