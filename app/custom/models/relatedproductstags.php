<?php

 class RelatedProductsTags extends Eloquent {
 	
 	protected $table = 'related_productstags';
 	
 	public function tag() {

 		return $this->belongsTo('ProductsTags');

 	}

  	public function product() {

 		return $this->belongsTo('Products');

 	}

}