<?php

 class RelatedPlusProducts extends Eloquent {
 	
 	protected $table = 'related_plusproducts';
 	
 	public function plus() {

 		return $this->belongsTo('PlusProducts');

 	}

  	public function product() {

 		return $this->belongsTo('Products');

 	}  


}