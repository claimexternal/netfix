<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;
use Illuminate\Database\Eloquent\SoftDeletingTrait;

 class User extends Eloquent implements UserInterface, RemindableInterface {

 	use SoftDeletingTrait;

 	protected $table = 'users';
 	protected $hidden = array('password');
  	protected $dates = ['deleted_at'];

  	static function get($field) {

  		if(isset($_SESSION['user'][$field]))

			return $_SESSION['user'][$field];

		else

			return '';

  	}

 	public function getAuthIdentifier() {
		
		return $this->getKey(); 

	}

	public function getAuthPassword() {
		
		return $this->password; 

	}

	public function getReminderEmail() {
		
		return $this->email;

	 }

	 public function getRememberToken()
	{
	    return $this->remember_token;
	}

	public function setRememberToken($value)
	{

	    $this->remember_token = $value;

	}

	public function getRememberTokenName()
	{

	    return 'remember_token';
	    
	}


}