<?php

 use Illuminate\Database\Eloquent\SoftDeletingTrait;
 
 class Medias extends Eloquent {

 	use SoftDeletingTrait;

 	protected $table = 'medias';
 	protected $dates = ['deleted_at'];

 	public function title() {

		return $this->belongsTo('Strings');

	}

}