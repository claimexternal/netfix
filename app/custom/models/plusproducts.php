<?php

 use Illuminate\Database\Eloquent\SoftDeletingTrait;

 class PlusProducts extends Eloquent {

 	use SoftDeletingTrait;
 	
 	protected $table = 'plus_products';
 	protected $dates = ['deleted_at'];
 	
 	public function title() {

 		return $this->belongsTo('Strings');

 	}

 	public function description() {

 		return $this->belongsTo('Strings');

 	}


}