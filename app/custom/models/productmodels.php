<?php

 use Illuminate\Database\Eloquent\SoftDeletingTrait;
 
 class ProductsModels extends Eloquent {

 	use SoftDeletingTrait;
 	
 	protected $table = 'products_models';
 	protected $dates = ['deleted_at'];

 	public function title() {

		return $this->belongsTo('Strings');

	}

}