<?php

 use Illuminate\Database\Eloquent\SoftDeletingTrait;

 class Accessories extends Eloquent {

 	use SoftDeletingTrait;
 	
 	protected $table = 'accessories';
 	protected $dates = ['deleted_at'];

 	public static $dbTable = 'accessories';

 	public static $fields = Array(

 		"id" => Array("increments"),
 		"code" => Array("string"),
 		"title_id" => Array("foreign" => "strings", "default" => 1),
 		"description_id" => Array("foreign" => "strings", "default" => 1),

 	);

 	public function title()
 		
 		{

 			return $this->belongsTo('Strings');

 		}

  	public function description()
 		
 		{

 			return $this->belongsTo('Strings');

 		}  

 	public function getMedias($limit = "18446744073709551615", $width, $height) {

 		if($limit == 1)

			return I::getCopertine(get_class($this), $this->id, $width, $height);

		else

			return I::getImages(get_class($this), $this->id, $width, $height, $limit);

	} 

}