<?php
	
 use Illuminate\Database\Eloquent\SoftDeletingTrait;

 class Notifications extends Eloquent {

 	use SoftDeletingTrait;

 	protected $table = 'notifications';
 	protected $dates = ['deleted_at'];


 	public function title()
 		
 		{

 			return $this->belongsTo('Strings');

 		}

  	public function description()
 		
 		{

 			return $this->belongsTo('Strings');

 		}

 		

 		
}
