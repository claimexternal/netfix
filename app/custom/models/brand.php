<?php

 use Illuminate\Database\Eloquent\SoftDeletingTrait;

 class Brands extends Eloquent {

 	use SoftDeletingTrait;
 	
 	protected $table = 'brands';
 	protected $dates = ['deleted_at'];

 	public function title() {

		return $this->belongsTo('Strings');

	}

  	public function description() {

		return $this->belongsTo('Strings');

	}
  
}