<?php
  use Illuminate\Database\Eloquent\SoftDeletingTrait;
 class Promotions extends Eloquent {
 	
 	use SoftDeletingTrait;

 	protected $table = 'promotions';
 	protected $dates = ['deleted_at'];

 	public function title() {

		return $this->belongsTo('Strings');

	}

  	public function description() {

		return $this->belongsTo('Strings');

	} 

 	public function codes() {

        return $this->hasMany('PromotionsCodes');
    }

}