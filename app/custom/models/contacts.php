<?php

 use Illuminate\Database\Eloquent\SoftDeletingTrait;

 class Contacts extends Eloquent {

 	use SoftDeletingTrait;
 	
 	protected $table = 'contacts';
 	protected $dates = ['deleted_at'];

 	public function formBuilder() {

 		return $this->belongsTo('FormBuilder');

 	}

 	public function user() {

 		$fields = $this->fields();

 		if($this->user_id != 0)

 			return User::find($this->user_id);

 		elseif(isset($fields['name']))

 			return $fields['name'];

 		elseif(isset($fields['email']))

 			return $fields['email'];



 	}

 	public function fields() {

 		return unserialize($this->fields);

 	}

}

