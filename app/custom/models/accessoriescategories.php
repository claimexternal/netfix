<?php

 use Illuminate\Database\Eloquent\SoftDeletingTrait;

 class CategoriesAccessories extends Eloquent {

 	use SoftDeletingTrait;
 	
 	protected $table = 'categories_accessories';
 	protected $dates = ['deleted_at'];

 	public static $dbTable = 'categories_accessories';

 	public static $fields = Array(

 		"id" => Array("increments"),
 		"online" => Array("boolean", "default" => 1),
 		"code" => Array("string"),
 		"ordination" => Array("integer"),
 		"parent_id" => Array("integer"),
 		"title_id" => Array("foreign" => "strings", "default" => 1),
 		"description_id" => Array("foreign" => "strings", "default" => 1),
 		"author_id" => Array("foreign" => "users", "default" => 1),

 	);

 	public function title() {

		return $this->belongsTo('Strings');

 	}

  	public function description() {

 		return $this->belongsTo('Strings');

 	}
 	

}