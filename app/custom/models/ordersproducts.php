<?php

 use Illuminate\Database\Eloquent\SoftDeletingTrait;
 class OrdersProducts extends Eloquent {

 	protected $table = 'orders_products';
 	protected $dates = ['deleted_at'];

 	public function product()
 		
 		{
 			
 			return $this->belongsTo('Products');

 		}

 }