<?php

 use Illuminate\Database\Eloquent\SoftDeletingTrait;

 class RelatedAccessories extends Eloquent {

 	use SoftDeletingTrait;
 	
 	protected $table = 'related_accessories';
 	protected $dates = ['deleted_at'];
 	public static $dbTable = 'related_accessories';

 	public static $fields = Array(

 		"id" => Array("increments"),
 		"product_id" => Array("foreign" => "products", "default" => 1),
 		"accessory_id" => Array("foreign" => "accessories", "default" => 1),

 	);
 	
 	public function accessory() {

 		return $this->belongsTo('Accessories');

 	}

  	public function product() {

 		return $this->belongsTo('Products');

 	}  


}