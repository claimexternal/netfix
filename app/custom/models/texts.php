<?php
	
 use Illuminate\Database\Eloquent\SoftDeletingTrait;

 class Texts extends Eloquent {

 	use SoftDeletingTrait;

 	protected $table = 'texts';
 	protected $dates = ['deleted_at'];

 	public function get() {

 		App::setLocale(Front::checklang(Request::segment(1)));
 		
 		$text = Texts::first();

 		return unserialize(urldecode($text[App::getLocale()]));

 	}

}
