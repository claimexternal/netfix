<?php

 use Illuminate\Database\Eloquent\SoftDeletingTrait;

 class ProductsTags extends Eloquent {

 	use SoftDeletingTrait;
 	
 	protected $table = 'products_tags';
 	protected $dates = ['deleted_at'];

 	public function title() {

		return $this->belongsTo('Strings');

	}

  	public function url() {

		return $this->belongsTo('Strings');

	} 

	static function getByProducts($products) {

		$productIds = array_column($products, 'id'); 

  		return RelatedProductsTags::select("products_tags.*", App::getLocale()." as title")
  				->join("products_tags", "products_tags.id", "=", "related_productstags.tag_id")
  				->join("strings", "products_tags.title_id", "=", "strings.id")
  				->whereIn('product_id', $productIds)
  				->orderBy('title')
  				->groupBy('tag_id')
  				->get();

	}

	static function getByCategoryId($id) {

		$products = Products::where('category_id', $id)->where('online', 1)->get()->toArray();

		$productIds = array_column($products, 'id'); 

  		return RelatedProductsTags::with('tag.title')
  				->whereIn('product_id', $productIds)
  				->groupBy('tag_id')
  				->get();

	}

}