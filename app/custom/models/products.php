<?php

 use Illuminate\Database\Eloquent\SoftDeletingTrait;
 class Products extends Eloquent {

 	use SoftDeletingTrait;
 	
 	protected $table = 'products';
 	protected $dates = ['deleted_at'];

 	public function getAccessories($strings = array("title")) {

 		$lang = App::getLocale();

 		foreach($strings as $string)

 			$include[] = "accessory.".$string;


 		$objects = RelatedAccessories::with($include)->whereRaw("product_id = ".$this->id)->get();
		
		$i = 0;

		$accessories = array();

		foreach($objects as $key => $object) {

			$accessories[$i] = $object['relations']['accessory'];

			foreach($strings as $value) {

				if(isset($object['relations']['accessory']['relations'][$value]))
					
					$accessories[$i][$value] = $object['relations']['accessory']['relations'][$value][$lang];

			}

			$i++;

		}

		return $accessories;

 	}
 
  	public function tomaia() {
 			
 		return $this->belongsTo('Strings');

 	}
 
  	public function soletta() {
 			
 		return $this->belongsTo('Strings');

 	}
 
  	public function lamina() {
 			
 		return $this->belongsTo('Strings');

 	}
 
  	public function versions() {
 			
 		return $this->belongsTo('Strings');

 	}

 	public function plus() {
 			
 		return $this->belongsTo('Strings');

 	}
 	
 	public function url()
 		
 		{
 			
 			return $this->belongsTo('Strings');

 		}

 	public function category()
	
	{
		
		return $this->belongsTo('Categories');

	}

 	public function title()
 		
 		{

 			return $this->belongsTo('Strings');

 		}

  	public function description()
 		
 		{

 			return $this->belongsTo('Strings');

 		}  

 	public function shortdescription()
 		
 		{

 			return $this->belongsTo('Strings');

 		}

  	public function metatitle()
 		
 		{

 			return $this->belongsTo('Strings');

 		}

 	public function metadescription()
	
		{

			return $this->belongsTo('Strings');

		}

 	public function metakeywords()
 		
		{

			return $this->belongsTo('Strings');

		}

 	public function getMedias($limit = "18446744073709551615", $width, $height) {

 		if($limit == 1)

			return I::getCopertine(get_class($this), $this->id, $width, $height);

		else

			return I::getImages(get_class($this), $this->id, $width, $height, $limit);

	} 

}