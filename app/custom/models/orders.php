<?php

 use Illuminate\Database\Eloquent\SoftDeletingTrait;
 class Orders extends Eloquent {

 	protected $table = 'orders';
 	protected $dates = ['deleted_at'];

 	public function user()
 		
 		{
 			
 			return $this->belongsTo('User');

 		}

 	public function payment()
 		
 		{
 			
 			return $this->belongsTo('PaymentMethods');

 		}
 }