<?php

 use Illuminate\Database\Eloquent\SoftDeletingTrait;

 class MapPins extends Eloquent {

 	use SoftDeletingTrait;
 	
 	protected $table = 'map_pins';
 	protected $dates = ['deleted_at'];

  	public function title() {

  		return $this->belongsTo('Strings');

  	}
  	
  	public function description() {

  		return $this->belongsTo('Strings');

  	}

}

