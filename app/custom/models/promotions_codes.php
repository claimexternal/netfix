<?php

 class PromotionsCodes extends Eloquent {
 	
 	protected $table = 'promotions_codes';

  	public function promotions() {

		return $this->belongsTo('Promotions');

	}

}