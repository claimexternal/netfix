<?php
	
 use Illuminate\Database\Eloquent\SoftDeletingTrait;

 class Slides extends Eloquent {

 	use SoftDeletingTrait;

 	protected $table = 'slides';
 	protected $dates = ['deleted_at'];


 	public function title()
 		
 		{

 			return $this->belongsTo('Strings');

 		}

  	public function slidedescription()
 		
 		{

 			return $this->belongsTo('Strings');

 		}

  	public function subtitle()
 		
 		{

 			return $this->belongsTo('Strings');

 		}  

  	public function link()
 		
 		{

 			return $this->belongsTo('Strings');

 		}  

 	public function getMedias($limit = "18446744073709551615", $width, $height) {

 		if($limit == 1)

			return I::getCopertine(get_class($this), $this->id, $width, $height);

		else

			return I::getImages(get_class($this), $this->id, $width, $height, $limit);

	} 

 	

}
