<?php

 use Illuminate\Database\Eloquent\SoftDeletingTrait;

 class FormBuilder extends Eloquent {

 	use SoftDeletingTrait;
 	
 	protected $table = 'form_builder';
 	protected $dates = ['deleted_at'];
 	
 	public function title()
 		
 		{
 			
 			return $this->belongsTo('Strings');

 		} 

    public function fields()
    {
        return $this->hasMany('FormBuilderFields');
    }

    public function deleteFields() {

    	$this->fields()->delete();

    }

 }


  class FormBuilderFields extends Eloquent {

 	use SoftDeletingTrait;
 	
 	protected $table = 'form_builder_fields';
 	protected $dates = ['deleted_at'];

 	public function error()
 		
 		{
 			
 			return $this->belongsTo('Strings');

 		}

 	public function title()
 		
 		{
 			
 			return $this->belongsTo('Strings');

 		}

 }