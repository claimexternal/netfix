<?php

include(public_path().'/assets.php');

class Front {

	public static $url = null;

	public static $assets = array();

	public static $texts = array();
	//Definisco i linguaggi del sito
	//Prima index -> lingua default
	public static $languages = 	array(

		'it' => 'Italiano',
		'en' => 'English',
		'es' => 'Spagnolo',
		'de' => 'Tedesco',
		'fr' => 'Francese',
		'hu' => 'Ungherese'
	
	);

	static function getHome() {

		$tp['lang'] = App::getLocale();

		$tp['page'] = F::byUrl('Pages', '', array('metatitle', 'metadescription', 'metakeywords', 'title', 'description'));

		$tp['type'] = 'pages';

		if($tp['page']['metatitle'] == "")

			$tp['page']['metatitle'] = $tp['page']['title'];

		$tp['page']['description'] = FrontendForm::replaceDescription('##_', '_##', $tp['page']);

		//Loggo visit
		V::insert($tp);

		echo View::make('homepage', $tp)->render();

	}

	//Trovo che tipo di contenuto è in base all'url in lingua
	//Contenuti routtabili in config::routertables
	static function findContent() {

		foreach(LabConfig::get('routertables') as $k => $value) {

			//Pesco dati pagina
			$tp['page'] = F::byUrl(ucfirst($k), end(self::$url), array('metatitle', 'metadescription', 'metakeywords', 'title', 'description', 'shortdescription'));
			
			//Tipo pagina
			$tp['type'] = (is_object($tp['page'])) ? get_class($tp['page']) : ucfirst($k);

			if( self::checkParent( $tp ) ) {

				$tp['page']['description'] = FrontendForm::replaceDescription('##_', '_##', $tp['page']);
				$content = explode(',', $value);

				if($tp['page']['metatitle'] == "")

					$tp['page']['metatitle'] = $tp['page']['title'];
		
				//Loggo visit
				V::insert($tp);

				if(isset($tp['page']['custom_template_id']))

					$content[1] = TemplateBuilder::getTemplate($tp['page'], $content[1]);

				//Configuro assets per la pagina
				//Disegno scheletro e contenuto 
				echo View::make( $content[0], $tp )->nest('content', $content[1], $tp)->render();

				die();

			}

		}

		header("HTTP/1.0 404 Not Found");
		echo View::make( 'skeleton', $tp )->nest('content', '404', $tp)->render();
		
	}


	static function checkParent( $tp ) {
		
		if($tp['page']) {

			if( F::getUrl( ucfirst($tp['type']), $tp['page']['id']) == Request::url())

				return true;
			
			else

				return false;
		}

		else return false;

	}

	static function changeLang( $page, $lang) {

		return F::geturl(ucfirst($page['table']), $page['id'], $lang);

	}

	static function islang() {

		$languages = LabConfig::get( 'languages' );

		if( isset( $languages[$url[0]] ) )

			return true;

		else

			return false;

	}

	static function checklang( $url ) {

		$languages = self::$languages;

		$default = array_keys($languages);

		//Cerco nell'array lingue se quella nell'url esiste
		if( isset( $languages[$url] ) )

			return $url;

		//Altrimenti restituisco quella di default
		else return $default[0];

	}

}