<?php

 /**
   * D placeholder per Documents
   * Funzioni relative alla manipolazione e estrapolazione documenti
   *
   * 
   * 19/10/2015
   * 
   * @package    bCMS
   * @subpackage Custom Classes
   * @author     Enrico Pettinao <enricopettinao@gmail.com>
   */

class D {

	static function getDocuments($schema, $id, $limit = "18446744073709551615", $permission = "public") {

		$lang = App::getLocale();

		$codist = LabConfig::$labconfigs['codist'];

		$env = (strpos(Request::server('HTTP_HOST'), LabConfig::$labconfigs['devdomain'])) ? 'prod' : F::getEnv();

		$rootdomain = LabConfig::$labconfigs['rootdomain'][$env];

		if($limit != 1)

			$objects = Medias::with('title')->whereRaw('parent_table ="'.$schema.'" and permission = "'.$permission.'" and type = "document" and parent_id ='.$id)->orderBy('ordination')->take($limit)->get();
		
		else

			$objects = Medias::with('title')->whereRaw('parent_table ="'.$schema.'" and permission = "'.$permission.'" and type = "document" and copertine=1 and parent_id ='.$id)->orderBy('ordination')->take($limit)->get();
				
		$tmparray = array();

		foreach($objects as $key => $object) {

			$tmparray[$key]['id'] = $object['id'];
			$tmparray[$key]['created_at'] = $object['created_at'];
			$tmparray[$key]['name'] = $object['name'];
			$tmparray[$key]['online'] = $object['online'];
			$tmparray[$key]['copertine'] = $object['copertine'];
			$tmparray[$key]['parent_table'] = $object['parent_table'];
			$tmparray[$key]['parent_id'] = $object['parent_id'];
			$tmparray[$key]['src'] = ($object['permission'] == "private") ? F::getUrl('Users', 'getdocument')."/".$object['id'] : $rootdomain.'/documents/'.$object['name'].'/'.$codist.'/';
			$tmparray[$key]['permission'] = $object['permission'];

			if(isset($object['relations']['title'])) {
					
				$tmparray[$key]['title'] = $object['relations']['title'][$lang];

				if($tmparray[$key]['title'] == "")

					unset($tmparray[$key]);

			}



		}

		return $tmparray;

	}

	static function getAllDocuments($schema, $id, $limit = "18446744073709551615") {

		$lang = App::getLocale();

		$codist = LabConfig::$labconfigs['codist'];

		$env = (strpos(Request::server('HTTP_HOST'), LabConfig::$labconfigs['devdomain'])) ? 'prod' : F::getEnv();

		$rootdomain = LabConfig::$labconfigs['rootdomain'][$env];

		if($limit != 1)

			$objects = Medias::with('title')->whereRaw('parent_table ="'.$schema.'" and type = "document" and parent_id ='.$id)->orderBy('ordination')->take($limit)->get();
		
		else

			$objects = Medias::with('title')->whereRaw('parent_table ="'.$schema.'" and type = "document" and copertine=1 and parent_id ='.$id)->orderBy('ordination')->take($limit)->get();
				
		$tmparray = array();

		foreach($objects as $key => $object) {

			$tmparray[$key]['id'] = $object['id'];
			$tmparray[$key]['created_at'] = $object['created_at'];
			$tmparray[$key]['name'] = $object['name'];
			$tmparray[$key]['online'] = $object['online'];
			$tmparray[$key]['copertine'] = $object['copertine'];
			$tmparray[$key]['parent_table'] = $object['parent_table'];
			$tmparray[$key]['parent_id'] = $object['parent_id'];
			$tmparray[$key]['src'] = $rootdomain.'/documents/'.$object['name'].'/'.$codist.'/';
			$tmparray[$key]['permission'] = $object['permission'];

			if(isset($object['relations']['title'])) {
					
				$tmparray[$key]['title'] = $object['relations']['title'][$lang];

				if($tmparray[$key]['title'] == "")

					unset($tmparray[$key]);

			}



		}

		return $tmparray;

	}

	static function getDefault($schema, $id) {

		$lang = App::getLocale();

		$codist = LabConfig::$labconfigs['codist'];

		$object = Medias::with('title')->whereRaw('parent_table ="'.$schema.'" and type = "document" and copertine=1 and parent_id ='.$id)->first();

		$env = (strpos(Request::server('HTTP_HOST'), LabConfig::$labconfigs['devdomain'])) ? 'prod' : F::getEnv();

		$rootdomain = LabConfig::$labconfigs['rootdomain'][$env];

		if($object) {

			$tmparray['src'] = $rootdomain.'/documents/'.$object['name'].'/'.$codist.'/';
			$tmparray['permission'] = $object['permission'];

			if(isset($object['relations']['title']))
					
				$tmparray['title'] = $object['relations']['title'][$lang];

			return $tmparray;

		} else return null;
	}

}