<?php


class TemplateBuilder {

	static function getTemplate($tp, $currentTemplate) {

		$id = $tp['custom_template_id'];

		$viewName = get_class($tp).$tp['id']."CustomTemplate";

		//Se il template è default ritorno il 
		//template che stava per stampare
		if($id == 1)

			return $currentTemplate;

		//Altrimenti proseguo alla creazione
		else {

			$customTemplate = CustomTemplate::find($id);


			if(File::exists(__DIR__.'/../views/customtemplate/'.$viewName.".blade.php"))
				
				return self::checkUpdates($viewName, $customTemplate);

			else

				return self::buildTemplate($viewName, $customTemplate);
			
		}


	}

	static function buildTemplate($viewName, $customTemplate) {

		//var_dump(File::exists(__DIR__.'/../views/customtemplate/')); die();

		if(!File::exists(__DIR__.'/../views/customtemplate/'))
		
			File::makeDirectory(__DIR__.'/../views/customtemplate/', $mode = 0777, true, true);

		File::put(__DIR__.'/../views/customtemplate/'.$viewName.".blade.php", $customTemplate->html);

		return 'customtemplate.'.$viewName;


	}

	static function checkUpdates($viewName, $customTemplate) {

		if((strtotime($customTemplate['updated_at']) > File::lastModified(__DIR__.'/../views/customtemplate/'.$viewName.".blade.php")))

			File::put(__DIR__.'/../views/customtemplate/'.$viewName.".blade.php", $customTemplate->html);

		return 'customtemplate.'.$viewName;

	}


}