<?php

	Front::$assets = Array(

		'header' => array(

			'css' => array( 

				/*'bootstrap/bootstrap.min.css',
				'steelomas.css',			
				'responsive.css',
				'owl-carousel/owl.carousel.css',
				'owl-carousel/owl.theme.css',
				'photoswipe/photoswipe.css',
				'photoswipe/default-skin/default-skin.css'*/
                
 				'bootstrap.min.css',
                'style.css',
                'responsive.css',
                'slick.css'

			),

			'js' => array(
                
                'jquery.min.js',
                'slick.min.js',
				'script.js',
				'jquery.autocomplete.js'

			)

		),

		'footer' => array(
		
			'css' => array(

				//'cookies/cookiechoices.css'

			),

			'js' => array(

				'bootstrap.min.js',
				//'cookies/cookiechoices.js'

			)

		)

	);