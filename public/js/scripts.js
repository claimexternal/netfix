// JS Document

// ===== Scroll to Top ==== 
$(window).scroll(function() {
    if ($(this).scrollTop() >= 50) {        // If page is scrolled more than 50px
        $('#return-to-top').fadeIn(200);    // Fade in the arrow
    } else {
        $('#return-to-top').fadeOut(200);   // Else fade out the arrow
    }
});
$('#return-to-top').click(function() {      // When arrow is clicked
    $('body,html').animate({
        scrollTop : 0                       // Scroll to top of body
    }, 500);
});

// AOS
$(document).ready(function (e) {
    AOS.init();
});

// Masterslider
$(document).ready(function (e) {

    var masterslider = new MasterSlider();

    // slider controls
    masterslider.control('arrows', {
        autohide: true,
        overVideo: true
    });
    masterslider.control('circletimer', {
        autohide: false,
        overVideo: true,
        color: '#FFFFFF',
        radius: 4,
        stroke: 9
    });
    masterslider.control('slideinfo', {
        autohide: false,
        overVideo: true,
        dir: 'h',
        align: 'bottom',
        inset: false,
        margin: 10
    });
    // slider setup
    masterslider.setup("masterslider", {
        width: 490,
        height: 310,
        minHeight: 0,
        space: 10,
        start: 1,
        grabCursor: true,
        swipe: true,
        mouse: true,
        keyboard: false,
        layout: "partialview",
        wheel: false,
        autoplay: false,
        instantStartLayers: false,
        loop: true,
        shuffle: false,
        preload: 3,
        heightLimit: true,
        autoHeight: false,
        smoothHeight: true,
        endPause: false,
        overPause: true,
        fillMode: "fill",
        centerControls: true,
        startOnAppear: false,
        layersMode: "center",
        autofillTarget: "",
        hideLayers: false,
        fullscreenMargin: 0,
        speed: 20,
        dir: "h",
        parallaxMode: 'swipe',
        view: "partialWave"
    });
});
