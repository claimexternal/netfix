//Lingua Front End
var AppLocale = $('#scriptFile').attr('lang');
var rootUrl = $('#scriptFile').attr('root');

$(document).ready(function() {

	var buttonLoader = '<div id="loaderbutton" style="display:none" class="la-ball-beat">'+
	'<div></div>'+
	'<div></div>'+
	'<div></div>'+
	'</div>';

	var buttonLoader = new pLoader("submit-button", buttonLoader);
	//var tableLoader = new pLoader(tableLoader);
	
	buttonLoader.initLoader();
	//Loader.initLoader();

	$(document).bind("ajaxSend", function(){
	
	  buttonLoader.showLoader();
	   //tableLoader.showLoader();
		
	}).bind("ajaxComplete", function(){
		
	   buttonLoader.hideLoader();
	   //tableLoader.hideLoader();
	
	});

});

function showLoader(idButton) {

	var buttonLoaderz = '<div id="loaderbutton_'+idButton+'" style="display:none" class="la-ball-beat">'+
	'<div></div>'+
	'<div></div>'+
	'<div></div>'+
	'</div>';

	$(document).bind("ajaxSend", function(){
	
		$('#loaderbutton_'+idButton).show();

		
	}).bind("ajaxComplete", function(){
		
	   $('#loaderbutton_'+idButton).hide();
	
	});

}

function pLoader(div, html) {

	var thisLoader = this, divId;

	if(typeof div == "undefined")

		divId = "main-content";

	else

		divId = div;

	this.isActive = false;
	this.loaderHtml = html;

	//Posiziono il Loader a seconda delle varie pagine
	this.initLoader = function() {
		
		if($('#'+divId).length) {

			$('#'+divId).append(thisLoader.loaderHtml);

		}

	}

	this.showLoader = function(idButton) {


		if(!thisLoader.isActive) {

			thisLoader.isActive = true;

			if($('#'+divId).length) 

				$('#loaderbutton').show();

		}

	}

	this.hideLoader = function () {

		if(thisLoader.isActive) {
		
			thisLoader.isActive = false;

			if($('#'+divId).length) 

				$('#loaderbutton').hide();

		}
	}

}

function psubmit_form(myform, div, carica, fade, callback){

$(document).ready(function() {

	

	if (carica==null || carica=="")

		carica = "loader";
	
	$("#" + myform).submit(function(e) {	

		var formURL = $(this).attr("action");
		var langQuery = "";

		if(formURL.indexOf('?') != "-1")
			
			langQuery = "&lang="+AppLocale;

		else

			langQuery = "?lang="+AppLocale;
		
		if(carica == "ladda") {

			var l = Ladda.create(this);
	   		l.start();

		}

		else

			$("#" + carica).show(100);
		
		if (fade!=null){
		
			$( "#" + fade ).fadeTo( "slow", 0.33 );
		
		}		
		
		var postData = new FormData(this);		
		var request = $.ajax({
			
			url : formURL + langQuery,
			type: "POST",
			cache: false,
			contentType: false,
			processData: false,
			data : postData,
			success:function(data, textStatus, jqXHR) {

				$("#" + carica).hide(100);
				
				if (fade!=null){
					$( "#" + fade ).fadeTo( "slow", 1 );
				}
		
				request.done(function(msg) {
				
					$("#" + div).html(msg); 

					if(typeof callback != "undefined")

						callback(msg);
							
				});		
				
				
			},
			error: function(jqXHR, textStatus, errorThrown) {
				alert("C'è stato un errore nell'invio del form, riprova: " +jqXHR.responseJSON.error.message +". FILE: "+jqXHR.responseJSON.error.file);
			}
		});
		e.preventDefault();	
	});
});
}

function psubmit_form_custom(myform, div, carica, fade, callback){

$(document).ready(function() {

	if (carica==null || carica=="")
		carica = "loader";
	
	$("#" + myform).submit(function(e) {	

		if(carica == "ladda") {

			var l = Ladda.create(this);
	   		l.start();

		}

		else

			$("#" + carica).show(100);
		
		if (fade!=null){
		
			$( "#" + fade ).fadeTo( "slow", 0.33 );

		
		}		
		
		var postData = new FormData(this);;
		var formURL = $(this).attr("action");
		var request = $.ajax({
			
			url : formURL + "?lang="+AppLocale,
			type: "POST",
			cache: false,
			contentType: false,
			processData: false,
			data : postData,
			success:function(data, textStatus, jqXHR) {
				$("#" + carica).hide(100);
				
				if (fade!=null){
					$( "#" + fade ).fadeTo( "slow", 1 );
				}
		
				request.done(function(msg) {
				
					if(typeof callback != "undefined")

						callback(msg);
							
				});		
				
				
			},
			error: function(jqXHR, textStatus, errorThrown) {
				alert("C'è stato un errore nell'invio del form, riprova: " +jqXHR.responseJSON.error.message +". FILE: "+jqXHR.responseJSON.error.file);
			}
		});
		e.preventDefault();	
	});
});
}

    function progress(e){
        if(e.lengthComputable){
            //this makes a nice fancy progress bar
            $('progress').attr({value:e.loaded,max:e.total});
        }
    }

function psubmit_formck(myform, div, carica, callback){

$(document).ready(function() {

	if (carica==null || carica==""){
		carica = "loader";

	}

	$("#" + myform).submit(function(e) {

		$("#" + carica).show(100);
		//Instaze per il ckeditor
		
		for ( instance in CKEDITOR.instances ) {

			CKEDITOR.instances[instance].updateElement();
		}
		
		var postData = $(this).serializeArray();
		var formURL = $(this).attr("action");
		var request = $.ajax({
			
			url : formURL,
			type: "POST",
			data : postData,
			success:function(data, textStatus, jqXHR) {

				$("#" + carica).hide(100);
				request.done(function(msg) {
				
					$("#" + div).html(msg);
					//callback();
							
				});				
				
			},

			error: function(jqXHR, textStatus, errorThrown) {

				alert("C'è stato un errore nell'invio del form, riprova: " +jqXHR.responseJSON.error.message +". FILE: "+jqXHR.responseJSON.error.file);
			
			}
		});

		e.preventDefault();	
	
	});

});

}

function p_onbasket(myform, div, idmodal, customfunction, fade, carica){

$(document).ready(function() {
	if (carica==null || carica==""){
		carica = "loader";
	}
	$("#" + myform).submit(function(e)
	{
		$("#" + carica).show(100);
		if (fade!=null){
		
			$( "#" + fade ).fadeTo( "slow", 0.33 );
			
		}
		
		var postData = $(this).serializeArray();
		var formURL = $(this).attr("action");
		var request = $.ajax({
			
			url : formURL,
			type: "GET",
			data : postData,
			success:function(data, textStatus, jqXHR) {
				
				$("#" + carica).hide(100);
				
				request.done(function(msg) {
					
					$('#'+idmodal).modal('show');
					
					if (fade!=null){
						$( "#" + fade ).fadeTo( "slow", 1 );
					}
		
					if(typeof customfunction!="undefined")
						customfunction();
					
					$("#" + div).html(msg); 
							
				});		
				
				
			},
			error: function(jqXHR, textStatus, errorThrown) 
			{
				alert("C'Ã¨ stato un errore nell'invio del form, riprova" +jqXHR+"##"+textStatus+"##"+errorThrown);
			}
		});
		e.preventDefault();	
	});
});
}

function submitta(formid){

	if($.active == 0)
 
 	 $("#"+ formid).submit();
	
	else return;			
}

//Funzione di load di un div dato l'id e url di processazione , carica Ã¨ il div di caricamento da far sparire e comparire, fade Ã¨ il div che magare vogliamo far fare l'effetto di fade in caricamento	
function pload(div, pagina, carica, fade){

$(document).ready(function() {											
	if (carica==null){
		carica = "#loader";
	}
	else
		carica = "#" + carica;
													
	$(carica).show(100);
	
	if (fade!=null){
		
		$( "#" + fade ).fadeTo( "slow", 0.33 );
		
	}		
	var request = $.ajax({
		url: pagina,
		type: "GET",           
		dataType: "html"
	});
							
	
								
	request.done(function(msg) {
									
		$(carica).hide(100);
		
		if (fade!=null){
			$( "#" + fade ).fadeTo( "slow", 1 );
		}
		
		if( div != "")
			$("#" + div).html(msg);         
														
	});
												 
	request.fail(function(jqXHR, textStatus) {
		alert( "C'Ã¨ stato un errore: " + textStatus );
	});
});

}

function ploadpost(div, pagina, carica, fade){


	if (carica==null){
		carica = "#loader";

	}

	else
		carica = "#" + carica;
													
	$(carica).show(100);
	
	if (fade!=null){
		
		$( "#" + fade ).fadeTo( "slow", 0.33 );
		
	}		
	var request = $.ajax({
		url: pagina,
		type: "POST",           
		dataType: "html"
	});
							
	
								
	request.done(function(msg) {
									
		$(carica).hide(100);
		
		if (fade!=null){
			$( "#" + fade ).fadeTo( "slow", 1 );
		}
		
		if( div != "")
			$("#" + div).html(msg);         
														
	});
												 
	request.fail(function(jqXHR, textStatus) {
		alert( "C'Ã¨ stato un errore: " + textStatus );
	});


}

//funzione di lancio di una form senza fade e con loader annesso
function launch_loader(form,div_write,loader) {

$(document).ready(function() {	
	$(loader).css("display","initial");
	$.ajax({
		data: $(form).serialize(),
		type: $(form).attr('method'),
		url: $(form).attr('action'),
		success: function(response) {
			$(loader).css("display","none");
			$(div_write).html(response); 
		}
	});
});
}

function get_url(fromfield, tofield) {

        var bb = $('#' + fromfield).val();

        bb = bb.toLowerCase();
        bb = bb.replace(/ /g, "-");
        bb = bb.replace(/:/g, "-");
        bb = bb.replace(/à/g, "a");
        bb = bb.replace(/á/g, "a");
        bb = bb.replace(/è/g, "e");
        bb = bb.replace(/ì/g, "i");
        bb = bb.replace(/í/g, "i");
        bb = bb.replace(/ù/g, "u");
        bb = bb.replace(/ò/g, "o");
        bb = bb.replace(/\?/g, "");
        bb = bb.replace(/!/g, "");
        bb = bb.replace(/,/g, "-");
        bb = bb.replace(/"/g, "-");
        bb = bb.replace(/\//g, "-");
        bb = bb.replace(/'/g, "-");
        bb = bb.replace(/@/g, "-");
        bb = bb.replace(/&/g, "-");
        bb = bb.replace(/#/g, "-");
        bb = bb.replace(/ő/g, "o");
        
        $('#' + tofield).val( bb );    
}

function psendpost(page, inputdata, success) {

	var langQuery = "";

	if(page.indexOf('?') != "-1")
		
		langQuery = "&lang="+AppLocale;

	else

		langQuery = "?lang="+AppLocale;

	var request = $.ajax({
			
			url : page + langQuery,
			type: "POST",
			data : inputdata,

			success:function(data, textStatus, jqXHR)  {
				
				request.done(function(data) {

					success(data);

				});

			},

			error: function(jqXHR, textStatus, errorThrown)  {
				alert("C'è stato un errore nell'invio del form, riprova: " +jqXHR.responseJSON.error.message +". FILE: "+jqXHR.responseJSON.error.file+". LINE: "+jqXHR.responseJSON.error.line);
			}

		});

}


function useFilter(url) {

	// Gestione filtri per desktop
	if ($(window).width() >= 992){
		var filtroType = $("#posts_type");
		var filtroTags = $("input[name='newss_tags_filter']:checked");
		var filtroNewssCats = $("input[name='newss_categories_filter']:checked");		
		var filtroLocationLat = $("#location_lat");
		var filtroLocationLng = $("#location_lng");
		var filtroPeriodFrom = $("#date_from");
		var filtroPeriodTo = $("#date_to");
	}

	// Gestione filtri per mobile
	else{		
		//var filtroTag = $("input[name='newss_categories_filter']:checked");
		//var filtroPrezzo = "#price-selector-mobile";
		//var filtroBrand  = "#brand-filter-mobile";
	}

	//Prelevo tipo di post
	var posts_type = filtroType.val();


	//Seleziono tag di post scelti
	var newss_tags = [];

	filtroTags.each(function () {
       newss_tags.push(this.checked ? $(this).val() : "");
  	});


	//Seleziono categorie di post scelti
	var newss_cats = [];

	filtroNewssCats.each(function () {
       newss_cats.push(this.checked ? $(this).val() : "");
  	});


  	var dataInfo = { "posts_type": posts_type, "newss_tags": newss_tags, "newss_cats": newss_cats };


  	//Location
  	if( filtroLocationLat.val() != "" ) dataInfo["location_lat"] = filtroLocationLat.val();
  	if( filtroLocationLng.val() != "" ) dataInfo["location_lng"] = filtroLocationLng.val();

  	//Seleziono periodo
  	if( filtroPeriodFrom.val() != "" ) dataInfo["date_from"] = filtroPeriodFrom.val();
  	if( filtroPeriodTo.val() != "" ) dataInfo["date_to"] = filtroPeriodTo.val();  	
	

	//Chiamata GET

	$.ajax({
			
		url : url,
		type: "GET",
		data : dataInfo,

		success:function(data, textStatus, jqXHR)  {

			var url = this.url;

			if( url.indexOf("filter") != -1 )

				url = url.substr(url.indexOf("?"), url.length-1);

			window.history.pushState(null, null, url);
			
			$("#posts_list_box").html(data);

			$("#filter_button_box").show();

		},

		error: function(jqXHR, textStatus, errorThrown)  {
			alert("C'è stato un errore nell'invio del form, riprova: " +jqXHR.responseJSON.error.message +". FILE: "+jqXHR.responseJSON.error.file+". LINE: "+jqXHR.responseJSON.error.line);
		}

	});	

}


function resetFilter(url) {

	var posts_type = $("#posts_type").val();
	var dataInfo = { "posts_type": posts_type };

	//Chiamata GET
	$.ajax({
			
		url : url,
		type: "GET",
		data : dataInfo,

		success:function(data, textStatus, jqXHR)  {

			var url = this.url;

			if( url.indexOf("filter") != -1 )

				url = url.substr(url.indexOf("?"), 1);

			window.history.pushState(null, null, url);
			
			$("#posts_list_box").html(data);

			$("#filter_button_box").hide();

			//Resetto i filtri
			$('#filter .filter_box input[type="checkbox"]').each(function( index ) { $(this).attr('checked', false); });
			$('#filter .filter_box input[type="text"]').each(function( index ) { $(this).val(""); });
			$('#filter .filter_box input[type="hidden"]').each(function( index ) { $(this).val(""); });

		},

		error: function(jqXHR, textStatus, errorThrown)  {
			alert("C'è stato un errore nell'invio del form, riprova: " +jqXHR.responseJSON.error.message +". FILE: "+jqXHR.responseJSON.error.file+". LINE: "+jqXHR.responseJSON.error.line);
		}

	});	

}


function intocart(url, product_id, model_class, quantity, cartDiv, itemsnumberDiv, loader, type) {

	var model_id = $('input[name=' + model_class + ']:checked').val();

	cartDiv = "#cart_preview";
	itemsNumber = "#preview_items_number";
	cartTotal = "#preview_cart_total";

	if(typeof type == "undefined")

		type = "normal";	

	$('#'+loader).show(100);

	psendpost(url, { product_id: product_id, model_id: model_id, quantity: quantity, type: type}, function(data) {

		data = JSON.parse(data);

		if( data.error == 1 ) $("#addToCartErrors").html("<span class='add_cart_error alert alert-danger'>" + data.message + "</span>");
		else{

			//Pulisco messaggi di errore
			$("#addToCartErrors").html("");
		
			//Sostituisco nome e immagine prodotto su modal carrello		
			$("#cartProductName").html(data.title);



			if(typeof data.nostock != "undefined" && data.nostock == 1 ) {

				$("#addingCart").hide();
				$("#cartProductImage").hide();
				$("#error_img").html("<i class='fa fa-exclamation-triangle'></i>");
				$("#link_to_cart").hide();

			}
			else {

				$("#cartProductImage").show();
				$("#cartProductImage").attr('src', data.src + "/resize");
				$("#error_img").html("");
				$("#addingCart").show();
				$("#link_to_cart").show();

				//Aggiorno anteprima carrello
				$('#cart_preview').html(data.cartPreview);

			}
			
			$('#'+loader).hide();
			//Visualizzo modal carrello
			$('#modalCart').modal('show');

		}

	});	
	
}


function deleteItemCartView(url, index) {

	psendpost(url, { index: index }, function(data) {

		data = JSON.parse(data);

		if(typeof($("#cart_item_"+index)) != "undefined" && $("#cart_item_"+index) !== null)
			
			$("#cart_item_"+index).hide();

		$("#cartPreviewItem").html(data.cartPreview);

		$("#item_number").html(data.items);
		
		//Aggiorno anteprima carrello
		$("#cartPreviewBox").html(data.cartPreviewBox);

		//Aggiorno carrello mobile (view anteprima)
		if ($(window).width() <= 600){  

		    var cartPreview = $('#cartPreviewItem').html();

		    $('#cartTable').html(cartPreview);

		}

		if(data.items <= 0){			

			$('#cart_content').html(data.htmlReview);			

		}

	});

}

function cartPlus(add, id) {


	var num = parseInt($('#'+id).val());

	num = num + add;

	if(num <= $('#'+id).attr("max")){

		$('#'+id).val(num);

		$("#quantity_error").hide();

	}

	else $("#quantity_error").show();

}

function cartMinus(min, id) {

	var num = parseInt($('#'+id).val());

	if(num > 1)
	
		num = num - min;

	if(num >= $('#'+id).attr("min"))

		$('#'+id).val(num);

	$("#quantity_error").hide();

}




function modCartQty( item_schema, item_id, op_type ){	

	var qty = $( "#quantity_" + item_id ).val();

	if( !(op_type == "-" && qty <= 0) ){

		//Faccio una chiamata per il controllo della giacenza
		psendpost("cartQty", { "qty": qty, "item_schema": item_schema, "item_id": item_id, "op_type": op_type }, function(data) {

			data = JSON.parse(data);

			if( data.error != 0 ){

				$( "#quantity_" + item_id + "_error").show();
				$( "#quantity_" + item_id + "_error li").html(data.message);

			}
			else{

				$( "#quantity_" + item_id + "_error").hide();
				$( "#quantity_" + item_id ).val(data.message);
				$( "#item_" + item_id + "_total" ).html( data.item_total + " &euro;" );

			}

		});

	}

}


function updateCart(){

	/**

	var items = []; 

	var item_qty;

	$( "#cartTable .cart_item" ).each(function( index, items ) {

		item_qty = $("#" + index.id + ".c-qty ").val();

		item = { 'key': index.id, 'quantity': 'item_qty' };
	  
		items.push(item);

	});

	*/


	psendpost("updateCart", {"items" : items}, function(data) {

		data = JSON.parse(data);

		if(typeof($("#cart_item_"+index)) != "undefined" && $("#cart_item_"+index) !== null)
			
			$("#cart_item_"+index).hide();

		$("#cartPreviewItem").html(data.cartPreview);

		$("#item_number").html(data.items);
		
		//Aggiorno anteprima carrello
		$("#cartPreviewBox").html(data.cartPreviewBox);

		//Aggiorno carrello mobile (view anteprima)
		if ($(window).width() <= 600){  

		    var cartPreview = $('#cartPreviewItem').html();

		    $('#cartTable').html(cartPreview);

		}

		if(data.items <= 0){			

			$('#cart_content').html(data.htmlReview);			

		}

	});

	estimateFees( "estimatefees", "ready" );

	//estimateDiscount

}





function estimateFees(url, caller, urlTax = "") {

	var reloadShipping = true;
	var billing_country = $("#sped1").val();
	var user_type = $("#user_type").val();

	if(typeof caller == "undefined")

		caller = "null";

	if(caller == "shipment_select"){


		var feesId = $('input[name=shipment_select]:checked').attr( "value" );

		psendpost(url, { type: "change_shipment_type", user_type: user_type, shipment_id: feesId, billing_country: billing_country }, function(data) {

			data = JSON.parse(data);

			$("#ordertable").html(data.ordertable);

			$("#cartTotal").html(data.total);

		});

	}
	else{

		var reloaddivTot = "#cartTotal";

		var countrySel = "#sped1";

		//var shipmentSel = $("input[name='shipment']:checked").val();

		if( typeof $("#check_address_shipping") != 'undefined' ) {

			if( $("#check_address_shipping").is(':checked') ) {

				countrySel = "#sped2";

				if( caller == "sped1") reloadShipping = false;

			}

		}

		//Tolgo provincia se necessario
		if( $("#" + caller ).val() == 115 )
		    $("#" + caller + "_province").show();
		else
		    $("#" + caller + "_province").hide();

		if(reloadShipping) {

			var country = $(countrySel).val();			

			psendpost(url, { country: country, user_type: user_type, billing_country: billing_country }, function(data) {

				data = JSON.parse(data);
				
				$("#feesPreview").show(); 
				$("#feesPreview").html(data.html);
				$(reloaddivTot).html(data.total);

				//Aggiorno la vista dei metodi di spedizione

				$("#shipping_methods").html(data.shippingMethods);


				if(typeof data.ordertable != "undefined")

					$("#ordertable").html(data.ordertable);


			});

		} else{

			var billing_country = $("#sped1").val();

			psendpost( urlTax , { user_type: user_type, billing_country: billing_country }, function(data) {

				data = JSON.parse(data);				
				
				$(reloaddivTot).html(data.total);

				if(typeof data.ordertable != "undefined")

					$("#ordertable").html(data.ordertable);

			});

		}

	}

}

function estimateDiscount(url, reloaddiv) {

	if(typeof reloaddiv == "undefined")

		reloaddiv = "cartTotal";

	psendpost(url, { code: $('#code').val() }, function(data) {

		data = JSON.parse(data);
		
		$("#promotionPreview").show(); 
		$("#promotionPreview").html(data.html);
		$("#" + reloaddiv).html(data.total); 
		

	});

}

function payOrder(url, id) {

	$('#loaderbutton_'+id).show();

	psendpost(url, { id: id, payment_id: 1 }, function(data) {

		$('#loaderbutton_'+id).hide();
		$('#errors_'+id).html(data);		

	});

}

function viewRegisterForm() {

	 if($("#check_register").is(':checked'))

	 	$("#register_with").toggleClass('hide show');

	 else

	 	$("#register_with").toggleClass('show hide');

}

function viewShippingForm() {

    if($("#check_address_shipping").is(':checked'))

       $("#box_address_shipping").attr("class","");

    else $("#box_address_shipping").attr("class","hidden");

    estimateFees(rootUrl + "/cart/estimatefees?checkout", "");
    
}



function changemethod(method) {

	if(method == "creditcard") {


		$('#creditCardBox').show();

	}

	else{

		$('#creditCardBox').hide();

	}

}



function setProdModel(idModel, modelImage){	

	$("#model_img").html('<div style="border: 1px solid #cdcdcd;"><img style="padding: 35px 85px;" src="' + modelImage + '/resize" alt="model_' + idModel + '" /></div>');

	$('#model_img').show(); 

	$('#carousel-bounding-box').hide();

}

function filterNewsCats(url, news_category_id) {

	//Preparo i dati per i filtri
  	var dataInfo = { "news_category_id": news_category_id };	
  	
	//Chiamata GET
	$.ajax({
			
		url : url,
		type: "GET",
		data : dataInfo,

		success:function(data, textStatus, jqXHR)  {

			var url = this.url;

			if( url.indexOf("filter") != -1 )

				url = url.substr(url.indexOf("?"), url.length-1);

			window.history.pushState(null, null, url);
			
			$("#news_list").html(data);

		},

		error: function(jqXHR, textStatus, errorThrown)  {
			alert("C'è stato un errore nell'invio del form, riprova: " +jqXHR.responseJSON.error.message +". FILE: "+jqXHR.responseJSON.error.file+". LINE: "+jqXHR.responseJSON.error.line);
		}

	});	

}



/*
| -----------------
| PERSONALIZZAZIONI
| -----------------
 */




